//
//
// ===== テーマスクリプト [Urban] =====
//
//


//
// === 初期実行 ===
//

$(function() {
'use strict';

  //
  // === 共通 ===
  //

  var
    $window = $(window),
    $body = $('body'),
    $pageHeader = $('#page-header'),
    slideSpeed = 'normal';



  //
  // === メニュー ===
  //

  // --- すべてを開く／閉じる ---

  var
    classGroupExpanded = 'expanded';

  $('.btn-expand-menu')
    .click(function(e) {

      var $this = $(this);

      if ($this.hasClass(classGroupExpanded)) {
        $this
          .removeClass(classGroupExpanded);
        $('.nav-menu-list > li.group')
          .removeClass(classGroupExpanded)
          .children('ul')
          .slideUp(slideSpeed);
      } else {
        $this
          .addClass(classGroupExpanded);
        $('.nav-menu-list > li.group')
          .addClass(classGroupExpanded)
          .children('ul')
          .slideDown(slideSpeed);
      }

      return false;

    });


  // --- メニューの開閉 ---

  var
    classMenuClosed = 'status-menu-closed';

  $('.btn-view-switch')
    .click(function(e) {

      $body.toggleClass(classMenuClosed, !$body.hasClass(classMenuClosed));

      return false;

    });



  //
  // === ウィンドウ ===
  //

  // --- ウィンドウイベント ---

  var
    $navMenuContent = $('.nav-menu-content'),
    subtractHeight = ($window.width() > 768) ? 95 : 70;


  $window
  
    // --- リサイズ ---
    .resize(function() {

      $navMenuContent
        .height($window.innerHeight() - subtractHeight);

    })
    .trigger('resize');


});

// --- eof ---
