function this_content() {
	
	var menu_index;
	var isCheck;
	
	for ( var i = 0; i < 5; i++ ) {
		menu_index = "head_menu_" + i;
		var menu = document.getElementById(menu_index);
		if (menu == null) {
			continue;
		}

		isCheck = menu.checked;
		if (isCheck == true) {
			menu.style.backgroundColor = "#e6bbc9";
		}
	}
}

function menu_toggle() {
	var isCheck = document.getElementById( "menu-toggle" ).checked;

	if ( isCheck == true ) {
		document.getElementById( "side_menu" ).style.display = "none";

		document.getElementById( "main" ).style.marginLeft = "2%";

		document.getElementById( "menu-toggle" ).checked = false;
	} else {
		document.getElementById( "side_menu" ).style.display = "block";

		document.getElementById( "main" ).style.marginLeft = "22%";

		document.getElementById( "menu-toggle" ).checked = true;
	}
}

$(function() {

	//.accordionの中のp要素がクリックされたら
	$('.accordion p').click(function(){

		//クリックされた.accordionの中のp要素に隣接する.accordionの中の.innerを開いたり閉じたりする。
		$(this).next('.accordion .inner').slideToggle();

		//クリックされた.accordionの中のp要素以外の.accordionの中のp要素に隣接する.accordionの中の.innerを閉じる
		$('.accordion p').not($(this)).next('.accordion .inner').slideUp();
	});
});

// ページトップに戻るボタンを表示・非表示にする
$(document).ready(function(){
	document.getElementById('top_link').style.display="none";

	$(window).scroll(function() {
		if($(this).scrollTop() > 200) { // 200pxスクロールしたらページトップに戻るボタンを表示
			document.getElementById('top_link').style.display="block";
		} else {
			document.getElementById('top_link').style.display="none";
		}
	});
});

