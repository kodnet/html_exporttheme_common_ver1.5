$(document).ready(function () {

    _removeIndexTree();
    _scrollImage();
    _setPositionNav();
   
    function _scrollImage() {
        var img = $("#main section").find("img");
        if (typeof img !== "undefined" && img.length > 0) {
            for (var i = 0; i < img.length; i++) {
                var imgWidth = $(img[i]).width();
                var parentWidth = $(img[i]).parent().width();
                if (imgWidth > parentWidth) {
                    $(img[i]).parent().css("overflow", "auto");
                }
            }
        }
    }

    function _setPositionNav() {
        var containerWidth = $("#container").width();
        var sideWidth = $("#container #side_menu").width();
        var widthNav = containerWidth - sideWidth;
        $("#navTopSection").css("width", Math.floor(widthNav - 1));
        var heightNav = $("#navTopSection").height();
        $("#topicpath").css("margin-top", heightNav - 32);
    }

    function _removeIndexTree() {
        var indexTree = $("#cover_contents_list ul li");
        $(indexTree).each(function () {
            if ($(this).find("a").attr("href") === "index.html") {
                $(this).remove();
            }
        });

        var indexTreeNav = $("#navTopSection ul li");
        $(indexTreeNav).each(function () {
            if ($(this).find("a").attr("href") === "index.html") {
                $(this).remove();
            }
        });
    }

    // function _setStyleMacroIconCaution() {
    //     var markIconCaution = $(document).find("div.table-wrap");
    //     if (typeof markIconCaution !== undefined && markIconCaution.length > 0) {
    //         $(markIconCaution).each(function () {
    //             var macroMarkCaution = $(this).attr("data-macro-name");
    //             if (macroMarkCaution === "mark-icon-caution") {
    //                 $(this).find("table.macroTable").addClass("table-mark-icon-caution");
    //             }
    //         });
    //     }
    // }

    $(window).resize(function () {
        _scrollImage();
        _setPositionNav()
    });
});