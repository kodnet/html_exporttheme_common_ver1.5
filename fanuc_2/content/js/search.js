//HTMLファイル格納パス
let htmlpath = './';

// Base64でコード
const Base64 = {
    encode: function(str) {
        return btoa(unescape(encodeURIComponent(str)));
    },
    decode: function(str) {
        return decodeURIComponent(escape(atob(str)));
    }
};

// --------------------
// 検索ボタン押下時処理
// --------------------
function openSearchResult(){
	const key = $('#searchText').val().trim();
	if (!key){
		// 検索キーワードが空の場合、パラメータなし
		window.open('search.html', '_self');
		return;
	}
	window.open('search.html?key=' + key, '_self');
}

// --------------------
// 画面表示時処理
// --------------------
$(document).ready( function(){
	let params = getURLParams(document.location.search)
    if (params == null){
    	return;
    }
	// パラメータの数で処理が分かれる
	if (Object.keys(params).length > 1){
		// ハイライト
		return setHighlight(params);
	}
	// 検索
	return search(params['key']);
});

// --------------------
// 検索用
// --------------------
// 検索
// - テキストキャッシュを検索、位置を取得
// - 位置からインデックスキャッシュを検索、タグ名と何個目のタグかを取得
// - 検索結果を検索表示に反映
function search(key){
	// 検索キーワード取得
	// const key = $('#searchText').val().trim();
	if (!key){
		// 検索キーワードが空の場合はなにもしない
		return;
	}

	// 結果表示用テキスト
	let resulttext = '<h1 class="head_n1">' + key + '　検索結果</h1>';
	// キャッシュテキストをBase64デコード
	const decodeddata = Base64.decode(cachedata);

	let position = 0;
	let doneparagraph = [];
	let tagposition = '';
	let regexp = createRegExp(key);
	// テキストキャッシュから検索キーワードが見つかるまで検索実行
	while ((regarray = regexp.exec(decodeddata)) !== null){
		position = regarray.index;
 		// インデックスキャッシュから段落を取り出す
		for (i = 0; i< indexdata.length; i++){
			if (position >= indexdata[i][0] && position <= indexdata[i][1]){
				// 段落テキスト
				paragraph = decodeddata.substring(indexdata[i][0], indexdata[i][1]+1);
				// 段落(タグ)位置
				tagposition = indexdata[i][2] + indexdata[i][3] + indexdata[i][4];
				if (doneparagraph.indexOf(tagposition) == -1){
					// 段落がすでに処理されていない場合、段落から検索結果を取得
					let extracttext = searchParagraphCache(paragraph, indexdata, key);
					resulttext += extracttext;
					doneparagraph.push(tagposition);
				}
			}
		}
	}

	// 結果表示
	$('#result').html(resulttext);
}

// 正規表現を作成
function createRegExp(key){
	// 全角英数記号を半角へ
	// ※キャッシュが正規化されていれば、コメントアウトを外す
	let str = key.toHalfWidth();
	// 半角カタカナを全角へ
	str = str.toFullWidthKana();
	// エスケープ ワイルドカードを実装しないので*?はエスケープ
	str = str.replace(/[\\^$.*?+()[\]{}|]/g, '\\$&');
	//str = str.replace(/[\\^$.+()[\]{}|]/g, '\\$&');
	// let str = key.replace(/[\\^$.+()[\]{}|]/g, '\\$&');
	// ワイルドカード 実装しない
	//str = str.replace('?', '.');
	//str = str.replace('*', '.*?');
	return new RegExp((str), 'gi');
}

// 全角英数(記号)を半角英数(記号)へ
String.prototype.toHalfWidth = function(flag = true) {
	// 半角変換
	if (flag){
		// 英数のみ
		str = this.replace(/[！-～]/g, function(tmpStr) {
			// 文字コードをシフト
			return String.fromCharCode( tmpStr.charCodeAt(0) - 0xFEE0 );
		});
	} else{
		// 英数記号
		str = this.replace(/[！-～]/g, function(tmpStr) {
			// 文字コードをシフト
			return String.fromCharCode( tmpStr.charCodeAt(0) - 0xFEE0 );
		});
	}
	// 文字コードシフトで対応できない文字の変換
	return str.replace(/”/g, "\"").replace(/’/g, "'").replace(/‘/g, "`")
		.replace(/￥/g, "\\").replace(/　/g, " ").replace(/〜/g, "~");
}

// 半角カタカナを全角カタカナへ
String.prototype.toFullWidthKana = function() {
    replaceFm = new Array(
            'ｳﾞ','ｶﾞ','ｷﾞ','ｸﾞ','ｹﾞ','ｺﾞ'
            ,'ｻﾞ','ｼﾞ','ｽﾞ','ｾﾞ','ｿﾞ'
            ,'ﾀﾞ','ﾁﾞ','ﾂﾞ','ﾃﾞ','ﾄﾞ'
            ,'ﾊﾞ','ﾋﾞ','ﾌﾞ','ﾍﾞ','ﾎﾞ'
            ,'ﾊﾟ','ﾋﾟ','ﾌﾟ','ﾍﾟ','ﾎﾟ'
        );
        replaceTo = new Array(
            'ヴ','ガ','ギ','グ','ゲ','ゴ'
            ,'ザ','ジ','ズ','ゼ','ゾ'
            ,'ダ','ヂ','ヅ','デ','ド'
            ,'バ','ビ','ブ','ベ','ボ'
            ,'パ','ピ','プ','ペ','ポ'
        );
        for (var key in replaceFm) {
            str = str.replace(new RegExp(replaceFm[key], 'g'),replaceTo[key]);
        }

        replaceFm = new Array(
            'ｱ','ｲ','ｳ','ｴ','ｵ'
            ,'ｶ','ｷ','ｸ','ｹ','ｺ'
            ,'ｻ','ｼ','ｽ','ｾ','ｿ'
            ,'ﾀ','ﾁ','ﾂ','ﾃ','ﾄ'
            ,'ﾅ','ﾆ','ﾇ','ﾈ','ﾉ'
            ,'ﾊ','ﾋ','ﾌ','ﾍ','ﾎ'
            ,'ﾏ','ﾐ','ﾑ','ﾒ','ﾓ'
            ,'ﾔ','ﾕ','ﾖ'
            ,'ﾗ','ﾘ','ﾙ','ﾚ','ﾛ'
            ,'ﾜ','ｦ','ﾝ'
            ,'ｧ','ｨ','ｩ','ｪ','ｫ'
            ,'ｬ','ｭ','ｮ','ｯ'
            ,'､','｡','ｰ','｢','｣','ﾞ','ﾟ'
        );
        replaceTo = new Array(
            'ア','イ','ウ','エ','オ'
            ,'カ','キ','ク','ケ','コ'
            ,'サ','シ','ス','セ','ソ'
            ,'タ','チ','ツ','テ','ト'
            ,'ナ','ニ','ヌ','ネ','ノ'
            ,'ハ','ヒ','フ','ヘ','ホ'
            ,'マ','ミ','ム','メ','モ'
            ,'ヤ','ユ','ヨ'
            ,'ラ','リ','ル','レ','ロ'
            ,'ワ','ヲ','ン'
            ,'ァ','ィ','ゥ','ェ','ォ'
            ,'ャ','ュ','ョ','ッ'
            ,'、','。','ー','「','」','”',''
        );
        for (var key in replaceFm) {
            str = str.replace(new RegExp(replaceFm[key], 'g'),replaceTo[key]);
        }
        return str;
}

// 段落から検索結果を取得
// - 段落から検索結果を作成し、返却
// - パラメータ
// - paragraph:Base64でコード済キャッシュテキストから取り出した段落
// - indexdata:インデックスキャッシュ
// - key:検索キーワード
// - 戻り値
// - 検索結果(HTMLテキスト)
function searchParagraphCache(paragraph, indexdata, key){
	let resulttext = '';
	let keypositions = '';
	let keylength = '';
	let regarray;
	let regexp = createRegExp(key);
	// 段落(タグ)テキストキャッシュから検索キーワード位置と長さをすべて取り出す
	while ((regarray = regexp.exec(paragraph)) !== null){
		keypositions += ',' + regarray.index;
		keylength += ',' + (Number(regexp.lastIndex) - Number(regarray.index));
	}
	// 検索キーワードを太字に設定
	paragraph = paragraph.replace(regexp, '<strong>' + '$&' + '</strong>')
	// 検索結果(HTMLテキスト)を生成
	resulttext = '<div class="sample_search"><p><a href="' + htmlpath + indexdata[i][4]
		+ '?key=' + encodeURIComponent(key)
		+ '&tag=' + indexdata[i][2]
		+ '&position=' + indexdata[i][3]
		+ '&keyposition=' + keypositions.slice(1)
		+ '&keylength=' + keylength.slice(1)
		+ '" target="_blank">'
		+ Base64.decode(indexdata[i][5]) + '</a></p><p>' + paragraph + '</p></div>';
	return resulttext;
}

// --------------------
// ハイライト表示用
// --------------------
// ハイライト開始タグ
const HLSTAG = '<strong class="highlight">';
const HLETAG = '</strong>';
// ハイライト設定
function setHighlight(params){

    // パラメータから検索キーワード取得
	let key = params['key'];
	// パラメータからタグ位置取得
	let position = params['position'];
	// パラメータからタグ名取得
	let tag = params['tag'];
	// パラメータから検索キーワード位置取得
	let keyposition = params['keyposition'];
	// パラメータから検索キーワード長さ取得
	let keylength = params['keylength'];

	// 検索キーワードがある場合
	if (key) {
		let tags = $('#main_contents').find(tag);
		// 検索キーワードを含む本文エレメントを取得
		let tagelm = tags[position];

		// 検索キーワードの一文字ずつにハイライトタグを設定
		// 検索キーワード位置にハイライトタグを設定した配列１を取得
		let keypositions = getKeyPositionArray(tagelm, key, keyposition, keylength);

		// 配列１に、本文と本文に含まれるタグを埋め込んむだ配列２を取得
		let htmls = [];
		getHtmlArray(tagelm, keypositions, htmls, false);

		// 配列２を文字列に結合してHTMLテキストとして反映
		tagelm.innerHTML = htmls.join('');

		// タグ位置付近にスクロール移動
		let p = $(tagelm).offset().top;
		let t = ($('header').height() + $('nav').height()) * 2;// *2は微調整
		$('html,body').animate({ scrollTop: p-t }, 'fast');

	}
}

// 検索キーワード位置にハイライトタグを設定した配列１を返却
// - パラメータ
// - tagelm:タグElement
// - key:検索キーワード
// - keyposition:検索キーワード位置
// - keylength:検索キーワード長さ
// - 戻り値
// - 配列１[本文位置][0]="ハイライト開始タグ"
// - 配列１[本文位置][1]="ハイライト終了タグ"
function getKeyPositionArray(tagelm, key, keyposition, keylength){
	let keypositions = [];
	let text = tagelm.textContent;
	let keypos = keyposition.split(',');
	let keylen = keylength.split(',');
	let textdone = 0;
	let trimnum = text.length - text.trimStart().length;
	// タグが階層になっているとtextContentでテキストを取得すると改行/タブが混入している
	// 先頭の改行/タブをずらしておく
	for(let k = 0; k < trimnum; k++) {//if (text[j] == '\n') {
		keypositions.push(['','']);
	}
	for(let i = 0; i < keypos.length; i++){
		for(let j = textdone; j < text.length; j++){
			if (Number(keypos[i]) == j){
				// 検索キーワードの配列は[<ハイライト開始タグ>,</ハイライト終了タグ>]を設定
				let keycnt = Number(keylen[i]);
				for (let i = 0; i < keycnt; i++){
					keypositions.push([HLSTAG,HLETAG]);
				}
				textdone = j + keycnt;
				break;
			}
			// ハイライト以外の配列には['','']を設定
			keypositions.push(['','']);
		}
	}
	if (keypositions.length < text.length){
		// ハイライト位置を付け終わった以降の配列には['','']を設定
		let remainlen = text.length - keypositions.length;
		for (let i = 0; i < remainlen; i++){
			keypositions.push(['','']);
		}
	}
	return keypositions;
}

// 本文と本文に含まれるタグを埋め込んむだ配列を返却
// - パラメータ
// - tagelm:タグElement
// - keypositions:検索キーワード位置にハイライトタグを設定した配列１
// - htmls:本文と本文に含まれるタグとハイライトを埋め込んだ配列２
// - exist:本文に含まれるタグがあるか
// - 戻り値
// - 配列２[本文位置]="本文と本文に含まれるタグとハイライトを埋め込んだHTMLテキスト"
function getHtmlArray(tagelm, keypositions,  htmls, exist){
	let chnodes = $(tagelm).contents();
	for(let i = 0; i < chnodes.length; i++){
		if (chnodes[i].nodeType == 1){
			if (chnodes[i].childNodes.length == 0){
				// 本文に含まれる本体しかないタグはここで埋め込み 例：<br/>など
					htmls[htmls.length -1] = htmls[htmls.length -1] + '<' + chnodes[i].tagName + '/>';
					htmls = getHtmlArray(chnodes[i], keypositions, htmls, true);
			} else {
				// 本文に含まれるタグは配下を文字列として取り出す
				htmls = getHtmlArray(chnodes[i], keypositions, htmls, true);
			}
		} else {
			let text = chnodes[i].data;
			// 開始タグ取得
			let starthtml = getStartTagHTML(chnodes[i].parentNode, exist);
			// 終了月取得
			let endhtml = getEndTagHTML(chnodes[i].parentNode, exist);
			for(let j = 0; j < text.length; j++){
				// 本文に含まれるタグとハイライトを含めて文字列として取得、配列に設定
				htmls.push(getTagHTML(text, j, keypositions, htmls, starthtml, endhtml));
			}
			exist = false;
		}
	}
	return htmls;
}

// 本文中に含まれる開始タグを文字列として返却
function getStartTagHTML(chnode, exist){
	if (!exist) return '';
	let starthtml = "<" + chnode.tagName;
	for (let k = 0; k < chnode.attributes.length; k++){
		starthtml += " " + chnode.attributes[k].name + "=\"" + chnode.attributes[k].value + "\"";
	}
	starthtml += ">";
	return starthtml;
}

// 本文中に含まれる終了タグを文字列として返却
function getEndTagHTML(chnode, exist){
	if (!exist) return '';
	return "</" + chnode.tagName + ">";
}

// 本文に含まれるタグとハイライトタグを含めて文字列として返却
function getTagHTML(text, j, keypositions, htmls, starthtml, endhtml){
	let contenttext = keypositions[htmls.length][0] + text[j] + keypositions[htmls.length][1];

	if (j == 0 && text.length == 1){
		return starthtml + contenttext + endhtml;
	}
	if (j == 0){
		return starthtml + contenttext;
	}
	if (j == text.length - 1){
		return contenttext + endhtml;
	}
	return contenttext;
}

// URLパラメータ取得
function getURLParams (path) {
    if (!path) return false;

    var param = path.match(/\?([^?]*)$/);

    if (!param || param[1] === '') return false;

    var tmpParams = param[1].split('&');
    var keyValue  = [];
    var params    = {};

    for (var i = 0, len = tmpParams.length; i < len; i++) {
        keyValue = tmpParams[i].split('=');
        params[keyValue[0]] = decodeURIComponent(keyValue[1]);
    }

    return params;
};
