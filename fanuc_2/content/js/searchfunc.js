﻿
$(function () {
	$("#searchText").click(function () {
		$("#searchText").val("");
	});
});

$(document).ready(function () {
	var keyword = '';
	var queries = getUrlVars();
	if (queries["q"]) {
		keyword = decodeURIComponent(queries["q"]);
		$(".main").highlight(keyword);
		$("#main").highlight(keyword);
	}
	// 20181010 ADD
	if (history.length < 2) {
		var footObj = document.getElementById('prev_link');
		if (footObj.length > 0)
			document.getElementById('prev_link').style.display = "none";
	}
});

function getUrlVars() {
	var vars = [], max = 0, hash = "", array = "";
	var url = window.location.search;
	vars["q"] = url.slice(1).split('=')[1];
	/*
	   hash = url.slice(1).split('&');
	   max = hash.length;
	   for (var i = 0; i < max; i++) {
		   array = hash[i].split('=');    //keyと値に分割。
		   vars.push(array[0]);    //末尾にクエリ文字列のkeyを挿入。
		   vars[array[0]] = array[1];    //先ほど確保したkeyに、値を代入。
	   }*/

	return vars;
}

function getFileName() {
	return window.location.href.match(".+/(.+?)([\?#;].*)?$")[1];
}

var footer_elm;//増山
function changeFind() {	//増山
	//	document.getElementById('contents').style.display = "block";
	document.getElementsByTagName('article')[0].style.display = "block";	//20181116 大島
	//	document.getElementById('footer').style.display = "block";
	document.getElementById('search_result').style.display = "none";
	document.getElementsByTagName('footer')[0].innerHTML = footer_elm;	//20181116 大島
}
//jquery.hyoji.jsからコピー 増山
function hihyoji(num) {
	var max_num = 500;
	for (var i = 0; i < max_num; i++) {
		var disp_num = "disp" + i;
		var hyoji_num = "hyoji" + i;
		var key_num = document.getElementsByClassName(hyoji_num).length;
		var objId = document.getElementById(disp_num);
		if (objId) {
			objId.style.display = "none";
		}
		//		document.getElementById("contents").style.paddingBottom = "92px";
		document.getElementsByTagName("article")[0].style.paddingBottom = "92px";	//20181116 大島
		//		var footerId=document.getElementById("footer");
		var footerId = document.getElementsByTagName("footer")[0];	//20181116 大島
		if (footerId) {
			footerId.style.height = "";
		}
		for (var j = 0; j < key_num; j++) {
			document.getElementsByClassName(hyoji_num)[j].style.backgroundColor = "";
			document.getElementsByClassName(hyoji_num)[j].id = "";
		}
	}
}
function ShowFormGuide(obj, GuideSentence) {
	// 入力案内を表示
	if (obj.value == '') {
		obj.value = GuideSentence;
		obj.style.color = '#808080';
	}
}
function HideFormGuide(obj, GuideSentence) {
	// 入力案内を消す
	if (obj.value == GuideSentence) {
		obj.value = '';
		obj.style.color = '#000000';
	}
}

// ヘッダの検索インプットから呼ばれる
function redirectSearch(GuideSentence) {
	var value = $("input.searchText").val();
	if (value == GuideSentence || value == "") {
		return;
	}

	var url = "./search.html?q=" + encodeURIComponent(value);
	window.location.href = url;
}

// 検索ロジック
function searchKeyword(Flag) {
	var keyword = '';
	var keyword_org = '';	//増山
	var m_type = '';
	keyword = document.getElementById('searchText').value;
	//	keyword_org = keyword;	//増山
	for (var i = 0; i < keyword.length; i++) {
		//特殊文字に対応 20181011
		if (keyword[i] == "&") {
			keyword_org += "＆";
		} else if (keyword[i] == ";") {
			keyword_org += "；";
		} else {
			keyword_org += keyword[i];
		}
	}
	// 20180922 ADD m.sato
	//	keyword = keyword.replace("'", "&#39;");	増山
	keyword = keyword.trim();	//増山
	// 20181008 ADD m.sato
	if (keyword.length < 1)
		return;
	m_type = 'manual'; //document.getElementById('mtype').value;

	var result = document.getElementById('result');
	//	var footer = document.getElementById('footer');	//増山
	var footer = document.getElementsByTagName('footer')[0];	//20181116 大島
	var reg_spchar = ['[', ']', '(', ')', '{', '}', '.', '^', '+', '*', '?', '$', '|', '-', '\\', '#', '%', '='];
	//;が特殊文字が入っているために関係ないページが表示される問題を回避するための宣言
	var spchar = ["&#39;", "&deg;", "&alpha;", "&beta;", "&amp;", "&Delta;", "&rarr;", "&plusmn;", "&uarr;", "&gamma;", "&epsilon;", "&hellip;", "&Prime;", "&nbsp;"];
	var tmp_str = '';
	for (var i = 0; i < keyword.length; i++) {
		//特殊文字に対応 20181011
		if (keyword[i] == "'") {	//"'"はsearchData.jsでは&#39;となっている
			// 2019-01-24 H.K. 1月分納品時のテスト時に発覚した不具合の対応
			// 症状： 検索用キャッシュファイルは、HTMLの見た目に合わせる必要があり、実体参照を展開させたところ引用符の検索ができない
			// 原因： 「特殊文字に対応 20181011」の処置が不要
			// 対処： 「特殊文字に対応 20181011」をロールバック
			// 検索結果を別タブに表示することで対応
			//tmp_str += "&#39;";
			tmp_str += "'";
		} else if (keyword[i] == '"') {
			//tmp_str += "&Prime;";
			tmp_str += '"';
		} else {
			for (var j = 0; j < reg_spchar.length; j++) {	//メタ文字に\を付ける
				if (keyword[i] == reg_spchar[j]) {
					tmp_str += '\\'
				}
			}
			tmp_str += keyword.substring(i, (i + 1))
		}
	}
	keyword = tmp_str;
	var keyword_array = keyword.split(/ |　/);
	var out_str = '';
	var count = 0;
	//
	var keywordReg = new Array();
	if (keyword == "\\#") { //#だけの検索だと、&#39;が検索されてしまう 20181011
		keywordReg.push(RegExp('[^&][#][^3][^9][^;]', 'gi'));
	} else if (keyword == '&') {
		keywordReg.push(RegExp('&[^n][^b][^s][^p]'));
	} else {
		for (var i = 0; i < keyword_array.length; i++) {
			if (keyword_array[i]) {
				var text = RegExp(keyword_array[i], 'gi')
				keywordReg.push(text);
			}
		}
	}

	var count = 0;
	var out_str = '';
	var footer_str = '';	//増山
	if (keyword && keywordReg.length) {
		//		document.getElementById('contents').style.display = "none";
		document.getElementsByTagName('article')[0].style.display = "none";	//20181116 大島
		//document.getElementById('footer').style.marginTop = "20px;";
		document.getElementById('search_result').style.display = "block";
		//document.getElementById('search_result').style.marginBottom = "20px;";

		// 現在のファイル名
		var filename = getFileName();

		// パンくず
		//		if (
		//			filename=="index.html" ||
		//			filename=="00_03_contents.html" ||
		//			filename=="00_05_index.html" ||
		//			filename=="00_04_history.html" ||
		//			filename=="00_02_overview.html"
		//		)
		out_str += '<div id="topicpath"><ol><li><a href="index.html">ホーム</a></li><li>検索結果</li></ol></div>';
		//		else
		//			out_str += '<div id="topicpath"><ol><li><a href="../index.html">ホーム</a></li><li>検索結果</li></ol></div>';

		out_str += '<h1 class="head_n1">' + keyword_org + '　検索結果</h1>';	//増山 keyword->keyword_org

		// out_str += '<div class="text">';
		// 検索結果部生成
		var keyw = '';	//html?q=の次のキーワードをエラーが出ないよう変換する
		for (var i = 0; i < keyword.length; i++) {
			//特殊文字に対応 20181011
			if (i < keyword.length - 1 && keyword[i] == '\\' && keyword[i + 1] == '%') {
				keyw += '\\parcent';
				i++;
			} else if (i < keyword.length - 1 && keyword[i] == '\\' && keyword[i + 1] == '=') {
				keyw += '\\equal';
				i++;
			} else if (i < keyword.length - 1 && keyword[i] == '\\' && keyword[i + 1] == '#') {
				keyw += '\\sharp';
				i++;
			} else {
				keyw += keyword.substring(i, i + 1);
			}
		}
		//検索用にブランクを取り除く これは動いてないと思われるが怖いので削除してない
		tmp_str = '';
		for (var i = 0; i < keyword.length; i++) {
			if (keyword[i] != ' ') {
				tmp_str += keyword.substring(i, i + 1);
			}
		}
		keyword = tmp_str;
		data = pageDataArray;
		for (var i = 0; i < data.length; i++) {
			var hit_pos = -1;
			var all_hit = true;
			var ttl_hit = true;

			for (var j = 0; j < keywordReg.length; j++) {
				if (data[i][1].toLowerCase().search(keywordReg[j]) == -1) {
					all_hit = false;
				} else {
					hit_pos = data[i][1].toLowerCase().search(keywordReg[j]);
					if (keyword == ';') {
						//キーワードが';'のとき、特殊文字の;にヒットしてしまうので、その回避策
						//';'のヒット数を数える
						var fnd = data[i][3].toLowerCase().match(keywordReg[j], 'gi');
						sln_cnt = fnd.length;
						//特殊文字のヒット数を数える
						var cnt = 0;
						for (var k = 0; k < spchar.length; k++) {
							fnd = data[i][1].toLowerCase().match(new RegExp('(' + spchar[k] + ')', 'gi'));
							if (fnd != null) cnt += fnd.length;
						}
						//もし、特殊文字の数と';'の数が等しいなら、';'は無かったことにする。
						if (sln_cnt <= cnt) {
							all_hit = false;
							hit_pos = -1;
						}
					} else if (keyword[0] == '&') {	//&nbsp;や&#39;でヒットしないようにする
						var s = data[i][1].toLowerCase();
						if (s.length > hit_pos && s[hit_pos + 1] == '#') {
							all_hit = false;
							hit_pos = -1;
						} else if (keyword.substring(0, 2) == "&n") {
							all_hit = false;
							hit_pos = -1;
						} else if (keyword[1] == '&') {
							all_hit = false;
							hit_pos = -1;
						}
					} else if (keyword[0] == '\\' && keyword[1] == '#' && keyword[2] == '3' && keyword[3] == '9') {	//#39でヒットしないようにする
						all_hit = false;
						hit_pos = -1;
					} else if (keyword == ',') {
						all_hit = false;
						hit_pos = -1;
					}

				}
			}

			for (var j = 0; j < keywordReg.length; j++) {
				if (data[i].length == 6) {
					if (data[i][3].toLowerCase().search(keywordReg[j]) == -1) {
						ttl_hit = false;
					}
				}
			}

			//if((all_hit) && (m_type == data[i][1])){
			if (all_hit || ttl_hit) {

				if (count == 0) {
					// 表レイアウト用
					out_str += '<div class="sample_search_container">';
					// out_str += '<table class="content_table">';
					// out_str += '<tr><th>編</th><th>章</th><th>見出し</th></tr>';
				}

				var title_str;
				//if (data[i].length == 4 || (data[i].length == 5 && data[i][4] == "")) {
				if (data[i].length == 5) {
					//title_str = data[i][2];
					title_str = data[i][1];
				}

				var dataObj = data[i][1];
				var targetObj = data[i][1];

				if (targetObj == null)
					targetObj = "";
				var text = data[i][3];
				//firstの構成は {offset, key}
				var first = "";
				var str = keyword.replace(/[\\^$.*?+()[\]{}|]/g, '\\$&');
				var paragraph = [];
				if (data[i][3] !== "") {
					var key = keyword;

					var match = getIndexKeyWord(data[i][3], keyword_org);
					if (match.length > 0) {
						for (var t = 0; t < match.length; t++) {
							var index = match[t];
							var last = match[t] + keyword.length + 30;
							if (match[t] - 30 > 0) {
								index = match[t] - 30;
							}
							if (last >= data[i][3].length) {
								last = data[i][3].length;
							}
							paragraph.push(data[i][3].substring(index, last));
						}
					}
				}
				if (targetObj != null && targetObj != "") {
					if (paragraph.length > 0) {
						for (var k = 0; k < paragraph.length; k++) {
							var key = keyword;
							if (paragraph[k].toLowerCase().search(keyword_org.toLowerCase()) != -1) {
								count++;
								var highlight = "<strong>" + keyword_org + "</strong>"
								paragraph[k] = paragraph[k].replace(keyword_org, highlight);
								out_str += '<div class="sample_search">';
								out_str += '<p><a href="' + data[i][5] + '?q=' + keyword_org + '" target="_blank">' + targetObj + '</a></p>';
								out_str += '<p>' + paragraph[k] + '</p>';
								out_str += '</div>';
							}
						}
					} else {
						out_str += '<div class="sample_search">';
						out_str += '<p><a href="' + data[i][5] + '?q=' + keyword_org + '" target="_blank">' + targetObj + '</a></p>';
						out_str += '<p><strong>' + targetObj + '</strong></p>';
						out_str += '</div>';
					}
				}

				count++;
			}
		}

		if (count < 1) {
			out_str += '<h3>一致する語句がありませんでした。</h3>';
		} else {

		}
		//out_str += '</table>';
		out_str += '</div>';
		if (count > 0) {
			result.innerHTML = out_str;
		} else {
			result.innerHTML = out_str;
		}
		hihyoji();
		// フッター部設定(増山)
		//		footer_str += '<section id="footer">';
		footer_elm = footer.innerHTML;
		footer_str += '<p id="prev_link"><a href="#" onClick="changeFind(); return false;">前のページに戻る</a></p>';
		footer_str += '<p id="top_link"><a href="">ページトップに戻る</a></p>';	//20181116 大島
		footer_str += '<p id="copyright">Copyright &copy; 2018 FANUC CORPORATION</p>';
		//		footer_str += '</section>';
		var heightNav = $("#navTopSection").height();
        //var searchResult = $("#search_result").find("#result").css("padding-top");
        $("#search_result").find("#result").css("padding-top", heightNav + 130);
		footer.innerHTML = footer_str;
	} else {
		result.innerHTML = out_str;
	}
}

function getIndexKeyWord(str, keyword) {
	var regexp = new RegExp(keyword.toLowerCase(), "gi");
	var match, matches = [];
	while ((match = regexp.exec(str.toLowerCase())) != null) {
		matches.push(match.index);
	}
	return matches;
}