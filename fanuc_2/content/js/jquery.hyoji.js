// JavaScript Document


// ページを開いた時に表示ボックスを非表示にする（初期設定）
$(document).ready(function () {
	hihyoji();
});

// 他領域クリックで非表示
$(document).on('click touchend', function (event) {
	if ($(event.target).length > 0 && !$(event.target)[0].onclick) {
		hihyoji();
	}
});

function hihyoji(num) {
	var max_num = 500;
	for (var i = 0; i < max_num; i++) {
		var disp_num = "disp" + i;
		var hyoji_num = "hyoji" + i;
		var key_num = document.getElementsByClassName(hyoji_num).length;
		var objId = document.getElementById(disp_num);
		if (objId) {
			objId.style.display = "none";
		}
		document.getElementsByTagName("article")[0].style.paddingBottom = "92px";	//20181116 大島
		//		var footerId=document.getElementById("footer");
		var footerId = document.getElementsByTagName("footer")[0];	//20181116 大島
		if (footerId) {
			footerId.style.height = "";
		}
		for (var j = 0; j < key_num; j++) {
			document.getElementsByClassName(hyoji_num)[j].style.backgroundColor = "";
			document.getElementsByClassName(hyoji_num)[j].id = "";
		}
	}
}

// 用語説明表示ボックスを表示する
function hyoji(num) {
	//hihyoji();
	var max_num = 500;
	var disp_num = "disp" + num;
	var hyoji_num = "hyoji" + num;
	var key_num = document.getElementsByClassName(hyoji_num).length;	// 配列 hyoji_num の格納数を調べる
	var find = -1;
	for (var i = 0; i < max_num; i++) {
		var disp_i = "disp" + i;
		var hyoji_i = "hyoji" + i;
		var key_i = document.getElementsByClassName(hyoji_i).length;		// 配列 hyoji_num の格納数を調べる
		if (num == i) {
			find++;
			var objId = document.getElementsByClassName(hyoji_num)[0].id;
			if (objId == "xxx") {
				hihyoji();
				break;
			} else {
				hihyoji();
				if (document.getElementById(disp_num).style.display == "none") {
					document.getElementsByTagName("footer")[0].style.height = "120px";							// IDのスタイルでheightを120pxに変更する	//20181116 大島
					document.getElementsByTagName("article")[0].style.paddingBottom = "152px";		// IDのスタイルでpadding-bottomを152pxに変更する	//20181116 大島
					document.getElementById(disp_num).style.display = "block";						// IDのスタイルで表示に変更する
					for (var j = 0; j < key_num; j++) {
						document.getElementsByClassName(hyoji_num)[j].style.backgroundColor = "#ffd700";	// classのスタイルでバックグラウンドのカラーを黄色に変更する
						document.getElementsByClassName(hyoji_num)[j].id = "xxx";
					}
					break;
				}
			}
		}
	}
	if (find < 0) {
		document.getElementById(disp_i).style.display = "none";
		for (j = 0; j < key_i; j++) {
			document.getElementsByClassName(hyoji_i)[j].style.backgroundColor = "#fff";	// classのスタイルでバックグラウンドのカラーを変更する
		}
	}
}

// ページトップに戻るボタンを表示・非表示にする
$(document).ready(function () {
	document.getElementById('top_link').style.display = "none";

	$(window).scroll(function () {
		if ($(this).scrollTop() > 200) { // 200pxスクロールしたらページトップに戻るボタンを表示
			document.getElementById('top_link').style.display = "block";
		} else {
			document.getElementById('top_link').style.display = "none";
		}
	});
});

