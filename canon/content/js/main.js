String.prototype.trim = function() {
    return this.replace(/(^\s+|\s+$)/g, "")
}

Array.indexOf || (Array.prototype.indexOf = function(b) {
    for (var h = this.length, p = 0; p < h; p++)
        if (this[p] === b)
            return p;
    return -1
});

(function() {
    var b, h, p, m, f, r, v, l;
    window.device = {};
    h = window.document.documentElement;
    l = window.navigator.userAgent.toLowerCase();
    device.ios = function() {
        return device.iphone() || device.ipod() || device.ipad()
    };
    device.iphone = function() {
        return p("iphone")
    };
    device.ipod = function() {
        return p("ipod")
    };
    device.ipad = function() {
        return p("ipad")
    };
    device.android = function() {
        return p("android")
    };
    device.androidPhone = function() {
        return device.android() && p("mobile")
    };
    device.androidTablet = function() {
        return device.android() && !p("mobile")
    };
    device.blackberry = function() {
        return p("blackberry") || p("bb10") || p("rim")
    };
    device.blackberryPhone = function() {
        return device.blackberry() && !p("tablet")
    };
    device.blackberryTablet = function() {
        return device.blackberry() && p("tablet")
    };
    device.windows = function() {
        return p("windows")
    };
    device.windowsPhone = function() {
        return device.windows() && p("phone")
    };
    device.windowsTablet = function() {
        return device.windows() && p("donottouch")
    };
    device.fxos = function() {
        return p("(mobile; rv:") || p("(tablet; rv:")
    };
    device.fxosPhone = function() {
        return device.fxos() && p("mobile")
    };
    device.fxosTablet = function() {
        return device.fxos() && p("tablet")
    };
    device.mobile = function() {
        return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone()
    };
    device.tablet = function() {
        return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet()
    };
    device.portrait = function() {
        return 90 !== Math.abs(window.orientation)
    };
    device.landscape = function() {
        return 90 === Math.abs(window.orientation)
    };
    p = function(b) {
        return -1 !== l.indexOf(b)
    };
    f = function(b) {
        var f;
        return f = RegExp(b, "i"),
            h.className.match(f)
    };
    b = function(b) {
        return f(b) ? void 0 : h.className += " " + b
    };
    v = function(b) {
        return f(b) ? h.className = h.className.replace(b, "") : void 0
    };
    device.ios() ? device.ipad() ? b("ios ipad tablet") : device.iphone() ? b("ios iphone mobile") : device.ipod() && b("ios ipod mobile") : device.android() ? device.androidTablet() ? b("android tablet") : b("android mobile") : device.blackberry() ? device.blackberryTablet() ? b("blackberry tablet") : b("blackberry mobile") : device.windows() ? device.windowsTablet() ? b("windows tablet") : device.windowsPhone() ? b("windows mobile") : b("desktop") : device.fxos() ? device.fxosTablet() ? b("fxos tablet") : b("fxos mobile") : b("desktop");
    m = function() {
        return device.landscape() ? (v("portrait"),
            b("landscape")) : (v("landscape"),
            b("portrait"))
    };
    r = "onorientationchange" in window ? "orientationchange" : "resize";
    window.addEventListener ? window.addEventListener(r, m, !1) : window.attachEvent ? window.attachEvent(r, m) : window[r] = m;
    m()
}).call(this);

function fncGetSessionStorage(b) {
    try {
        if (sessionStorage) {
            var h = sessionStorage.getItem(b);
            null == h && (h = "");
            return h
        }
        return fncGetCookie(b)
    } catch (p) {
        return fncGetCookie(b)
    }
}

function fncSetSessionStorage(b, h) {
    try {
        sessionStorage ? sessionStorage.setItem(b, h) : fncSetCookie(b, h)
    } catch (p) {
        fncSetCookie(b, h)
    }
}

function fncGetWindowWidth() {
    try {
        var b = $(window).width();
        $(window).height();
        return b
    } catch (h) {}
}

function fncGetWindowHeight() {
    try {
        var b = window;
        if (window.opera)
            var h = b.innerHeight;
        else
            document.all ? h = b.document.body.clientHeight : document.getElementById && (h = b.innerHeight);
        return h
    } catch (p) {}
}

function fncGetLayoutMode() {
    var b = fncGetSessionStorage("layout-mode");
    b || (device.mobile() || device.tablet() ? b = "mobile" : (b = "pc",
        navigator.msMaxTouchPoints && 1 < navigator.msMaxTouchPoints && (b = "mobile")));
    return b
}

window.onresize = fncOnResize;

function fncOnResize() {
    try {
        var b = fncGetWindowWidth(),
            h = fncGetWindowHeight();
        if (-1 == document.location.search.indexOf("?sub=yes")) {
            var p = fncGetLayoutMode();
            document.getElementById("id_panel").style.height = h - 67 + "px";
            document.getElementById("id_content").style.height = h - 67 + "px";
            if ($('#id_tabs').css('display') == 'none') {
                document.getElementById("id_content").style.width = b - 0 + "px";
                document.getElementById("id_content").style.left = "0px";
            } else {
                document.getElementById("id_content").style.width = b - 300 + "px"
                document.getElementById("id_content").style.left = "300px";
            }
            "pc" == p ? document.getElementById("id_search_results").style.height = h - 118 + "px" : document.getElementById("id_search_results").style.height = h - 138 + "px";
            "pc" == p ? document.getElementById("id_toc") && (document.getElementById("id_toc").style.top = "0px",
                document.getElementById("id_toc").style.height = h - 120 + "px") : "pc" == p && "iframe" == toc_mode && document.getElementById("id_toc") && (document.getElementById("id_toc").style.top = "0px",
                document.getElementById("id_toc").style.height = h - 66 + "px");
            if ("HOME_TOC" == strWindowType) {
                var m = $(".right.b").get(0);
                m && m.parentNode.appendChild(m);
                $(".home").css("width", document.getElementById("id_content").style.width);
                var f = $("#pc_home").width();
                700 < f ? (b = f / 2 - 52,
                    "rtl" != langdir ? ($(".column .left").css({
                            width: b,
                            position: "absolute",
                            left: "0"
                        }),
                        $(".column .right").css({
                            width: b,
                            position: "absolute",
                            right: "0"
                        })) : ($(".column .left").css({
                            width: b,
                            position: "absolute",
                            right: "0"
                        }),
                        $(".column .right").css({
                            width: b,
                            position: "absolute",
                            left: "0"
                        })),
                    $(".column").each(function() {
                        var b = $(this).children(".left").outerHeight(),
                            f = $(this).children(".right").outerHeight();
                        b < f ? (b = f,
                            $(this).children(".left").height(b - 35)) : $(this).children(".right").height(b - 35);
                        $(this).css({
                            height: b
                        })
                    })) : ($(".column .left").css({
                        width: f - 40,
                        position: "static"
                    }),
                    $(".column .right").css({
                        width: f - 40,
                        position: "static"
                    }),
                    $(".column .left").parent().css({
                        height: "auto"
                    }),
                    $(".column .left").height("auto"),
                    $(".column .right").height("auto"))
            }
            if (document.getElementById("child_window_shadow") && "block" == document.getElementById("child_window_shadow").style.display.toLowerCase()) {
                if ("rtl" != langdir) {
                    var r = $("div.child_window_background").width() / 2;
                    $(".child_window_header").css({
                        left: document.body.clientWidth / 2 - r + "px"
                    });
                    $(".child_window").css({
                        left: document.body.clientWidth / 2 - r + "px"
                    });
                    $(".child_window").css({
                        height: $(window).height() - 180 - $(".child_window_header").height() + "px"
                    });
                    $(".child_window_background").css({
                        left: document.body.clientWidth / 2 - r + "px"
                    });
                    $(".child_window_background").css({
                        height: $(window).height() - 100 + "px"
                    });
                    $(".child_window_footer").css({
                        left: document.body.clientWidth / 2 - r + "px"
                    })
                } else
                    r = $("div.child_window_background").width() / 2,
                    $(".child_window_header").css({
                        left: document.body.clientWidth / 2 - r + "px"
                    }),
                    $(".child_window").css({
                        right: document.body.clientWidth / 2 - r + "px"
                    }),
                    $(".child_window").css({
                        height: $(window).height() - 180 - $(".child_window_header").height() + "px"
                    }),
                    $(".child_window_background").css({
                        right: document.body.clientWidth / 2 - r + "px"
                    }),
                    $(".child_window_background").css({
                        height: $(window).height() - 100 + "px"
                    }),
                    $(".child_window_footer").css({
                        right: document.body.clientWidth / 2 - r + "px"
                    });
                $(".child_window_footer").css({
                    top: $(window).scrollTop() + $(window).height() - 100 + "px"
                })
            }
        }
    } catch (v) {}
}

//1:もくじ
//2:検索
function fncShowButton(type) {
    var mode = $('#id_tabs').data('mode');
    if (mode == type && typeof mode != 'undefined') {
        if ($('#id_tabs').css('display') == 'none') {
            $('#id_tabs').css('display', 'table-cell');
            $('#id_panel').css('display', 'block');
            $('#id_tabs').next().attr('colspan', '1');
        } else {
            $('#id_tabs').css('display', 'none');
            $('#id_panel').css('display', 'none');
            $('#id_tabs').next().attr('colspan', '2');
        }
        fncOnResize();
    } else {
        mode = type;
        switch (type) {
            case 1:
                $('#icon_plus').css('display', 'inline');
                $('#icon_minus').css('display', 'inline');
                $('#id_panel_toc').css('display', 'block');
                $('#id_search').css('display', 'none');
                $('#id_search_button').css('display', 'none');
                $('#id_panel_search').css('display', 'none');
                break;
            case 2:
                $('#icon_plus').css('display', 'none');
                $('#icon_minus').css('display', 'none');
                $('#id_panel_toc').css('display', 'none');
                $('#id_search').css('display', 'inline');
                $('#id_search_button').css('display', 'inline');
                $('#id_panel_search').css('display', 'block');
                break;
        }

        $('#config').css('display', 'inline');
        $('#help').css('display', 'inline');
        $('#icon_toc').css('display', 'inline');
        $('#icon_search').css('display', 'inline');
        $('#id_res_bar_icon_previous').css('display', 'inline');
        $('#id_res_bar_icon_next').css('display', 'inline');
    }

    $('#id_tabs').data('mode', mode);
}

function fncCollapseTocAll() {
    $('#id_toc').find('li.level_1 .sign img').removeClass('chapter-sign-1').addClass('chapter-sign-2');
    $('#id_toc').find('li:not(.level_1) .sign img.toc-sign-1').removeClass('toc-sign-1').addClass('toc-sign-2');
    $('#id_toc').find('li:not(.level_1)').css('display', 'none');
}

function fncExpandTocAll() {
    $('#id_toc').find('li.level_1 .sign img').removeClass('chapter-sign-2').addClass('chapter-sign-1');
    $('#id_toc').find('li:not(.level_1) .sign img.toc-sign-2').removeClass('toc-sign-2').addClass('toc-sign-1');
    $('#id_toc').find('li:not(.level_1)').css('display', 'block');
}

function fncCollapseToc(li) {
    var pageId = $(li).data('page-id');
    var childs = $('#id_toc').find('li[data-parent="' + pageId + '"]');
    childs.each(function(index, item) {
        $(item).css('display', 'none');
        fncCollapseToc(item);
    });
}

function fncExpandToc(li) {
    $(li).css('display', 'block');
    var pageId = $(li).data('page-id');
    var childs = $('#id_toc').find('li[data-parent="' + pageId + '"]');
    childs.each(function(index, item) {
        $(item).css('display', 'block');
        if ($(item).find('.toc-sign').hasClass('toc-sign-1')) {
            fncExpandToc(item);
        }
    });
}

function fncAddIcon(li) {
    var img = $(li).find('img');
    if ($(img).hasClass('chapter-sign')) {
        if ($(img).hasClass('chapter-sign-1')) {
            $(img).removeClass('chapter-sign-1');
            $(img).addClass('chapter-sign-2');
        } else {
            $(img).removeClass('chapter-sign-2');
            $(img).addClass('chapter-sign-1');
        }
    } else if ($(img).hasClass('toc-sign')) {
        if ($(img).hasClass('toc-sign-1')) {
            $(img).removeClass('toc-sign-1');
            $(img).addClass('toc-sign-2');
        } else {
            $(img).removeClass('toc-sign-2');
            $(img).addClass('toc-sign-1');
        }
    }
}

function fncActionToc(li, isCollapse) {
    if (typeof isCollapse == undefined) {
        isCollapse = false;
    }

    fncAddIcon(li);
    if (isCollapse) {
        fncCollapseToc(li);
    } else {
        fncExpandToc(li);
    }
}

function fncInitToc(li) {
    var lis = [];
    lis.push(li);
    var parentId = $(li).data('parent');
    while (typeof parentId != 'undefined' && parentId != '') {
        var liParent = $('#id_toc').find('li[data-page-id="' + parentId + '"]');
        lis.push(liParent);
        parentId = $(liParent).data('parent');
    }

    for (var i = lis.length - 1; i >= 0; i--) {
        fncActionToc(lis[i], false);
    }
}

function fncGetResourceByResourceId(b) {
    try {
        for (var h = eval(resource), p = h.length, m = 0; m < p; m++)
            if (h[m].id == b)
                return h[m].value;
        return ""
    } catch (f) {}
}

function fncLoadResource() {
    var dicResource = {};
    for (var i = 0; i < resource.length; i++) {
        dicResource[resource[i].id] = resource[i].value;
    }

    // --------------------------------------------------------------------------------------------
    // HEADER
    // --------------------------------------------------------------------------------------------    
}

$(document).ready(function() {
    fncLoadResource();
    fncShowButton(1);
    fncCollapseTocAll();
    fncOnResize();
    fncEvent();
    fncInitToc($('#id_toc').find('li.current'));
    $('body').css('visibility', 'visible');
});

function fncEvent() {
    //すべて開く
    $(document).on('click', '#icon_plus', function() {
        fncExpandTocAll();
    });

    //title="すべてたたむ"
    $(document).on('click', '#icon_minus', function() {
        fncCollapseTocAll();
    });

    //もくじ
    $(document).on('click', '#icon_toc', function() {
        fncShowButton(1);
    });

    //検索
    $(document).on('click', '#icon_search', function() {
        fncShowButton(2);
    });

    //前へ、次へ
    $(document).on('click', '#id_res_bar_icon_previous, #id_res_bar_icon_next, #config, #help', function() {
        var href = $(this).data('href');
        if (typeof href != 'undefined' && href.trim() != '') {
            window.location.href = href;
        }
    });

    //Link
    $(document).on('click', '#id_toc li', function(e) {
        if (e.target.nodeName.toLocaleLowerCase() == 'img') {
            return false;
        }

        window.location.href = $(this).find('a[href!="#"]').attr('href');
    });

    //Chaper
    $(document).on('click', '.chapter-sign', function() {
        var li = $(this).closest('li');
        fncActionToc(li, $(this).hasClass('chapter-sign-1'));
    });

    //Toc
    $(document).on('click', '.toc-sign', function() {
        var li = $(this).closest('li');
        fncActionToc(li, $(this).hasClass('toc-sign-1'));
    });

    //Scroll
    $('#id_content').scroll(function() {
        setTimeout(function() {
            $("#action-pc").show()
            setTimeout(function() {
                $("#action-pc").hide();
            }, 3000)
        }, 100);
    });

    $(document).on('click', '#action-pc', function() {
        $('#id_content').scrollTop(0);
    });

}