﻿var resource = [
	// --------------------------------------------------------------------------------------------
	// HEADER
	// --------------------------------------------------------------------------------------------
	{
		id:		"canon_logo",
		value:	"<img src=\"images_common/hdr_canon_logo.gif\" title=\"CANON\" />"
	},{
		id:		"canon_logo_link",
		value:	"/"
	},{
		id:		"product_main_name",
		value:	"Satera"
	},{
		id:		"product_sub_name",
		value:	"LBP162 / LBP162L / LBP161"
	},{
		id:		"version",
		value:	""
	},{
		id:		"guide_name",
		value:	"ユーザーズガイド"
	},{
		id:		"toc_button",
		value:	"もくじ"
	},{
		id:		"home",
		value:	"トップページ"
	},{
		id:		"contents",
		value:	"もくじ"
	},{
		id:		"search",
		value:	"検索"
	},{
		id:		"portal",
		value:	"<p>本マニュアルを読まれる前に：</p><p>ウェブ検索結果から本マニュアルを見つけた場合は、適切なマニュアルであることを下記URLでご確認ください。</p><p><a href=\"https://oip.manual.canon\" target=\"_blank\">https://oip.manual.canon</a></p>"
	},{
		id:		"local2portal",
		value:	"<p>本書の改訂版が弊社Webサイトに掲載されることがあります。</p><p><a href=\"https://oip.manual.canon\" target=\"_blank\">https://oip.manual.canon</a></p>"
	},{
		id:		"com_address",
		value:	"<p>本機についてのご質問や修理のご依頼は、お買い上げ販売店または修理受付窓口へお問い合わせください。各種窓口は、キヤノンホームページ内のサポートページでご確認いただけます。</p><p><a href=\"https://global.canon\" target=\"_blank\">https://global.canon</a></p>"
	},{
		id:		"gb",
		value:	"<p>使用说明书</p><p>在使用本产品之前，请务必先仔细阅读本使用说明书。请务必保留备用。请在充分理解内容的基础上，正确使用。</p>"
	},{
		id:		"gb_name",
		value:	"激光打印机" // [彩色数码多功能复印/打印机|彩色数码复合机|彩色数码多功能复合机|数码多功能复印/打印机|数码多功能复合机|数码复合机|多功能传真一体机|数码打印机|激光多功能一体机|激光打印机|彩色激光打印机|彩色激光打印机（数码复合机）|多功能一体机|打印机]
	},{
		id:		"gb_place_of_origin",
		value:	"原产地：中国"
	},{
		id:		"gb_importer",
		value:	"进口商：佳能(中国)有限公司"
	},{
		id:		"gb_address",
		value:	"地址： 100005 北京市东城区金宝街89号金宝大厦2层"
	},{
		id:		"gb_revised",
		value:	"修订日期：2016. 6"
	},{
		id:		"bar_label_previous",
		value:	"前へ"
	},{
		id:		"bar_label_next",
		value:	"次へ"
	// --------------------------------------------------------------------------------------------
	// FOOTER
	// --------------------------------------------------------------------------------------------
	},{
		id:		"copy_right",
		value:	"Copyright CANON INC. 2019"
	},{
		id:		"pub_number",
		value:	"USRMA-3519-00"
	},{
		id:		"pub_number2",
		value:	"2019-03"
	// --------------------------------------------------------------------------------------------
	// MAIN
	// --------------------------------------------------------------------------------------------
	},{
		id:		"open_next_sibling",
		value:	"詳細を開く"
	},{
		id:		"close_next_sibling",
		value:	"詳細を閉じる"
	},{
		id:		"open_all",
		value:	"すべての詳細説明を表示"
	},{
		id:		"close_all",
		value:	"すべての詳細説明を非表示"
	},{
		id:		"close",
		value:	"閉じる"
	},{
		id:		"close_toc_all",
		value:	"すべてたたむ"
	},{
		id:		"open_toc_all",
		value:	"すべて開く"
	},{
		id:		"child_window_close",
		value:	"閉じる"
	// --------------------------------------------------------------------------------------------
	// SEARCH
	// --------------------------------------------------------------------------------------------
	},{
		id:		"search_found",
		value:	"件"
	},{
		id:		"enter_search_keyword",
		value:	"キーワードを入力"
	},{
		id:		"search_message_wait",
		value:	"検索中。しばらくお待ちください。"
	},{
		id:		"search_message_enter_search_keyword",
		value:	"キーワードを入力"
	},{
		id:		"search_message_more",
		value:	"もっと表示..."
	// --------------------------------------------------------------------------------------------
	// CONFIG
	// --------------------------------------------------------------------------------------------
	},{
		id:		"config_message_font_size",
		value:	"文字サイズ:"
	},{
		id:		"config_message_layout_mode",
		value:	"画面レイアウト:"
	},{
		id:		"config_message_layout_mode_mobile",
		value:	"モバイル"
	},{
		id:		"config_message_layout_mode_pc",
		value:	"PC"
	},{
		id:		"config_message_layout_mode_caution_1",
		value:	"メモ"
	},{
		id:		"config_message_layout_mode_caution_2",
		value:	"一部のモバイル機器では、PC用表示で操作に支障が出る場合があります。"
	},{
		id:		"config_message_toc_mode",
		value:	"目次表示:"
	},{
		id:		"config_message_toc_mode_all",
		value:	"すべての章"
	},{
		id:		"config_message_toc_mode_chapter",
		value:	"章単位"
	},{
		id:		"config_message_toc_mode_caution_1",
		value:	"メモ"
	},{
		id:		"config_message_toc_mode_caution_2",
		value:		"トップページで表示する目次は、常時\"すべての章\"です。"
	},{
		id:		"config_message_back",
		value:	"もどる"
	// --------------------------------------------------------------------------------------------
	// GROUP
	// --------------------------------------------------------------------------------------------
	},{
		id:		"FAQ",
		value:	"よくある質問（ＦＡＱ）"
	// --------------------------------------------------------------------------------------------
	// CATEGORY
	// --------------------------------------------------------------------------------------------
	},{
		id:		"ts_1",
		value:	"よくあるトラブル"
	},{
		id:		"ts_2",
		value:	"メッセージが表示された"
	},{
		id:		"ts_3",
		value:	"「#」ではじまるエラーコード（#xxx）が表示された"
	},{
		id:		"panel",
		value:	"ランプが点灯／点滅している"
	},{
		id:		"ts_7",
		value:	"印刷できない"
	},{
		id:		"jam",
		value:	"用紙がつまった"
	},{
		id:		"incorrect_prt",
		value:	"正しく印刷できない"
	},{
		id:		"paper",
		value:	"使用できる用紙について知りたい"
	},{
		id:		"ts_8",
		value:	"ネットワークがつながらない"
	},{
		id:		"ts_10",
		value:	"トナーカートリッジの交換方法を知りたい"
	},{
		id:		"ts_11",
		value:	"ドラムカートリッジの交換方法を知りたい"
	},{
		id:		"guarantee",
		value:	"保守サービスについて"
	// --------------------------------------------------------------------------------------------
	// WebManual
	// --------------------------------------------------------------------------------------------
	},{
		id:	"static_toc_html_template_1",
		value:	"<meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml; charset=UTF-8\" /><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/frame_toc.css\" /><script type=\"text/javascript\" src=\"../scripts/toc.js\"></script><script type=\"text/javascript\" src=\"../scripts/jquery.js\"></script><ul id=\"id_toc\" class=\"nocontent\"><li toc_id=\"home\" class=\"level_1\" title=\"%s\"><img src=\"../images/toc-icon.png\" class=\"toc-sign-0\" /><a target=\"_parent\" href=\"../frame_htmls/home.html\">%s</a></li%s></ul><div class=\"code nocontent\" id=\"%s\"></div>"
	},{
		id:	"static_toc_html_template_2",
		value:	"><li toc_id=\"%s\" class=\"level_%s\" title=\"%s\">%s<img src=\"../images/toc-icon.png\" class=\"%s-sign-%s\" />%s<a target=\"_parent\" href=\"../contents/%s\">%s</a></li"
	},{
		id:	"static_toc_html_template_3",
		value:	"<a href=\"#\" class=\"sign\" onclick=\"fncToggle(this); return false;\">"
	},{
		id:	"static_toc_html_template_4",
		value:	"</a>"
	},{
		id:		"static_toc_html_code",
		value:	"6168766083582342"
	// --------------------------------------------------------------------------------------------
	// PDFManual
	// --------------------------------------------------------------------------------------------
	},{
		id:		"PDF_guide_name",
		value:	"Users_Guide" // PDFマニュアル名の英語版名称を命名ルールに従って記述（英数半角と-_のみ使用可）
	}
];
var link_info = [
	// h1とは異なる文字をリンクタイトルに表示する必要がある場合はresource側に対応するidとvalueを定義する
	// --------------------------------------------------------------------------------------------
	// MAIN
	// --------------------------------------------------------------------------------------------
	{
		link_name:"ts_1",
		link_target:"_self"
	},{
		link_name:"ts_2",
		link_target:"_self"
	},{
		link_name:"ts_3",
		link_target:"_self"
	},{
		link_name:"ts_4",
		link_target:"_self"
	},{
		link_name:"ts_5",
		link_target:"_self"
	},{
		link_name:"ts_6",
		link_target:"_self"
	},{
		link_name:"ts_7",
		link_target:"_self"
	},{
		link_name:"ts_8",
		link_target:"_self"
	},{
		link_name:"ts_10",
		link_target:"_self"
	},{
		link_name:"ts_11",
		link_target:"_self"
	},{
		link_name:"panel",
		link_target:"_self"
	},{
		link_name:"jam",
		link_target:"_self"
	},{
		link_name:"incorrect_prt",
		link_target:"_self"
	},{
		link_name:"paper",
		link_target:"_self"
	},{
		link_name:"guarantee",
		link_target:"_self"
	},{
		link_name:"theme_top",
		link_target:"_self"
	},{
		link_name:"theme_01",
		link_target:"_self"
	},{
		link_name:"theme_02",
		link_target:"_self"
	},{
		link_name:"theme_04",
		link_target:"_self"
	},{
		link_name:"license_nw_pdf",
		link_href:"../pdfs/LBP162_lic_JPN_00.pdf"
	},{
		link_name:"route_map",
		link_href:"../pdfs/route_map.pdf"
	},{
		link_name:"GS",
		link_href:"../pdfs/LBP162_GS_ja_jp_R.pdf"
	// --------------------------------------------------------------------------------------------
	// FOOTER
	// --------------------------------------------------------------------------------------------
	},{
		link_name:"disclaimer",
		link_target:"_self",
		link_resource:"disclaimer",
		link_href:""
	},{
		link_name:"notice",
		link_target:"_self",
		link_resource:"notice",
		link_href:""
	}
];
var header = "config,help";
var constant = [
	{
		/* Remove "//" below to specify the language code. Simplified Chinese must be specified as "zh-CN". Traditional Chinese must be specified as "zh-TW". */
		//lang_code:					"xx-XX",
		asset_mode:					"local", // [web(default)|local]
		search_show_around:			50,
		search_show_result_count:	10,
		search_option_multibyte:	1,	// [0|1]
		join_chapters:				1,	// [0|1]
		ga_tracking_id:				"UA-65621281-1",
		ga_tracking_domain:			"auto", // oip.manual.canon
		frame_path:					"/assets/manual/frames/v1/ja/",
		frame_version:				"v2",
		template_path:				"css/", // template_typeがneの場合は「/assets/manual/templates/v1ne/ja/」それ以外は「/assets/manual/templates/v1/ja/」
		template_type:				"ne" // [ne|v2]
	}
];