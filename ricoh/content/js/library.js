$(document).ready(function() {
    $(document).on('click', '#main_box_control', function(e) {
        e.stopPropagation();
        if ($(this).hasClass('arrow-down')) {
            //Show
            $('#main_box_content').slideDown("400");
            $(this).removeClass('arrow-down');
            $(this).addClass('arrow-up');
        } else {
            //Hidden
            $('#main_box_content').slideUp("400");
            $(this).removeClass('arrow-up');
            $(this).addClass('arrow-down');
        }
    });

    $(document).on('click', '#main_box_look, #main_box_close a, #more', function() {
        $('#main_box_control').trigger('click');
    });
});