//
//
// ===== テーマスクリプト [Stone] =====
//
//


//
// === 初期実行 ===
//

$(function() {
'use strict';


  //
  // === 共通 ===
  //

  var
    $window = $(window),
    $body = $('body'),
    $header = $('#page-header'),
    $search = $('#page-search'),
    slideSpeed = 'normal';


  //
  // === メニュー ===
  //

  var
    $navMenuToc = $('#nav-menu-toc'),
    $navMenuHelp = $('#nav-menu-help'),
    $btnMenuToc = $('.btn-menu-toc'),
    $btnMenuHelp = $('.btn-menu-help'),
    classBehind = 'behind',

  // --- メニューの開閉 ---
  slideMenu = function($openMenu, $closeMenu) {

    if ($openMenu.is(':visible')) {
      $openMenu.slideUp(slideSpeed);
      return false;
    } else {
      if ($closeMenu.is(':visible')) {
        $closeMenu.slideUp(slideSpeed, function() {
          $openMenu.slideDown(slideSpeed);
        });
      } else {
        $openMenu.slideDown(slideSpeed);
      };
      return true;
    };

  };

  // --- 目次メニューの開閉 ---

  $btnMenuToc
    .click(function(e) {

    var opened = slideMenu($navMenuToc, $navMenuHelp);

    $btnMenuHelp.toggleClass(classBehind, opened);
    $btnMenuToc.removeClass(classBehind);

    return false;
  });

  // --- ヘルプメニューの開閉 ---

  $btnMenuHelp
    .click(function(e) {

      var opened = slideMenu($navMenuHelp, $navMenuToc);

      $btnMenuToc.toggleClass(classBehind, opened);
      $btnMenuHelp.removeClass(classBehind);

      return false;
    });



  //
  // === ウィンドウ ===
  //

  // --- ウィンドウイベント ---

  var
    $navMenuContent = $('.nav-menu-content'),
    menuBottomMargin = 180;


  $window

    // --- リサイズ ---
    .resize(function() {

      var
        headerHeight = $header.height(),
        searchHeight = $search.height();

      $navMenuContent
        .css('max-height', $window.innerHeight() - headerHeight - searchHeight - menuBottomMargin);

    })
    .trigger('resize');


});

// --- eof ---
