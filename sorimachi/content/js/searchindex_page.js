var HIGHT_LIGHT_STYLE  = "background-color: #B4EBFA;";
var KEYWORD_PARAMETER_NAME = "k";
var SPACE_NAME_PARAMETER_NAME = "spaceName";
var DISPLAYE_PAGE_INDEX_PARAMETER_NAME = "page";
var BACK_URL_PARAMETER_NAME = "backto";
var NUMBER_OF_PAGE_PARAMETER_NAME = "numberOfPage";
var SPACE_FIELD_ID = "spaceName";
var NUMBER_OF_PAGE_FIELD_ID = "numberOfPage";

var KEYWORD_LIMIT = 3;
var DISPLAY_PAGE_LIMIT = 200;
var SEARCH_RESULT_PAGE = "search.html";
var NUMBER_ROW_IN_PAGE = 2;
var paramterMap = {};
var searchResult = [];
var pageCount = 0;
var showpage = 0;
var CONTEXT_ID = "resultSearchContainner";

// search

var path = /search.htm$/;
var path2 = /$/;

$(document).ready(function(){
	try{
		// スペースのプルダウンを初期する
		initSpaceComboxbox();

		//パラメータを取得する)
		paramterMap　 = getParamterMap();

		//パラメータから項目にセットする
		setOldValue();

		NUMBER_ROW_IN_PAGE = parseInt($("#" + NUMBER_OF_PAGE_FIELD_ID).val(), 0);

		rerenderHtml();

		window.scrollTo(0,0);
		return;
	} catch (error) {
		console.log(error);
	}
});

function initSpaceComboxbox(){
	var spaceComboxObj = document.getElementById(SPACE_FIELD_ID);
	if(!isArray(spaces) || spaces.length <=1) {
		$(spaceComboxObj).hide();
		return;
	}
	if(isNullOrEmpty(spaceComboxObj)) return;
	spaceComboxObj.options[0] = new Option('All space', '');
	spaceComboxObj.options[0].selected = "selected";

	for (var i = 0; i < spaces.length; i++) {
		var space = spaces[i];
		spaceComboxObj.options[i + 1] = new Option(space[2], space[1]);
	}
}

function SearchWord_manual() {
	var keyword = document.getElementById("seek").value;
	var b = getCurrentHref();
	var bookname = "";
	location.href = SEARCH_RESULT_PAGE + '?' + KEYWORD_PARAMETER_NAME+ '=' + encodeURIComponent(keyword) + '&' + SPACE_FIELD_ID +'=' + encodeURIComponent(bookname) + '&'+BACK_URL_PARAMETER_NAME+ '=' + encodeURIComponent(b) + '#1';
}

function rerenderHtml() {
	var w = paramterMap[KEYWORD_PARAMETER_NAME];
	if(isNullOrEmpty(w)) return;
	w = w.replace(/　/g, " ");
	w = w.replace(/^\s+|\s+$/g, "").replace(/\s+/g, " ");

	var d = document.getElementById(CONTEXT_ID);

	var a = new Array();
	traverseDom(d, a);
	var n;
	while (n = a.pop()) {
		highlightKeyword(n, w);
	}
}

function checkSkipHighLight(c){
	var attr = getAttribute(c,"class");
	if(isNullOrEmpty(attr)){
		return false;
	}
	return attr.match("skipHighlight");
}

function traverseDom(root, a) {
	var c = root,
	n = null;
	var it = 0;
	do {
		n = c.firstChild;
		if (n == null || (c.nodeType == Node.ELEMENT_NODE && c.className == "relation") || checkSkipHighLight(n) || checkSkipHighLight(c)) {
			// visit c
			if (c.nodeType == Node.TEXT_NODE) {
				a.push(c);
			}
			// done visit c
			n = c.nextSibling;
		}

		if (n == null) {
			var tmp = c;
			do {
				n = tmp.parentNode;
				if (n == root)
					break;
				try {
					// visit n
					if (n.nodeType == Node.TEXT_NODE) {
						a.push(n);
					}
				} catch (e) {
					n = root;
					break;
				}
				// done visit n
				tmp = n;
				n = n.nextSibling;
			} while (n == null)
		}
		c = n;
	} while (c != root);
	return;
}

function highlightKeyword(n, w) {
	var b = w.split(' ');
	for (var i = 0; i < b.length; i++) {
		b[i] = quotemeta(b[i])
	}

	var ww = b.join('|');

	var a = n.nodeValue.split(new RegExp("(" + ww + ")", "i"));
	var htmlStr = "<span>";
	for (var i = 0; i < a.length; i++) {
		var currentElement;
		if (a[i].match(new RegExp(ww, "i"))) {
			htmlStr += "<span style='" + HIGHT_LIGHT_STYLE + "'>" + a[i] + "</span>";
		} else {
			htmlStr +=a[i];
		}
	}
	htmlStr += "</span>";
	n.parentNode.replaceChild($(htmlStr)[0], n);
}

function findParent(node, tag, classname) {
	var n = node;
	while (n != null) {
		var p = n.parentNode;
		if (p != null && p.nodeType == Node.ELEMENT_NODE &&
			p.nodeName.match(new RegExp(tag, "i")) && p.className == classname) {
			return true;
		}
		n = p;
	}
	return false;
}

function adjust() {
	var MacFireFox = false;
	var ua = navigator.userAgent.toLowerCase();
	if (ua.indexOf("mac") != -1 && ua.indexOf("firefox") != -1) {
		MacFireFox = true;
	}
	var MacChrome = false;
	if (ua.indexOf("mac") != -1 && ua.indexOf("chrome") != -1) {
		MacChrome = true;
	}
	var isOpera,
	isSafari = false;
	if (typeof(window.opera) != 'undefined')
		isOpera = true;
	if (navigator.userAgent.indexOf('Safari') > -1 && navigator.userAgent.indexOf('Chrome') == -1)
		isSafari = true;
	var tmp_s = document.getElementById(SPACE_FIELD_ID);
	setTimeout(function () {
		var m = $(tmp_s).innerWidth();
		if (!isSafari && !isOpera && !MacChrome)
			m += 2;
		// Cheetahパネルの場合+2する(上記での+2と合わせて計+4する)
		if (navigator.userAgent.indexOf("ricoh_mfpapnl") > -1)
			m += 2;
		m = Math.min(m, 395);
		if (!MacFireFox)
			$(tmp_s).width(m + 'px');
		else {
			m = Math.min($(tmp_s).outerWidth(), 395);
			$(tmp_s).width(m + 'px');
		}
	}, 0 * 1000); // need setTimeout for IE6.

	// To fix cursor.
	$(".linkimage").css('cursor', 'pointer');

	return;
}
