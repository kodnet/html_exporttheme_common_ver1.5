
/* last update 2015.03.26 */

$(function() {
	$("#nav li > div.listTitle img").css({'opacity':0});
	$("#nav li > div.listTitle").css( {"background-size": "contain"} );
	$("#nav > ul > li > div.listTitle").css( {"padding-left": "62px"} );
	$("i.icon_warning").css( {"background-size": "contain"} );
	$('#guide p.question_text').css( {"background-size": "auto"} );
	$('#guide p.question_text img').css( {"width": "25px" , "margin": "7px 0 0 7px"} );

	//$('ul.changeNav li#b_appearance a').css( {"background-size": "contain"} );
	$("#graphic div.point").hide();
	$(window).load(function() {
		setTimeout(function(){
			$('div.zoomImg div.imgTarget').css( {"background-size": "contain"} );
			$("#graphic div.point").show().css( {"background-size": "contain"});
			$("#nav li > div.listTitle img").css( {"width": "45px", "height":"auto",'opacity':1} );
		},300);

		$("#MF_form_phrase").keypress(function(ev) {
			
			if ((ev.which && ev.which === 13) ||
				(ev.keyCode && ev.keyCode === 13)) {
				ev.preventDefault();
				Library.makeSearchWord();
			}
		});
	});


	var timer = false;
	$(window).resize(function() {
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function() {
		   $('#nav').show();
		}, 500);
	});
});