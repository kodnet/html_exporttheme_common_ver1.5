
/**
*   last update 2015.11.12
*/


;$.fn.trans = function( xyList, speed, easing, callback ) {

    $.support.transition = typeof $("body").css("transitionProperty") === "string";

    if( $.support.transition ){
        this.transition(
            { x : xyList.x , y: xyList.y, 'marginLeft':xyList.marginLeft}
            , speed
            , easing
            , function(){
                if( callback ){
                    callback();
                }
            }
        );
    }else{
        this.animate(
            { 'left' : xyList.x , "top": xyList.y }
            , speed
            , easing
            , function(){
                if( callback ){
                    callback();
                }
            }
             );
    }
    return this;
};

// sp tablet用
var DIRECTION_TATE = 0;
var DIRECTION_YOKO = 1;
var WIDTH_TAB = 991;
var WIDTH_SP = 479;

var DEVICE_PC = 0;
var DEVICE_TAB = 1;
var DEVICE_SP = 2;


// Library
var Library = (function(){

    var _getDateTime = function(){
        return new Date().getTime() ;
    }

    return {



        getUA : function()
        {
            var userAgent = window.navigator.userAgent.toLowerCase();
            var appVersion = window.navigator.appVersion.toLowerCase();

            if (userAgent.indexOf('opera') != -1) {
              return 'opera';
            } else if (userAgent.indexOf('msie') != -1) {
                 //IE6～9
                if (appVersion.indexOf("msie 8.") != -1) {
                    return 'IE8';
                }else if (appVersion.indexOf("msie 9.") != -1) {
                    return 'IE9';
                }else{
                    return 'IE';
                }
            } else if (userAgent.indexOf('chrome') != -1) {
              return 'chrome';
            } else if (userAgent.indexOf('safari') != -1) {
              return 'safari';
            } else if (userAgent.indexOf('gecko') != -1) {
              return 'gecko';
            } else {
              return false;
            }
        },

        // UAによるデバイス取得 幅によるデバイス取得はgetDeviceFromWidth();
        getDeviceFromUA: function()
        {
            var ua = navigator.userAgent;
            if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
                return DEVICE_SP;
            }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
                return DEVICE_TAB;
            }else{
                return DEVICE_PC;
            }
        },

        matrixToArray:function( matrix )
        {
            if(matrix == "none" || typeof matrix == "undefined"){
                return false;
            }else{
                var values = matrix.split('(')[1];
                values = values.split(')')[0];
                values = values.split(',');
                var matrixList = new Object();
                matrixList.a = values[0];
                matrixList.b = values[1];
                matrixList.c = values[2];
                matrixList.d = values[3];
                matrixList.e = values[4];
                matrixList.f = values[5];

                return matrixList;
            }
        },
        layout: (function()
        {

            //ナビの高さを取得
            var _getNavHeight = function()
            {
                var windowHeight = _getWinInnerHeight();
                var headerHeight = $('#header').height();
                var targetHeight = windowHeight;
                if( $('#SPburger').css('display') == "none" ){
                    targetHeight -= headerHeight;
                }
                return targetHeight;
            }
            var setScrollPosition  = function()
            {
                var my = this;
                var scrollMax = $('#screen').height() - $('#footer').outerHeight() - _getWinInnerHeight() ;
                var wScroll = $(window).scrollTop();
                var navHeight = this.getNavHeight();
                if( wScroll > scrollMax){
                    if( _DEVICE_PC == _getDeviceFromWidth() ){
                        navHeight = navHeight - (wScroll - scrollMax);
                        $('#nav').height( navHeight );
                    }
                }else{
                    $('#nav').height( _getNavHeight() );
                }
            }
            //検索ボックスの設定
            var _setSearchNav = function()
            {
                var searchBox = $('#searchWord');
                $('#search').click(function(){
                    $(this).addClass( 'active' );
                    if( searchBox.val() == 'Search'){
                         searchBox.val('');
                    }
                });
                searchBox.blur(function() {
                    $('#search').removeClass( 'active' );
                    if( searchBox.val() == ''){
                        searchBox.val('Search')
                    }
                });
                var speed = 100;
                var serchBox = $('#search');
            }
            //sphoneのナビを設定
            var _setSPNav = function()
            {
                $('span#SPburger').click(function(){
                    var matrix = Library.matrixToArray( $('#main').css('transform') );
                    if(matrix && matrix.e != 0){
                        _hideNav();
                    }else{
                        _showNav();
                    }
                });
                $('#main').click(
                    function(){
                        var matrix = Library.matrixToArray( $('#main').css('transform'));
                        if(matrix && matrix.e != 0){
                            _hideNav();
                        }
                    }
                );
            }
            var _setNavIcon = function()
            {
                var targetTop = _getWinInnerHeight()/2;
                $('#navCloseIcon').css({'top': targetTop});
            }
            //SPHONE時ナビを非表示に
            var _hideNav = function( speed, callback )
            {
                if( $('#SPburger').css('display') == "block"  ){
                    if(!speed && speed != 0){
                        speed = 100;
                    }
                    $('#header').trans( { 'x':0 },speed ,'easeOutQuart');
                    $('#main').trans( { 'x':0 },speed ,'easeOutQuart', function(){
                        $('#main, #header').css({'transform':'none'});
                        $('#nav').removeClass('visible');
                        $('body').css( {'width': $(window).width()*1.01 });
                        if( callback ){ callback(); };
                        setTimeout(function(){
                            $('body').css( {'width': "auto" });
                            $('#screen').css({'overflow':'auto'});
                        },50);
                    } );
                    $('#nav').removeClass('show');//css({'z-index':-1});
                }else{
                    callback();
                }
            }
            //SPHONE時ナビを表示に
            var _showNav = function( speed, callback )
            {
                if(!speed && speed != 0){
                    speed = 200;
                }
                $('#nav').addClass('visible');
                $('#screen').css({'overflow':'hidden'})
                $('#main,#header').trans( { 'x':$('#nav').width() },speed ,'easeOutQuart',function(){
                    $('#nav').addClass('show');//.css({'z-index':1});
                });
                if( callback ){ callback(); };
            }
            //PC TABLET共通の目次ナビ
            var _setNav = function()
            {
                $('#nav ul ul').hide();
                var _touch = ('ontouchstart' in document) ? 'touchstart' : 'click';
                $('#nav li span.has_child').on(_touch,function(e){
                    var ul = $(this).siblings('ul');
                    var speed = 300;
                    if( ul.css('display') == "block" ){
                        ul.slideUp(speed);
                        $(this).removeClass('open');
                    }else{
                        ul.slideDown(speed);
                        $(this).addClass('open');
                    }
                });
                $("#container").swipe(
                    {
                    //Generic swipe handler for all directions
                    swipeLeft:function(event, direction, distance, duration, fingerCount) {
                        //_hideNav();
                    },
                    swipeRight:function(event, direction, distance, duration, fingerCount) {
                        if( distance > 150 && $('#SPburger').css('display') == "block" && 'ontouchstart' in window ){
                            _showNav();
                        }
                    },
                    //Default is 75px, set to 0 for demo so any distance triggers swipe
                    threshold:0
                });
                $('#main').on(_touch,function(e){
                    if( $('#nav').hasClass('visible') ){
                        e.preventDefault();
                        _hideNav();
                    }
                });

                //
                var breadcrumb = $('#breadcrumb-section').clone()
                breadcrumb.attr("id","breadcrumb-section-bottom");
                $('#main').append(breadcrumb);

                //next nav
                var $pager = $('<div>').attr('id','pager')
                var $ul = $('<ul>');
                $ul.prepend($('<li>').addClass('next').html('<a href="">&raquo;</a>'));
                $ul.prepend($('<li>').addClass('prev').html('<a href="">&laquo;</a>'));
                $pager.append( $ul);
                $('#main').append($pager);

                //現在表示中のメニューをアクティブにする
                var url = window.location.href;
                var path = window.location.href.match(".+/(.+?)([\?#;].*)?$")[1];
                var navHref = $('#nav div.navInner a');
                var count = navHref.length;
                if( path  == "/"){
                    path = "index.html";
                }
                navHref.each(function(i){
                    var href = $(this).attr('href');
                    // var isIndexPage = href.indexOf( "index.html" );
                    // if(isIndexPage >= 0) {
                    //     var parents_li = $(this).parents('li');
                    //     parents_li.children('ul').show();
                    // }
                    var result = href.indexOf( path );
                    if( result >= 0 ){
                        //var parents_li = $(this).parents('li');
                        //parents_li.first().addClass('current');
                        //$(this).addClass('current');
                        $(this).addClass('current');
                        var parents_li = $(this).parents('li');
                        parents_li.addClass('current').children('ul').show();
                        parents_li.children('span.has_child').addClass('open');
                        //pager 構築
                        var thisIndex = i;
                        var nextIndex = thisIndex + 1;
                        if( count-1 < nextIndex){
                            nextIndex = count-1;
                            $pager.find('li.next').addClass('is_disable');
                        }
                        var prevIndex = thisIndex - 1;
                        if( prevIndex < 0){
                            prevIndex = 0;
                            $pager.find('li.prev').addClass('is_disable');
                        }

                        var nextURL = navHref[nextIndex];
                        $pager.find('li.next a').attr('href', nextURL);
                        var prevURL = navHref[prevIndex];
                        $pager.find('li.prev a').attr('href', prevURL);

                        return false
                    }
                    
                    i++;
                });

                //closeアイコン設置
                $('#container').prepend( '<div id="navCloseIcon"></div>' );
                $('#navCloseIcon').on('click touchend',function(){
                    if( $('body').hasClass('is_full') ){
                        var that = $(this);
                        that.hide();
                        $('#nav').show().trans({ x:0 }, 200,function(){
                            that.fadeIn(100);
                        } );
                        $('body').removeClass('is_full');
                        _setNavOn();
                    }else{
                        var that = $(this);
                        that.hide();
                        $('#nav').trans({ x:-300}, 300,function(){
                            $(this).hide();
                            that.fadeIn(100);
                            _setNavOff();
                        })
                        $('body').addClass('is_full');
                    }
                });

                //opener
                $('#opener').on('click touchend',function(){
                    if( $('#opener').hasClass('active') ){
                        $('#nav ul ul').hide();
                        $('#nav span.has_child').removeClass('open');
                        $('#opener').removeClass('active');
                        $('#opener span.openLabel').html(CONST_TEXT.openall);
                    }else{
                        $('#nav ul ul').show();
                        $('#nav span.has_child').addClass('open');
                        $('#opener').addClass('active');
                        $('#opener span.openLabel').html(CONST_TEXT.closeall);
                    }
                });
            }

            var _setNavOn = function()
            {
                $.localStorage.set( 'is_navOpen', true );
            };
            var _setNavOff = function()
            {
                $.localStorage.set( 'is_navOpen', false );
            };
            var _checkNavOpen = function()
            {
                var is_navOpenFlag = $.localStorage.get('is_navOpen');

                if( is_navOpenFlag === null ){
                    _setNavOn();
                }
                if( is_navOpenFlag == false ){
                    $('#nav').trans({ x:-300},0,function(){
                        $(this).hide();
                    })
                    $('body').addClass('is_full');
                }
            }

            var _getWinInnerHeight = function()
            {
                var winH = window.innerHeight;
                if( Library.getUA() == "IE8"){
                     winH = document.documentElement.clientHeight;
                }
                return winH;
            }

            _setAnchor = function()
            {
                var hash = location.hash;
                var targetId = hash.replace('#', '');
                var speed = 300;

                if( $("[id ='"+ targetId +"' ]").length > 0 ){
                    var top =  $("[id ='"+ targetId +"' ]").offset().top;
                    $('body,html').animate({scrollTop:top - $('#header').height() - 30 }, speed, 'swing');
                }
                if( $("[name ='"+ targetId +"' ]").length > 0 ){
                    var top =  $("[name ='"+ targetId +"' ]").offset().top;
                    $('body,html').animate({scrollTop:top - $('#header').height() - 30 }, speed, 'swing');
                }
                $('a[href^=#]').click(function() {
                    // アンカーの値取得
                    var href= $(this).attr("href");
                    // 移動先を取得
                    var target = $(href == "#" || href == "" ? 'html' : href);
                    // 移動先を数値で取得
                    var position = target.offset().top - $('#header').height() - 30 ;
                    // スクロール
                    $('body,html').animate({scrollTop:position}, speed, 'swing');
                    return false;
                });
            }

            //レイアウト調整
            var _setPosition = function()
            {
                var my = this;
                var headerHeight = $('#header').outerHeight();
                $('#nav').height( _getNavHeight() ).css( {'top': headerHeight} );
                $('#container').css( {'padding-top': $('#header').outerHeight()} );
                $('#main').css( {'height': 'auto'} );
                var minHeight = $(window).height()-$('#header').height() ;
                if( $('#main').height() < minHeight ){
                    $('#main').height(minHeight);
                }
                //console.log($(window).innerHeight());
                if( _getWinInnerHeight() > $('#main').outerHeight() ){
                    var main = $('#main');
                    var padding = parseInt(main.css('padding-top')) + parseInt(main.css('padding-bottom'));
                    $('#main').height( _getWinInnerHeight() - $('#header').outerHeight() - padding )
                }
                if( $('#nav').hasClass('show') ){
                    _hideNav(0);
                }
            }
            return{
                init:function()
                {
                    _setPosition();
                    _setNav();
                    _setSPNav();
                    _setSearchNav();
                    _setNavIcon();
                    _checkNavOpen();
                    _setAnchor();
                },

                resize:function()
                {
                    _setPosition();
                    _setNavIcon();
                },
                hideNav:function( speed, callback )
                {
                    _hideNav( speed, callback );
                }
            }
        }())
//
    }
}());


////////////////////////////////////////////

//$.localStorage.removeAll();

$(function ($) {

    // DOM育成 window load後処理
    $(window).load(function(){
        //レイアウト調整
        Library.layout.init();
    });

    var timer ;
    $(window).resize(function() {
        if (timer !== false) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            Library.layout.resize();
        }, 50);
    });

    $(window).scroll(function(){
    });
});

