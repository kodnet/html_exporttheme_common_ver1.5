﻿var gtmCode = (function() {
    /*
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WD6S56" 
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WD6S56');</script>
    <!-- End Google Tag Manager -->
    <!-- START OF SmartSource Data Collector TAG v10.4.1 -->
    <!-- Copyright (c) 2014 Webtrends Inc.  All rights reserved. -->
    <script>
    window.webtrendsAsyncInit=function(){
        var dcs=new Webtrends.dcs().init({
            dcsid:"dcs222xzkv8dlacg62sbyhe8k_8m4z",
            domain:"statse.webtrendslive.com",
            timezone:0,
            i18n:true,
            adimpressions:true,
            adsparam:"WT.ac",
            offsite:true,
            download:true,
            downloadtypes:"xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
            onsitedoms:"oki.com",
            fpcdom:".oki.com",
            plugins:{
                //hm:{src:"//s.webtrends.com/js/webtrends.hm.js"}
            }
            }).track();
    };
    (function(){
        var s=document.createElement("script"); s.async=true; s.src="/printing/js/webtrends.min.js";    
        var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
    }());
    </script>
    <noscript><img alt="dcsimg" id="dcsimg" width="1" height="1" src="//statse.webtrendslive.com/dcs222xzkv8dlacg62sbyhe8k_8m4z.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=10.4.1&amp;dcssip=www.oki.com"/></noscript>
    <!-- END OF SmartSource Data Collector TAG v10.4.1 -->
    */
}).toString().match(/^[\s\S]*?\/\*([\s\S]*)\*\/[\s\S]*?$/)[1];
if (pageInfo.isHTTP && gtmCode)
    document.write(gtmCode);
$(document).ready(function() {
    $(document).find('#page a[href]:not([href^="#"])').attr('data-ajax', false);
    $(document).find('#nextPrev a[href]:not([href^="#"])').attr('data-ajax', false);
    $(document).find('#menu-html a[href]:not([href^="#"])').attr('data-ajax', false);
});