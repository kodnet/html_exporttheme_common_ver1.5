// 
// ---------------------


/* global template, txt, pageInfo, menu */

$(function(){
	
	//PC用テンプレート適用
	var setHtml = function(){
		
		var rep = function(reText, temp){
			var r = temp;
			for(var i in reText){
				var re = RegExp(reText[i][0], 'g');
				r = r.replace(re, reText[i][1]||'');
			}
			return r;
		};
		
		$('body').prepend('<div id="body"/>');
		
		var gnavi =$('body').is('.sfp')?[]:["co","sc","pr","fa"];
		var gNav = [];
		for (var i in gnavi) {
		    var m = menu[gnavi[i]];
		    if(m==void(0)){
                alert("js設定不良")
		    }

			i = Number(i);
			var gnum = i+1;
			gNav[gnum] = $(menu[gnavi[i]].titlelink).text(txt["gNav"+gnum]).attr('title', txt["gNavTitle"+gnum]).wrapInner('<span/>')[0].outerHTML;
		}
		var reText_header = [
			['@@usersmanual@@', $('header .usermanual').text()]
			,['@@search@@', txt.search]
			,['@@searchOption@@', $('#option .optionBtn').text()]
			,['@@close@@', txt.close]
			,['@@andSearch@@', $('#andSearch').parent('label').text()]
			,['@@orSearch@@', $('#orSearch').parent('label').text()]
			,['@@ZenHan@@', $('#ZenHan').parent('label').text()]
			,['@@charUpLo@@', $('#charUpLo').parent('label').text()]
			,['@@pMatch@@', $('#pMatch').parent('label').text()]
			,['@@gNav0@@',  '<img src="shared/img/icon_toppage.png" alt="'+ txt.gNavTitle0 +'" />']
			,['@@gNavTitle0@@', txt.gNavTitle0]
			,['@@gNav1@@', gNav[1]]
			,['@@gNav2@@', gNav[2]]
			,['@@gNav3@@', gNav[3]]
			,['@@gNav4@@', gNav[4]]
			,['@@sitemap@@', txt.sitemap]
			,['@@gNavTitleSitemap@@', txt.gNavTitleSitemap]
			,['@@global_printout@@', txt.printTitle]
			,['@@print@@', txt.print]
			,['@@gNavPrint@@', '<img src="shared/img/icon_printout.png" alt="'+txt.print+'" />']
			,['@@copyright@@', $('footer .copy').html()]
		];
		var header = rep(reText_header, template.header);
		
		var reText_article = [
			['@@localnavi_tab_toc@@', $('#localToc .paneltitle').attr('title')]
			,['@@toc@@', $('#searchResult .paneltitle').html()]
			,['@@localnavi_tab_searchresult@@', $('#searchResult .paneltitle').attr('title')]
			,['@@searchResult@@', $('#localToc .paneltitle').html()]
			,['@@colapseall@@', txt.colapseall]
			,['@@expandall@@', txt.expandall]
			,['___articletitle___', $('h1').html()]
			,['___article___', $('#pageInner').html()]
			,['___filename___', $('#contentsNo').text()]
		];
		var article = ($('body').is('#c1')) ? template.singleArticle : template.article;
		article = rep(reText_article, article);
		
		$('#body').append(header + article);
		
		$('body').children('div[data-role="page"], #localToc, #searchResult').remove();
		
	};
	
	//HTML生成
	setHtml();
});