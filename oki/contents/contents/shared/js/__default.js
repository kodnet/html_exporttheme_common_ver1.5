// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide
		Defalt JavaScript
 Modified : 2016-08-18
/////////////////////////////////////////////////////////////////////////////////////////////////*/


/* global get, txt, pageInfo, ua, debag, _isWebStrg, template, homeFile */

var menuShow = (get["m"]) ? false : true;
var menuHideWidth = 22;
var menuPosDef = {};

var defFunc = {

    //ページトップへボタン ------------------------------------------------------------
    "toTop": function(e) {
        $(e).append('<div id="toTop"><span title="' + txt.toTop + '"/></div>');
        $('#toTop span').on('click', function() {
            $('html,body').animate({ scrollTop: 0 }, 300, 'swing');
            $(this).blur();
        });
        var minWidthScroll = function() {
            if ($(window).width() < 889) {
                $('#toTop').css('top', function() {
                    return $(window).height() + $(window).scrollTop();
                });
            } else {
                $('#toTop').css('top', 'auto');
            }
        };
        $(window).on("load scroll resize", minWidthScroll);

        $(window).on("scroll", function() {
            // トップから250px以内はボタンを非表示にする
            if ($(this).scrollTop() < 250) {
                $("#toTop").hide();
            } else if ($(this).scrollTop() > 350) {
                // トップから350px以上スクロールしたらボタンを表示する
                $("#toTop").fadeIn();
            } else if (250 < $(this).scrollTop() && $(this).scrollTop() < 350) {
                // それ以外はフェードアウトする
                $("#toTop").fadeOut();
            }
        });
    },

    //メニュー構築 ------------------------------------------------------------
    "menu": function() {

        $('#Navi').tabs();
        $('#localMenu .list').html(function() {
            var html = $('#menu-html').html();
            $('#menu-html').remove();
            return html;
        });
        $("#localMenu .list a[href$='" + pageInfo.fileName + "']")
            .removeAttr('href')
            .closest("span,h2").addClass("selected")
            .closest("li").addClass("selectedList childSelected")
            .parents("li").addClass("childSelected");

        $('#localMenu>.list').listTree();



        var seleLI = $('#localMenu .selected').closest('li');
        var collexp = function(add, remove) {
            var a = add || 'collapsable';
            var r = remove || 'expandable';
            seleLI.removeClass(r).addClass(a);
            seleLI.children('.hitarea').removeClass(r + '-hitarea').addClass(a + '-hitarea');
            if (seleLI.is('.last' + a + ',.last' + r)) {
                seleLI.removeClass('last' + r).addClass('last' + a);
            }
        };
        if (seleLI.is('.expandable')) {
            /*seleLI.removeClass('expandable').addClass('collapsable');
            seleLI.children('.hitarea').removeClass('expandable-hitarea').addClass('collapsable-hitarea');
            if(seleLI.is('.lastexpandable')){
            	seleLI.removeClass('lastexpandable').removeClass('lastcollapsable');
            }*/
            collexp('collapsable', 'expandable');
        };

        $('#localMenu .selected a').on('click', function(e) {
            if (seleLI.is('.expandable')) {
                collexp('collapsable', 'expandable');
            } else {
                collexp('expandable', 'collapsable');
            }
        });

        if (!$('#localMenu li').is('.collapsable')) {
            $('#ExpandAll').removeClass('disabled').removeAttr('disabled');
            $('#CollapseAll').addClass('disabled').attr('disabled', 'disabled');
        }
        $(document).on('click', '.hitarea, #ExpandAll, #CollapseAll', function() {
            var exFlag = $('#localMenu .treeview li').is('.expandable');
            var coFlag = $('#localMenu .treeview li').is('.collapsable');
            if (exFlag && coFlag) {
                $('#CollapseAll, #ExpandAll').removeClass('disabled').removeAttr('disabled');
            } else if (exFlag || coFlag) {
                if (exFlag) {
                    $('#ExpandAll').removeClass('disabled').removeAttr('disabled');
                    $('#CollapseAll').addClass('disabled').attr('disabled', 'disabled');
                } else {
                    $('#ExpandAll').addClass('disabled').attr('disabled', 'disabled');
                    $('#CollapseAll').removeClass('disabled').removeAttr('disabled');
                }
            }
        });
    },

    //
    "menuPos": function(re) {
        var r = {};
        if (re === 'open') {
            r = {
                "opacity": 1,
                "NaviMarginLeft": delPx($('#Navi').css('marginLeft')),
                "categoryNaviWidth": $('#categoryNavi').width(),
                "containerWidth": $('#container').width(),
                "menuMove": $('#categoryNavi').width() - menuHideWidth
            };
        } else {
            r = {
                opacity: 0,
                NaviMarginLeft: "-" + menuPosDef.menuMove,
                categoryNaviWidth: menuHideWidth,
                containerWidth: menuPosDef.containerWidth + menuPosDef.menuMove
            };
        }

        return r;
    },

    //メニュースライド関数 ------------------------------------------------------------
    "menuShowHide": function() {
        $('#categoryNavi').prepend('<div id="menuShowHide"><span title="' + txt.closeMenu + '"/></div>');

        var posSet = {};
        var menuSlid = function(p) {
            if (p == 'hide') { //閉じ動作
                posSet = defFunc.menuPos('close');
            } else { //開き動作
                posSet = menuPosDef;
            }

            $('#Navi').animate({ 'marginLeft': posSet.NaviMarginLeft + "px", 'opacity': posSet.opacity }, 300, 'swing',
                function() {
                    if ($('#categoryNavi').is('.hide')) {
                        $('#menuShowHide').addClass('hide');
                        $('#Navi>ul').hide();
                    } else {
                        $('#menuShowHide').removeClass('hide');
                    }
                }
            );
            $('#categoryNavi').animate({ width: posSet.categoryNaviWidth + "px" }, 300, 'swing');
            $('#container').animate({ width: posSet.containerWidth + 'px' }, 300, 'swing');
        };

        $('#categoryNavi').on('click', '#menuShowHide span', function() {
            if (menuShow) { //閉じ
                $('#menuShowHide').children('span').attr('title', txt.openMenu);
                $('#categoryNavi').addClass('hide');
                menuSlid('hide');
                menuShow = false;
            } else { //開き
                $('#menuShowHide').children('span').attr('title', txt.closeMenu);
                $('#categoryNavi').removeClass('hide');
                $('#Navi>ul').show();
                menuSlid('show');
                menuShow = true;
            }
            defFunc.NextPrevLinkAdd();
        });
    },

    //ページ読み込み時にメニュー非表示 -----------------------------------------------
    "onLoadMenuHide": function() {
        $('#menuShowHide').addClass('hide').children('span').attr('title', txt.openMenu);
        $('#categoryNavi').addClass('hide');
        var posSet = defFunc.menuPos('close');
        $('#Navi').css({ 'marginLeft': posSet.NaviMarginLeft + "px", 'opacity': posSet.opacity });
        $('#categoryNavi').css({ width: posSet.categoryNaviWidth + "px" });
        $('#container').css({ width: posSet.containerWidth + 'px' });
    },

    //ページ送り・戻り ------------------------------------------------------------
    "NextPrev": function() {

        var src = '';
        var srcPrev = '';
        var srcNext = '';
        var thisPgaeIndex = $('#localMenu .list .selected a').index('#localMenu .list a');
        var menuLen = $('#localMenu .list a').length;
        var disabledElement = '<a class="disabled"></a>';

        var anchr = {
            "prev": function() {
                if (thisPgaeIndex <= 0) {
                    return disabledElement;
                } else {
                    var prevHref = $("#localMenu .list a:eq(" + (thisPgaeIndex * 1 - 1) + ")").attr('href');
                    return '<a href="' + prevHref + '" title="' + txt.prevPage + '" data-ajax="false"></a>';
                }
            },
            "next": function() {
                if (thisPgaeIndex < 0 || thisPgaeIndex >= menuLen - 1) {
                    return disabledElement;
                } else {
                    var nextHref = $("#localMenu .list a:eq(" + (thisPgaeIndex * 1 + 1) + ")").attr('href');
                    return '<a href="' + nextHref + '" title="' + txt.nextPage + '" data-ajax="false"></a>';
                }
            }
        };

        srcPrev = '<span class="prev">' + anchr.prev() + '</span>';
        srcNext = '<span class="next">' + anchr.next() + '</span>';
        src = '<div id="nextPrev">' + srcPrev + srcNext + '</div>';
        $('#container').prepend(src);

        defFunc.NextPrevLinkAdd();
    },

    //ページ送り・戻り にヘッダとメニューの表示情報付加 ---------------------------
    "NextPrevLinkAdd": function() {
        var s = [];
        if (!menuShow) {
            s.push('m=1');
        }
        var hm = (s.length) ? "?" + s.join("&") : "";
        $('#nextPrev span a').each(function(i, e) {
            if ($(e).attr('href')) {
                var url = $(e).attr('href').replace(/(\.html?)\?.+$/, "$1");
                $(e).attr('href', url + hm);
            }
        });
    },

    //シンプルサーチ ------------------------------------------------------------
    "SimpleSearch": function() {
        var targetTag = "h3, tr th:eq(1) p, .supplement";
        if ($('div').is('#simpleSearchForm')) {
            $('#sSearchKey').after('<span id="sSearchBtn"/>');
            $('#simpleSearchForm .count span').text('-');
            var simpleSearchType = '',
                notFoundMsg = '';
            if ($('#simpleSearch > div').is("#termSearch")) {
                simpleSearchType = 'term';
                notFoundMsg = txt.simpleSearchNotFound;
            } else if ($('#simpleSearch > div').is("#errMsg")) {
                simpleSearchType = 'errMsg';
                notFoundMsg = txt.simpleSearchReTry;
            }
            var EMindex = [];
            $('#simpleSearch div.item').find(targetTag).each(function(i, e) {
                EMindex[i] = $(e).text();
            });

            $('#simpleSearchForm').on('click', '.clear span', function() {
                $('#sSearchKey').val('');
                $('#simpleSearchForm form').submit();
            });

            $('#sSearchBtn').on('click', function() { $('#simpleSearchForm form').submit(); });
            $('#simpleSearchForm form').on('submit', function() {
                $('#simpleSearch .notFound').remove();
                $('#simpleSearch .hit').each(function(i, e) {
                    $(e).replaceWith($(this).html());
                });
                var and = true;
                var key = $.trim($('#sSearchKey').val()).replace(/[\s　]+?/, ' ');
                var keyword = key;

                $('#sSearchKey').val(key);

                if (!key) {
                    $('#simpleSearchForm .keyword_text').html('');
                    $('#simpleSearchForm .count span').text('-');
                    $('#simpleSearch div.item').show();
                    return false;
                }

                var opt = $('fieldset.option input');
                var option = [];
                $(opt).each(function(i, e) {
                    option[$(e).attr('id')] = ($(e).is(':checked')) ? true : false;
                });
                var charUpLo = option['op1'];
                var ZenHan = option['op2'];


                var escapeRegex = function(s) {
                    return s.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&");
                };

                if (!ZenHan && /[\!-\~！-～￥]/.test(key)) {
                    key = key.split('');
                    for (var i = 0, len = key.length; i < len; i++) {
                        key[i] = key[i].replace(/[\!-\~！-～￥]/, function(s) {
                            if (/[！-～]/.test(s)) {
                                return '(' + s + '|' + escapeRegex(String.fromCharCode(s.charCodeAt(0) - 0xFEE0)) + ')';
                            } else if (/[￥]/.test(s)) {
                                return '(' + s + '|' + escapeRegex(String.fromCharCode(0x005C)) + ')';
                            } else if (/[\\]/.test(s)) {
                                return '(' + escapeRegex(s) + '|' + String.fromCharCode(0xFFE5) + ')';
                            } else {
                                return '(' + escapeRegex(s) + '|' + String.fromCharCode(s.charCodeAt(0) + 0xFEE0) + ')';
                            }
                        });
                    }
                    key = key.join('');
                } else {
                    key = escapeRegex(key);
                }

                var op1 = (charUpLo) ? '' : 'i';

                var boldRe = function(k, g) {
                    g = g || '';
                    k = k.split(' ').join('|');
                    return new RegExp('(' + k + ')', g + op1);
                };

                var hitRe = boldRe(key, "g");

                var re = [];
                if (and) {
                    key = key.split(' ');
                    for (var i = 0, len = key.length; i < len; i++) {
                        if (key[i] == "") key.splice(i, 1);
                    }
                    for (var i = 0, len = key.length; i < len; i++) {
                        try { re[re.length] = new RegExp('(' + key[i] + ')', op1); } catch (e) {}
                    }
                } else {
                    re[0] = boldRe(key);
                }
                var count = 0;
                var searchItem = $('#simpleSearch div.item');
                var itemCount = searchItem.length;
                $('#simpleSearch div.item').find('.wbr').remove(); //内包していて表示に関係なく邪魔なものを削除

                for (var i = 0; i < itemCount; i++) {
                    var target = searchItem.eq(i);
                    var searchBlock = target.find(targetTag);
                    var str = searchBlock.text();
                    var Hit = 1;
                    for (var j = 0; j < re.length; j++) {
                        if (!re[j].test(str)) Hit--;
                    }
                    if (Hit > 0) {
                        //エラーメッセージ検索の検索範囲を広げたことによりハイライトするには
                        //タグとテキストを分離する必要があるため現状はやめ
                        //searchBlock.html(str.replace(hitRe, '<span class="hit">$&</span>'));

                        //.supplement h5、h3で中にタグなし条件でハイライト付加 by hira
                        /*for(var j=0, len=searchBlock.length; j<len; j++){
                        	if($(searchBlock[j]).is('h3:not(:has("*")), h5:not(:has("*"))')){
                        		var str = $(searchBlock[j]).text();
                        		$(searchBlock[j]).html(str.replace(hitRe, '<span class="hit">$&</span>'));
                        	}
                        }
                        */

                        target.show();
                        count++;
                    } else {
                        target.hide();
                    }
                }

                //for hight light section　ハイライトの対象が変わったため、処理を分離した。by sugi + hira
                targetHTag = "h3:not(:has('*')), tr th:eq(1) p:not(:has('*')), .supplement h5:not(:has('*'))"; //要素を内包してないものに限る
                $('#simpleSearch div.item:visible').find(targetHTag).each(function(i, e) {
                    var s = $(e).text();
                    $(e).html(s.replace(hitRe, '<span class="hit">$&</span>'));
                });

                //end

                $('#simpleSearchForm .keyword_text').html(keyword.replace(' ', '<br/>'));
                $('#simpleSearchForm .count span').text(count);
                if (!count) {
                    $('#simpleSearch').append('<div class="notFound">' + notFoundMsg + '</div>');
                }
                return false;
            });
        }
    }

};






///////////////////////////////////////////////////////////////////////////////////////////
$(function() {


    //ハイライト
    if (get["k"]) {
        var hilightOp = {
            "UpLo": (get["charUpLo"]) ? true : false,
            "ZenHan": (get["ZenHan"]) ? true : false,
            "pMatch": (get["pMatch"]) ? true : false
        };
        $('h1, #pageInner').keyHighlight(get["k"], hilightOp);
    }

    //クッキーの有効無効確認
    _isCookie = isCkeck();

    //検索
    searchFormFunc();

    //ページトップへボタン
    defFunc.toTop('#contentsInner');

    //便利な使い方 一押し
    $(document).on('click', '.tileLayoutA .block .link', function() {
        location.href = $(this).find('a').attr('href');
    });

    //メニューがある場合***********************
    if (isNavi()) {

        //メニュー
        defFunc.menu();
        menuPosDef = defFunc.menuPos('open');

        //メニュースライド
        defFunc.menuShowHide();

        //NextPrevボタン
        defFunc.NextPrev();

        //メニューの表示の引継ぎ
        if (!menuShow) {
            defFunc.onLoadMenuHide();
        }

    }
    //***********************

    //Simple Search
    defFunc.SimpleSearch();



    if (_isCookie || _isWebStrg) {
        //ページスクロール位置記憶読み込み
        //transition.load();
    }


    //ボタンクリックイベント===================================================================
    $(document).on('click', '.accordion-title, .supplement>h5, a[href], :disabled', function(e) {

        //PageNavi
        if ($(e.currentTarget).parents().is('.pagenavi')) {
            var t = $($(this).attr('href'));
            $('html,body').animate({ scrollTop: t.offset().top }, 300, 'swing');
            $(e.currentTarget).blur();
            return false;
        }

        //アコーディオン
        if ($(e.currentTarget).is('.accordion-title, .supplement>h5') &&
            $(e.currentTarget).closest('.supplement').is(':not(.non_child)')) {
            if ($(e.currentTarget).is('.open')) {
                $(e.currentTarget).removeClass('open').next('.accordion-contents, .supplementContents').slideUp("fast");
            } else {
                $(e.currentTarget).addClass('open').next('.accordion-contents, .supplementContents').slideDown("fast");
            }
        }

        //外部リンク[rel="external"]
        if ($(e.currentTarget).is('[href][rel="external"]')) {
            window.open($(this).attr('href'));
            return false;
        }

        //サイトマップのpagelink
        if ($(e.currentTarget).parents().is('#sitemap .pagelink')) {
            $('html,body').animate({ scrollTop: $(e.currentTarget.hash).offset().top }, 300, 'swing');
            $(this).blur();
            return false;
        }
        //jamp index
        if (/^\.\.\/index\.html$/.test($(e.currentTarget).attr('href'))) {
            var href = $(e.currentTarget).attr('href').replace(/(index)(\.html?)$/, homeFile);
            $(e.currentTarget).attr('href', href);
        }


        //
        if ($(e.currentTarget).is(':disabled')) {
            return false;
        };
        //return false;
    });
    //====================================================================================




    // 見た目調整
    lookfeel();


    //サイジング====================================================================================
    sizeing();
    if (ua._isIE8) {
        document.body.onresize = function() {
            sizeing();

            //IE8のFixedがおかしいへの対応
            var target = { "#footer": "bottom" };
            for (var key in target) {
                $(key).removeAttr('style')
                    .css(target[key], $(key).css(target[key]));
            }
            /*var targetKeys = Object.keys(target);
            for(var i=0, len=targetKeys.length; i<len; i++){
            	$(targetKeys[i]).removeAttr('style')
            		.css(target[targetKeys[i]], $(targetKeys[i]).css(target[targetKeys[i]]));
            }*/
        };
    } else {
        $(window).resize(function() {
            sizeing();
        });
    }
    //====================================================================================





    //check
    //	if(debag){
    //		if($('#localMenu>.list>ul').is('.treeview')){
    //			if($('.treeview .selected').text() != $('h1').text()) $('#page').css('background', "#f00");
    //		}
    //	}



}); //($function()の終了
///////////////////////////////////////////////////////////////////////////////////////////////////////