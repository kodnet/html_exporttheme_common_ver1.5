// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide 
		Load JavaScript
 Modified : 2016-02-27
/////////////////////////////////////////////////////////////////////////////////////////////////*/

/* global opera, decodeURIComponent, settings */


var initData = {
    "path": {
        "contents": "contents",
        "shared": "contents/shared",
        "local": "contents/local"
    },
    "topFileName": {
        "def": "index_p.html",
        "pc": "index_p.html",
        "mobile": "index_m.html"
    }
};



var init = (function() {


    var f = {
        "_setUAinfo": function() {
            var _na = navigator,
                _ua = _na.userAgent;
            var ua = {},
                bver;
            ua._isWin = /win/i.test(_na.platform);
            if (ua._isWin) {
                ua._isWin10 = /Win(dows )?NT 10\./i.test(_ua);
                ua._isWin8_1 = /Win(dows )?NT 6\.3/i.test(_ua);
                ua._isWin8 = /Win(dows )?NT 6\.2/i.test(_ua);
                ua._isWin7 = /Win(dows )?NT 6\.1/i.test(_ua);
                ua._isWinVista = /Win(dows )?NT 6\.0/i.test(_ua);
                ua._isWinSv2003 = /Win(dows )?NT 5\.2/i.test(_ua);
                ua._isWinXP = /Win(dows )?(NT 5\.1|XP)/i.test(_ua);
            }
            ua._isMac = /mac/i.test(_na.platform);
            if (ua._isMac) {
                var osver = _ua.match(/Mac OS X [0-9._]+/i);
                if (osver) {
                    osver = (osver + '').replace(/Mac OS X ([0-9._]+)/i, '$1');
                    osver = (/\./.test(osver)) ? osver.split('.') : osver.split('_');
                    ua['_isMacOSX' + osver[0]] = true;
                    osver2 = (!osver[1]) ? osver[0] + '_0' : osver[0] + '_' + osver[1];
                    ua['_isMacOSX' + osver2] = true;
                    osverFull = osver.join('_');
                    ua['_isMacOSX' + osverFull] = true;
                }
            }
            ua._isX11 = /X11/i.test(_ua);

            ua._isWebKit = /WebKit/i.test(_ua);
            ua._isChrome = /chrome/i.test(_ua);
            ua._isSafari = ua._isWebKit && !ua._isChrome;
            ua._isOpera = window.opera && opera.buildNumber;
            ua._isEdge = /Edge\//i.test(_ua);
            ua._isIE = !ua._isWebKit && !ua._isOpera && (/MSIE/i.test(_ua) || /Trident/i.test(_ua)) /* && (/Explorer/gi).test(_na.appName)*/ ;
            ua._isGecko = !ua._isWebKit && /Gecko/.test(_ua);
            ua._isFirefox = ua._isGecko && /Firefox/.test(_ua);


            ua._isiPhone = /iPhone/i.test(_ua);
            ua._isiPod = /iPod/i.test(_ua);
            ua._isiPad = /iPad/i.test(_ua);
            ua._isiOS = ua._isiPhone || ua._isiPod || ua._isiPad;
            ua._isAndroid = /Android/i.test(_ua);
            ua._isMobile = false;
            if (ua._isiPhone || ua._isiPod || ua._isiPad || ua._isAndroid) {
                ua._isMobile = true;
            }

            var width = window.innerWidth;
            if (ua._isMobile == false) {
                if (width <= 1024) {
                    ua._isMobile = true;
                } else {
                    ua._isMobile = false;
                }
            }

            if (ua._isSafari) {
                ua._isSafari1 = /WebKit\/(85|10[36]|12[45]|312)(|u|\.[0-9.]+) /i.test(_ua);
                ua._isSafari2 = /WebKit\/4[1-5][0-9.+]+ /i.test(_ua);
                bver = '';
                bver = _ua.match(/Version\/[0-9.]+ /i);
                if (bver) {
                    bver = (bver + '').replace(/Version\/([0-9.]+?) /i, '$1').split('.');
                    ua['_isSafari' + bver[0]] = true;
                    bver2 = (!bver[1]) ? bver[0] + '_0' : bver[0] + '_' + bver[1];
                    ua['_isSafari' + bver2] = true;
                    bverFull = bver.join('_');
                    ua['_isSafari' + bverFull] = true;
                }
                ua._isSafari3_0low = ua._isSafari1 || ua._isSafari2 || ua._isSafari3_0;
                ua._isSafari5low = ua._isSafari3_0low || ua._isSafari3 || ua._isSafari4 || ua._isSafari5;
                ua._isSafari6low = ua._isSafari5low || ua._isSafari6;
                ua._isSafari6hi = !ua._isSafari5low;
            }

            if (ua._isIE) {
                bver = '';
                bver = _ua.match(/(MSIE |rv:)[0-9]+/i);
                if (bver) {
                    bver = (bver[0] + '').replace(/(MSIE |rv:)([0-9]+)/i, '$2');
                    if (bver == '5') bver = '6';
                    ua['_isIE' + bver] = true;
                }
            }

            if (ua._isFirefox) {
                bver = '';
                bver = _ua.match(/Firefox\/[0-9.]+/i);
                if (bver) {
                    bver = (bver + '').replace(/Firefox\/([0-9.]+)/i, '$1').split('.');
                    ua['_isFirefox' + bver[0]] = true;
                    bver2 = (!bver[1]) ? bver[0] + '_0' : bver[0] + '_' + bver[1];
                    ua['_isFirefox' + bver2] = true;
                    bverFull = bver.join('_');
                    ua['_isFirefox' + bverFull] = true;
                }
                ua._isFirefox7low = ua._isFirefox1 || ua._isFirefox2 || ua._isFirefox3 || ua._isFirefox4 || ua._isFirefox5 || ua._isFirefox6 || ua._isFirefox7;
            }

            if (ua._isChrome) {
                bver = '';
                bver = _ua.match(/Chrome\/[0-9.]+/i);
                if (bver) {
                    bver = (bver + '').replace(/Chrome\/([0-9.]+)/i, '$1').split('.');
                    ua['_isChrome' + bver[0]] = true;
                    bver2 = (!bver[1]) ? bver[0] + '_0' : bver[0] + '_' + bver[1];
                    ua['_isChrome' + bver2] = true;
                    bverFull = bver.join('_');
                    ua['_isChrome' + bverFull] = true;
                }
            }

            if (ua._isiPhone || ua._isiPad || ua._isiPod) {
                bver = '';
                bver = _ua.match(/OS [0-9_]+/i);
                if (bver) {
                    bver = (bver + '').replace(/OS ([0-9_]+)/i, '$1').split('_');
                    ua['_isiOS' + bver[0]] = true;
                    bver2 = (!bver[1]) ? bver[0] + '_0' : bver[0] + '_' + bver[1];
                    ua['_isiOS' + bver2] = true;
                    bverFull = bver.join('_');
                    ua['_isiOS' + bverFull] = true;
                }
            }

            if (ua._isAndroid) {
                bver = '';
                bver = _ua.match(/Android [0-9.]+/i);
                if (bver) {
                    bver = (bver + '').replace(/Android ([0-9.]+)/i, '$1').split('.');
                    ua['_isAndroid' + bver[0]] = true;
                    bver2 = (!bver[1]) ? bver[0] + '_0' : bver[0] + '_' + bver[1];
                    ua['_isAndroid' + bver2] = true;
                    bverFull = bver.join('_');
                    ua['_isAndroid' + bverFull] = true;
                }
            }
            return ua;
        },
        "_pageInfo": function() {
            var pageInfo = {};
            pageInfo.fileName = document.location.pathname.replace(/^.+(\/|\\)([^\/\\]+)$/i, "$2");
            if (/\/$/.test(pageInfo.fileName)) pageInfo.fileName = initData.topFileName.def;
            pageInfo.file = pageInfo.fileName.replace(/\.html?$/, ""); //拡張子抜き
            pageInfo.siteTop = (/^index/.test(pageInfo.file)) ? true : false;
            pageInfo.topPageType = false;
            if (pageInfo.siteTop) {
                if (initData.topFileName.pc == pageInfo.fileName) {
                    pageInfo.topPageType = "pc";
                } else if (initData.topFileName.mobile == pageInfo.fileName) {
                    pageInfo.topPageType = "mobile";
                }
            }
            pageInfo.id = pageInfo.fileName.split("_", 1).join('');
            pageInfo.hash = location.hash;
            pageInfo.getdata = location.search.replace(/^\?/, '');
            pageInfo.get = [];
            if (pageInfo.getdata) {
                pageInfo.getdata = pageInfo.getdata.split('&');
                for (var i = 0, len = pageInfo.getdata.length; i < len; i++) {
                    temp = decodeURIComponent(pageInfo.getdata[i]).split('=');
                    ak = temp[0];
                    temp.shift();
                    pageInfo.get[ak] = temp.join('=');
                }
            }
            pageInfo.isHTTP = /https?:/.test(document.location.protocol);
            pageInfo.isTop = (/^index.*$/.test(pageInfo.file)) ? true : false;

            var pathTemp = {};
            if (!pageInfo.isTop) {
                var re = RegExp('^(\.\/)?' + initData.path.contents + '\/?');
                for (var k in initData.path) {
                    pathTemp[k] = initData.path[k].replace(re, '');
                }
            }
            pageInfo.path = f.extend(initData.path, pathTemp);

            pageInfo.lang = settings.language + ((settings.country) ? '-' + settings.country : '');
            pageInfo._lang = (pageInfo.lang) ? '_' + pageInfo.lang : '';


            return pageInfo;
        },
        "extend": function(dest, source) {
            for (var property in source) {
                dest[property] = source[property];
            }
            return dest;
        },
        "_GetCookies": function() {
            var r = [];
            var cookies = document.cookie;
            if (cookies != '') {
                cookies = cookies.split('; ');
                for (var i = 0, len = cookies.length; i < len; i++) {
                    var cookie = cookies[i].split('=');
                    if (cookie[0] == 'okiManualDevice') {
                        if (location.href.indexOf(initData.topFileName.pc) > -1 || location.href.indexOf(initData.topFileName.mobile) > -1) {
                            r[cookie[0]] = decodeURIComponent(location.href.indexOf(initData.topFileName.pc) > -1 ? 'pc' : 'mobile');
                            continue;
                        }
                    }
                    r[cookie[0]] = decodeURIComponent(cookie[1]);
                }
            }

            return r;
        }
    };
    return {
        "ua": f._setUAinfo,
        "pageInfo": f._pageInfo,
        "getCookies": f._GetCookies
    };
}());

var ua = init.ua();
var pageInfo = init.pageInfo();

var get = undefined;
var _isCookie = false; //後でdefault.jsでチェック。
var _isWebStrg = false;
_isWebStrg = typeof sessionStorage !== 'undefined';

var cookiePath = undefined;
var cookieDevice = undefined;
if (location.href.indexOf(initData.topFileName.pc) > -1 || location.href.indexOf(initData.topFileName.mobile) > -1) {
    cookieDevice = location.href.indexOf(initData.topFileName.pc) > -1 ? 'pc' : 'mobile';
}

var homeFile = undefined;
var srCountType = 0;
var loadPath = undefined;
var loadFile = [];
var htmlBody = undefined;

function detectDeviceMode(isResize) {
    if (typeof htmlBody == 'undefined') {
        htmlBody = document.body.innerHTML;
    }
    cookieDevice = init.getCookies()['okiManualDevice'];
    ua = init.ua();
    pageInfo = init.pageInfo();
    get = pageInfo.get;
    pageInfo.viewMobile = ua._isMobile;
    var isMobileDevice = cookieDevice;
    if (saveIsMobile == ua._isMobile) {
        return;
    }

    if (isResize == true) {
        document.body.innerHTML = htmlBody;
        document.body.className = 'sfp';
        document.head.className = '';
        document.getElementsByTagName('html')[0].className = '';
    }
    _isCookie = false; //後でdefault.jsでチェック。
    _isWebStrg = false;
    _isWebStrg = typeof sessionStorage !== 'undefined';
    loadPath = pageInfo.path;
    var srCountType = 0;
    if (pageInfo.lang == 'zh-tw') srCountType = 1;
    cookiePath = '; path=' + window.location.pathname.replace(/\/contents\/(contents\/)?.*$/, '');
    homeFile = (pageInfo.viewMobile) ? initData.topFileName.mobile : initData.topFileName.pc;
    document.cookie = 'okiManualDevice=' + isMobileDevice + cookiePath;
    cookieDevice = init.getCookies()['okiManualDevice'];
    if (location.href.indexOf(initData.topFileName.pc) > -1 || location.href.indexOf(initData.topFileName.mobile) > -1) {
        cookieDevice = location.href.indexOf(initData.topFileName.pc) > -1 ? 'pc' : 'mobile';
    }
    if (typeof cookieDevice == 'undefined') {
        cookieDevice = saveIsMobile;
    }

    /*
     * PC, mobile 分岐
     */
    //cookieがある場合
    if (cookieDevice) {
        if ("pc" == cookieDevice) {
            pageInfo.viewMobile = false;
            document.cookie = 'okiManualDevice=pc' + cookiePath;
            if (pageInfo.topPageType == "mobile") {
                location.href = initData.topFileName.pc;
            }
        } else if ("mobile" == cookieDevice) {
            pageInfo.viewMobile = true;
            document.cookie = 'okiManualDevice=mobile' + cookiePath;
            if (pageInfo.topPageType == "pc") {
                location.href = initData.topFileName.mobile;
            }
        }
    }
    //cookieがない場合
    else {
        //cookieにアクセス端末情報["pc"|"mobile"]を書き込み
        document.cookie = 'okiManualDevice=' + ((ua._isMobile) ? 'mobile' : 'pc') + cookiePath;
        cookieDevice = init.getCookies()['okiManualDevice'];
        //アクセスがモバイル端末で表示がPCトップページの場合にモバイルトップへ遷移
        if (ua._isMobile && pageInfo.topPageType == "pc") {
            location.href = initData.topFileName.mobile;
        }
        if (pageInfo.topPageType == "mobile") {
            pageInfo.viewMobile = true;
            cookieDevice = 'mobile';
        } else if (pageInfo.topPageType == "pc") {
            pageInfo.viewMobile = false;
            cookieDevice = 'pc';
        }
    }

    var body = document.getElementsByTagName('body')[0];
    var div = document.getElementById('css-js');
    if (typeof div != 'undefined' && div != null) {
        div.parentElement.removeChild(div);
    }

    div = document.createElement('div');
    div.setAttribute('id', 'css-js');
    body.appendChild(div);
    if (pageInfo.viewMobile) {
        loadCss(loadPath.shared + '/css/jquery.mobile-1.4.5.min.css');
        loadCss(loadPath.shared + '/css/bootstrap.min.css');
        loadCss(loadPath.shared + '/css/mobile.css');
        loadCss(loadPath.shared + '/css/anki_m.css');

        loadScript(loadPath.local + '/' + pageInfo.lang + '/js/text' + pageInfo._lang + '.js');
        loadScript(loadPath.shared + '/js/jquery-2.1.4.min.js');
        loadScript(loadPath.shared + '/js/jquery-ui-forAutocomp.min.js');
        loadScript(loadPath.shared + '/js/jquery.mobile-1.4.5.min.js');
        loadScript(loadPath.shared + '/js/bootstrap.min.js');
        loadScript(loadPath.shared + '/js/jquery.keyHighlight.min.js');
        loadScript(loadPath.shared + '/js/common.js');
        loadScript(loadPath.shared + '/js/search.js');
        loadScript(loadPath.shared + '/js/mobile.js');

        if (pageInfo.isTop) {
            loadScript(loadPath.shared + '/js/mobile_top.js');
        }
    } else {
        //css
        if (!pageInfo.isTop) {
            loadCss(loadPath.shared + '/css/jquery-ui.css');
            loadCss(loadPath.shared + '/css/font.css');
            loadCss(loadPath.shared + '/css/pc.css');
            loadCss(loadPath.shared + '/css/anki.css');
            loadCss(loadPath.local + '/' + pageInfo.lang + '/css/' + pageInfo.lang + '.css');
            loadCss(loadPath.shared + '/css/print.css', 'print');
        }

        //js
        loadScript(loadPath.shared + '/js/jquery-1.11.3.min.js');
        loadScript(loadPath.shared + '/js/jquery-ui-1.11.4.min.js');
        loadScript(loadPath.shared + '/js/jquery.cookie.js');
        loadScript(loadPath.shared + '/js/jquery.keyHighlight.min.js');
        loadScript(loadPath.shared + '/js/jquery.listTree.js');
        loadScript(loadPath.local + '/' + pageInfo.lang + '/js/text_' + pageInfo.lang + '.js');
        loadScript(loadPath.shared + '/js/common.js');
        loadScript(loadPath.shared + '/js/function.js');
        if (pageInfo.isTop) {
            loadScript(loadPath.shared + '/js/search.js');
            loadScript(loadPath.shared + '/js/__top.js');

        } else {
            loadScript(loadPath.shared + '/js/template_pc.js');
            loadScript(loadPath.shared + '/js/set_html.js');
            loadScript(loadPath.shared + '/js/search.js');
            loadScript(loadPath.shared + '/js/__default.js');

        }
    }

    loadScript(loadPath.shared + '/sysout/gtmCode.js');
    body.appendChild(div);
}

function loadScript(src) {
    var script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.src = src;
    script.async = false;
    var div = document.getElementById('css-js');
    div.append(script);
}

function loadCss(href, media) {
    var elLink = document.createElement('link');
    elLink.rel = 'stylesheet';
    elLink.type = 'text/css';
    elLink.href = href;
    if (typeof media == 'undefined') {
        elLink.setAttribute('media', 'print,screen');
    } else {
        elLink.setAttribute('media', media);
    }

    var div = document.getElementById('css-js');
    div.append(elLink);
}

window.onload = detectDeviceMode;
var saveIsMobile;
window.onresize = function detectDivice() {
    saveIsMobile = ua._isMobile;
    if (location.href.indexOf(initData.topFileName.pc) > -1 && ua._isMobile == true) {
        location.href = initData.topFileName.mobile;
    }
    if (location.href.indexOf(initData.topFileName.mobile) > -1 && ua._isMobile == false) {
        location.href = initData.topFileName.pc;
    }
    detectDeviceMode(true);
}