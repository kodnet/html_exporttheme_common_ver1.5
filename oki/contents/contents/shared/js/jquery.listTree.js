// 
// ---------------------
/* リストツリー ------------------------------------
 * 	
 * 	$([target]).listTree([options]);
 * 		target
 * 			selector ([LIST Parent] > ul) | ([LIST Parent]) 例）'#hoge > ul' | '#hoge'
 * 		options{
 * 			[level: (num)|'full'],
 * 			[collapsed: true|false ],
 * 			[strClick: false|true ]
 * 		}
 * 
 * target    = [必須] ターゲットのUL、もしくはその親] 例）"#naviTree>ul" 
 * level     = デフォルト[full]  ツリー開閉を一番親から何階層までにするか(数値以外は全階層)  例）"3" "full"
 * collapsed = デフォルト[true]  初期の開閉状態 falseの場合のみ全開  例）[全開]false [閉じ]"full"、true
 * strClick  = デフォルト[false] 文字列でも開閉動作させる  例）[させない]false [させる]true
 * 
 * 
 * HTMLコード
 * <html>
 * <head>
 * <script src="jquery.js" type="text/javascript"></script>
 * <script src="jquery.listTree.js" type="text/javascript"></script>
 * <script type="text/javascript">
 * $(function(){
 * 	$('#hoge > ul').listTree();
 * });
 * </script>
 * </head>
 * <body>
 * <div id="hoge">
 * 	<ul>
 * 		<li><span><a href="" data-ajax="false">***</a></span></li>
 * 		<li>
 * 			<ul>
 * 				<li><span><a href="" data-ajax="false">***</a></span></li>
 * 			</ul>
 * 		</li>
 * 		<li><span><a href="" data-ajax="false">***</a></span></li>
 * 	</ul>
 * </div>
 * </body>
 * </html>
 * 
 * 
 * 
 * Create by k.Hiramatsu
 */
(function($) {

    $.fn.listTree = function(options) {
        if (this.length && this && this.length) {
            var target = this;

            var defaults = {
                level: "full",
                collapsed: true,
                strClick: false
            };

            var settings = $.extend({}, defaults, options);

            var treeLevel = settings.level;
            var collapsed = settings.collapsed;
            var strlink = (settings.strClick) ? ', .opcl' : '';

            if ($(target).is('ul')) {
                $(target).addClass('treeview');
            } else {
                $(target).children("ul").addClass('treeview');
            }

            excol = (collapsed) ? 'expandable' : 'collapsable';


            if ((settings.level + "").match(/[0-9]+/)) {
                var tlevel = '';
                for (i = 0; i < treeLevel; i++) {
                    $('ul.treeview > li' + tlevel).each(function(j) {
                        if ($(this).children().is('ul')) {
                            if (settings.strClick) $(this).children('span').addClass('opcl');
                            $(this).addClass(excol).prepend('<div class="hitarea ' + excol + '-hitarea"/>');
                            $(this).filter(':last-child').addClass('last' + excol);
                        }
                    });
                    tlevel += '>ul>li';
                }
            } else {
                var cLI = $('.treeview ul').closest("li");
                if (settings.strClick) cLI.children('span').addClass('opcl');
                cLI.addClass(excol).prepend('<div class="hitarea ' + excol + '-hitarea"/>');
                cLI.filter(':last-child').addClass('last' + excol);
            }

            var selmap = $('ul.treeview .selectedList').parents('li.expandable').map(function() { return this; }).get();
            for (i = 0, len = selmap.length; i < len; i++) {
                $(selmap[i]).removeClass('expandable').addClass('collapsable').removeClass('lastexpandable').addClass('lastcollapsable');
                $(selmap[i]).children('.hitarea').removeClass('expandable-hitarea').addClass('collapsable-hitarea');
                //$(selmap[i]).children('ul').show();
            }
            $('.hitarea' + strlink).on('click', function() {
                var parent = $(this).parent('li');
                if (parent.children().is('.expandable-hitarea')) {
                    parent.removeClass('expandable').addClass('collapsable');
                    $(this).parent('li:last-child').removeClass('lastexpandable').addClass('lastcollapsable');
                    parent.children('div').removeClass('expandable-hitarea').addClass('collapsable-hitarea');
                    //$(this).nextAll('ul').show();
                } else {
                    parent.removeClass('collapsable').addClass('expandable');
                    $(this).parent('li:last-child').removeClass('lastcollapsable').addClass('lastexpandable');
                    parent.children('div').removeClass('collapsable-hitarea').addClass('expandable-hitarea');
                    //$(this).nextAll('ul').hide();
                }
            });
            $('#ExpandAll').on('click', function() {
                $('.treeview').find('.expandable').removeClass('expandable').addClass('collapsable');
                $('.treeview').find('.hitarea').removeClass('expandable-hitarea').addClass('collapsable-hitarea');
                $('.treeview').find('.lastexpandable').removeClass('lastexpandable').addClass('lastcollapsable');
            });
            $('#CollapseAll').on('click', function() {
                $('.treeview').find('.collapsable').removeClass('collapsable').addClass('expandable');
                $('.treeview').find('.hitarea').removeClass('collapsable-hitarea').addClass('expandable-hitarea');
                $('.treeview').find('.lastcollapsable').removeClass('lastcollapsable').addClass('lastexpandable');
            });

            $(target).find("a").hover(
                function() { $(this).closest("li").addClass("hoverList"); },
                function() { $(this).closest("li").removeClass("hoverList"); }
            );



        }
        return this;
    };

})(jQuery);