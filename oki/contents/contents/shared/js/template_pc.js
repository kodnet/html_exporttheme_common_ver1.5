// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide
		PC View Template
 Modified : 2016-02-27
/////////////////////////////////////////////////////////////////////////////////////////////////*/

var convertHereToStr = function(t) {
    return t.toString().match(/^[\s\S]*?\/\*([\s\S]*)\*\/[\s\S]*?$/)[1].replace(/(\r\n|\r|\n)(\t|\s)+/g, '');
};

var template = {

    "header": convertHereToStr((function() {
        /*
        		<div id="header">
        			<div>
        				<div id="meta">
        					<img src="shared/img/corporate_logo.png" title="" alt="" />
        					<span class="usermanual">@@usersmanual@@</span>
        				</div>
        				<form id="search">
        					<label for="searchKey">@@search@@ : </label><input type="text" id="searchKey" /><input type="submit" id="searchSubmit" value="@@search@@" />
        					<div id="searchOption">
        						<span class="label"><a href="#option">@@searchOption@@</a></span>
        						<div id="option">
        							<div class="header">
        								<h5>@@searchOption@@</h5>
        								<span class="close">@@close@@</span>
        							</div>
        							<div>
        								<span class="formItem"><input type="radio" id="andSearch" name="andOr" value="and" checked="checked" /><label for="andSearch">@@andSearch@@</label></span>
        								<span class="formItem"><input type="radio" id="orSearch" name="andOr"  value="or"/><label for="orSearch">@@orSearch@@</label></span>
        							</div>
        							<div><span class="formItem"><input type="checkbox" id="ZenHan" name="ZenHan" value="1" /><label for="ZenHan">@@ZenHan@@</label></span></div>
        							<div><span class="formItem"><input type="checkbox" id="charUpLo" name="charUpLo" value="1" /><label for="charUpLo">@@charUpLo@@</label></span></div>
        							<div><span class="formItem"><input type="checkbox" id="pMatch" name="pMatch" value="1" /><label for="pMatch">@@pMatch@@</label></span></div>
        						</div>
        					</div>
        				</form>
        				<div id="globalNav">
        					<ul>
        						<li class="naviTop"><a href="../../index.html" title="@@gNavTitle0@@" data-ajax="false"><span>@@gNav0@@</span></a></li>
        						<li class="navi1">@@gNav1@@</li>
        						<li class="navi2">@@gNav2@@</li>
        						<li class="navi3">@@gNav3@@</li>
        						<li class="navi4">@@gNav4@@</li>
        						<li class="naviSitemap"><a href="sitemap.html" title="@@gNavTitleSitemap@@" data-ajax="false"><span>@@sitemap@@</span></a></li>
        						<li class="naviPrintout"><a href="javascript: window.print();" data-ajax="false" title="@@global_printout@@"><span>@@gNavPrint@@</span></a></li>
        					</ul>
        				</div>
        			</div>
        			<div id="copyright"><address>@@copyright@@</address></div>
        		</div>
        	*/
    }))

    ,
    "article": convertHereToStr((function() {
        /*
        		<div id="contents">
        			<div id="contentsInner">
        				<div id="categoryNavi">
        					<div id="Navi">
        						<ul>
        							<li class="tab1"><a href="#localMenu" title="@@localnavi_tab_toc@@"><span>@@toc@@</span></a></li>
        							<li class="tab2"><a href="#searchResult" title="@@localnavi_tab_searchresult@@"><span>@@searchResult@@</span></a></li>
        						</ul>
        						<div id="localMenu">
        							<div class="ope"><a id="CollapseAll"><span>@@colapseall@@</span></a><a id="ExpandAll"><span>@@expandall@@</span></a></div>
        							<div class="list"></div>
        						</div>
        						<div id="searchResult">
        							<div class="navigation">
        								<div class="count"></div>
        								<div class="navi"></div>
        							</div>
        							<div class="result"></div>
        						</div>
        					</div>
        				</div>
        				<div id="container" class="container">
        					<h1>___articletitle___</h1>
        					<div id="page">
        						<div id="pageInner">
        							___article___
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div id="contentsNo">___filename___</div>
        	*/
    }))



    ,
    "singleArticle": convertHereToStr((function() {
        /*
        		<div id="contents">
        			<div id="contentsInner">
        				<div id="container" class="container">
        					<h1>___articletitle___</h1>
        					<div id="page">
        						<div id="pageInner">
        							___article___
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div id="contentsNo">___filename___</div>
        	*/
    }))


};