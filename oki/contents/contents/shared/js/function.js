// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide 
		Function JavaScript
 Modified : 2015-04-20
/////////////////////////////////////////////////////////////////////////////////////////////////*/


/* global txt,  availableTags, get, Searching, menu, ua, settings, _isWebStrg, domException, pageInfo */

// プロトコルを見て検索のフォーム action値を html＞phpへ------
	function searchAction(formAction){
		var searchAction = (pageInfo.isHTTP && formAction)? formAction.replace(/\.htm.?$/,".php") : formAction ;
		return searchAction || '';
	}
	// ---------------------------------------------------------
	
	
	//アウターサイズからインナーサイズを引いた値（余白）----
	function margeWH(ele, wh){
		var temp = 0;
		if(wh == "w"){
			temp = $(ele).outerWidth(true) - $(ele).width();
		}else{
			temp = $(ele).outerHeight(true) - $(ele).height();
		}
		return (temp||0);
	}
	// ---------------------------------------------------------
	
	
	//PXをEMで返す  ------------------------------------------
	function pxToEm(t,wh,num){//t=target; wh=(w:width | h:height); num=[size]
		var temp=0,w;
		var f = $(t).css('fontSize').replace('px','') *1;
		if(wh == "w"){
			w = num||$(t).width();
			temp = (w / f) + 'em';
		}else{
			w = num||$(t).height();
			temp = (w / f) + 'em';
		}
		return (temp||0);
	}
	// ---------------------------------------------------------
	
	
	//CSSの値からPXを消して数値として返す  ------------------------------------------
	function delPx(n){
		if(/px\s*$/i.test(n)){
			var n = n.replace(/(px|\s)/gi, '')*1;
		}
		if(n == void(0)) n = 0;
		return n;
	}
	// ---------------------------------------------------------
	
	
	// 配列の重複削除  ------------------------------------------
	function uniqueArray(array){
		var storage = {};
		var uniqueArray = [];
		var i,value;
		for ( i=0; i<array.length; i++){
			value = array[i];
			if (!(value in storage)){
				storage[value] = true;
				uniqueArray.push(value);
			}
		}
		return uniqueArray;
	}
	// ---------------------------------------------------------
	
	// 半角カタカナ　=>　全角カタカナ  -----------------------------------------
	function katakanaToZen(str){
		var han = "ｶﾞ ｷﾞ ｸﾞ ｹﾞ ｺﾞ ｻﾞ ｼﾞ ｽﾞ ｾﾞ ｿﾞ ﾀﾞ ﾁﾞ ﾂﾞ ﾃﾞ ﾄﾞ ﾊﾞ ﾊﾟ ﾋﾞ ﾋﾟ ﾌﾞ ﾌﾟ ﾍﾞ ﾍﾟ ﾎﾞ ﾎﾟ ｳﾞ ｧ ｱ ｨ ｲ ｩ ｳ ｪ ｴ ｫ ｵ ｶ ｷ ｸ ｹ ｺ ｻ ｼ ｽ ｾ ｿ ﾀ ﾁ ｯ ﾂ ﾃ ﾄ ﾅ ﾆ ﾇ ﾈ ﾉ ﾊ ﾋ ﾌ ﾍ ﾎ ﾏ ﾐ ﾑ ﾒ ﾓ ｬ ﾔ ｭ ﾕ ｮ ﾖ ﾗ ﾘ ﾙ ﾚ ﾛ ﾜ ｦ ﾝ ｡ ｢ ｣ ､ ･ ｰ ﾞ ﾟ".split(' ');
		var zen = "ガギグゲゴザジズゼゾダヂヅデドバパビピブプベペボポヴァアィイゥウェエォオカキクケコサシスセソタチッツテトナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲン。「」、・ー゛゜".split('');
		for(var i = 0, len = han.length; i < len; i++){
			var re = new RegExp(han[i], "g");
			str = str.replace(re, zen[i]);
		}
		return str;
	}
	// ---------------------------------------------------------
	
	
	// 以下、要JQuery ************************************************************
	
	// Naviの有無 ---------------------------------------------
	function isCategoryNavi(){
		return $('#contents').is(':has(#categoryNavi)');
	}
	// ---------------------------------------------------------
	
	
	// Naviの有無 ---------------------------------------------
	function isNavi(){
		return $('#contents').is(':has(#Navi)');
	}
	// ---------------------------------------------------------
	
	
	// 検索フォーム --------------------------------------------
	function searchFormFunc(){
		if(!$('#searchKey').val()){
			$('#searchKey').val(txt.searchKeyDef);
			//$('#searchKey').attr('placeholder', txt.searchKeyDef);
		}
		$('#searchKey')
			.after(
				'<span id="searchBtn" title="'+txt.search+'"></span>')
			.focus(function(){
				if($(this).val() == txt.searchKeyDef){
					$(this).val('').addClass("keyIn");
				}else{
					$(this).addClass("keyIn");
				}
			})
			.blur(function(){
				if(!$(this).val()){
					$('#searchKey').val(txt.searchKeyDef).removeClass("keyIn");
				}
			})
			.autocomplete({
				delay: 500,
				appendTo: '#search',
				select: function(event, ui){
					$(this).blur();
				},
				source: function(request, response){
					var key = katakanaToZen(request.term);
					var re = new RegExp( '^' + $.ui.autocomplete.escapeRegex( key ), "i" );
					if(/[\!-\~！-～￥]/.test(key)){
						key = key.split('');
						for(var i=0, len=key.length; i<len; i++){
							key[i] = key[i].replace(/[\!-\~！-～￥]/, function(s){
								if(/[！-～]/.test(s)){
									return '(' + s + '|' + $.ui.autocomplete.escapeRegex(String.fromCharCode(s.charCodeAt(0) - 0xFEE0)) + ')';
								}else if(/[￥]/.test(s)){
									return '(' + s + '|' + $.ui.autocomplete.escapeRegex(String.fromCharCode(0x005C)) + ')';
								}else if(/[\\]/.test(s)){
									return '(' + $.ui.autocomplete.escapeRegex(s) + '|' + String.fromCharCode(0xFFE5) + ')';
								}else{
									return '(' + $.ui.autocomplete.escapeRegex(s) + '|' + String.fromCharCode(s.charCodeAt(0) + 0xFEE0) + ')';
								}
							});
						}
						key = key.join('');
						re = new RegExp( '^' + key, "i" );
					}
					//console.log(re);
					var list = [];
					$.each(availableTags, function(i, values){
						if($.isArray(values)){
							for(var j in values){
								if(re.test(values[j])){
									list.push(values[0]);
									break;
								}
							}
						}else{
							if(re.test(values)){
								list.push(values);
							}
						}
					});
					list = uniqueArray(list);
					if(list.length > 10){
						list = list.slice(0, 10);
						$('#search > .ui-autocomplete').addClass('moreThan');
					}else{
						$('#search > .ui-autocomplete').removeClass('moreThan');
					}
					response(list);
				}
			});
		
		if(get['k']) Searching.searching(get['k'], "onload");
		$('#searchBtn').on("click", function(){
			$('form#search').submit();
		});
		$('form#search').submit(function(){
			var pMatch = $('#pMatch').is(':checked');
			var str = $("#searchKey").val();
			str = (pMatch)? str : $.trim( str.replace(/[\s　]+/g," ") );
			if(str == txt.searchKeyDef || str == ""){
				return false;
			}else{
				if(pageInfo.isTop || $('body').is('#c1')){
					Searching.searchingTop(str, null, "btn");
				}else{
					Searching.searching(str, null, "btn");
				}
				$('#searchKey').blur();
				return false;
			}
		});
	}
	// ---------------------------------------------------------
	
	
	
	//アラートWin -----------------------------------------------
	function alertWin(title, text, autoClose){
		$("body").append('<div id="alertWin">'+text+'</div>');
		var modalType;
		if(autoClose){
			modalType = false;
		}else{
			$('#alertWin').append('<div class="btn"><button value="yes">'+ txt.ok +'</button></div>');
			modalType = true;
		}
		$('#alertWin button').button();
		$('#alertWin').dialog({
			dialogClass: "modalWin",
			title: title,
			modal: modalType,
			resizable: false,
			draggable: false,
			closeOnEscape: false,
			open: function(event, ui){
				setTimeout(function(){$('.ui-state-focus').removeClass('ui-state-focus');},0);
				if(autoClose){
					setTimeout(
						function(){
							$('#alertWin').dialog('destroy').remove();
						}, autoClose
					);
				}
			},
			close: function(event, ui){
				$('#alertWin').dialog('destroy').remove();
			}
		});
		$('#alertWin').on('click', 'button', function(){
			switch($(this).val()){
				case "yes":
				default:
					$('#alertWin').dialog('destroy').remove();
					break;
			}
		});
		$('.modalWin button').attr('hideFocus','true');
	}
	// ---------------------------------------------------------
	
	
	//サイジング ------------------------------------------------
	function sizeing(){
		var winW = $(window).width();
		var winH = $(window).height();
		var maxH = Math.max(winH, $(document).height());
		
		var blockMinHeight = '#contentsInner #localMenu #searchResult #container'.split(' ');
		for(var i in blockMinHeight){
			$(blockMinHeight[i]).css('minHeight', function(){
				var blockMargin = $(blockMinHeight[i]).outerHeight(true) - $(blockMinHeight[i]).height();
				var position = 0;
				if($(blockMinHeight[i]).parent().is('#Navi')){
					position = $(blockMinHeight[i]).parent().offset().top + $(blockMinHeight[i]).parent().children('ul').outerHeight(true);
				}else{
					position = $(blockMinHeight[i]).offset().top;
				}
				return maxH - position - blockMargin;
			});
		}
		
	}
	// ---------------------------------------------------------
	
	
	//カテゴリトップのリンク -----------------------------------
	function categoryFirstLink(id){
		if (menu[id].titlelink) {
			return menu[id].titlelink;
		}else{
			var m = $(menu[id].menu);
			return m.find('span:has(>a):first').html();
		}
	}
	// ---------------------------------------------------------
	
	
	// アンカー、ボタンの選択枠消し ----------------------------
	function hideFocus(){
		$('a, button').attr('hideFocus','true');
	}
	// ---------------------------------------------------------
	
	
	//クッキー確認 ---------------------------------------------
	function isCkeck(){
		$.cookie("test", "1");
		var r;
		if($.cookie("test") === "1"){
			r = true;
		}else{
			r = false;
		}
		$.removeCookie("test");
		return r;
	}
	// ---------------------------------------------------------
	
	
	//クッキー or WebStrage 読み込み＆保存----------------------
	function localSpace(cmd, key, val, option){
		if(!option) option = {};
		
		var op = {};
		op.expires = option.expires || 365;
		if(ua._isIE) option.path = '/';
		if(option.path) op.path = option.path;
		if(option.domain) op.domain = option.domain;
		if(option.secure) op.secure = option.secure;
		
		key = (settings.cookieKey) ? key +'-'+  settings.cookieKey : key;
		
		if(_isWebStrg){
			var Storage = (option.session) ? sessionStorage : localStorage;
			if("save" === cmd){
				try{
					Storage.setItem(key, val);
				}catch(domException){
					if(domException.name === 'QuotaExceededError' || domException.name === 'NS_ERROR_DOM_QUOTA_REACHED'){
						// Fallback code comes here.
					}
				}
			}else if("load" === cmd){
				return Storage.getItem(key);
			}else if("remove" === cmd){
				Storage.removeItem(key);
			}
		}else{
			if("save" === cmd){
				$.cookie(key, val, op);
			}else if("load" === cmd){
				return $.cookie(key);
			}else if("remove" === cmd){
				$.removeCookie(key, op);
			}
		}
	}
	// ---------------------------------------------------------
	
	
	
	// 見た目調整 ---------------------------------------------------------
	function lookfeel(){
		
		//tileLayoutA =============================================================================
		if($('#pageInner div').is('.tileLayoutA')){
			$('.tileLayoutA').each(function(i,e){
				var heightH4 = 0;
				$(e).find('.block h4').each(function(j,e2){
					heightH4 = Math.max($(e2).height(), heightH4);
				});
				heightH4 = pxToEm($(e).find('.block h4'), 'h', heightH4);
				$(e).find('.block h4').height(heightH4);
				
				var heightBlock = 0;
				var blockLen = $(e).find(".block").length;
				var k = 0;
				//if(ua._isMac && ua._isSafari){
					$(e).find(".block").each(function(j,e2){
						$(e2).find('.image img').on("load", function(e3){
							heightBlock = Math.max($(e2).height(), heightBlock);
							k++;
							if(k >= blockLen){
								$(".tileLayoutA .block").height(heightBlock);
							}
						});
					});
				//}else{
				//	$(e).find(".block").each(function(j,e2){
				//		heightBlock = Math.max($(e2).height(), heightBlock);
				//	});
				//	$(".tileLayoutA .block").height(heightBlock);
				//}
			});
		}
		
		//tileLayoutB =============================================================================
		if($('#pageInner div').is('.tileLayoutB')){
			$('.tileLayoutB').each(function(i,e){
				var h=0;
				$(e).find('.block').each(function(j,e2){
					h = Math.max($(e2).height(), h);
				});
				$(e).find('.block').height(h);
			});
		}
		//====================================================================================
		
		//tileLayoutC =============================================================================
		if($('#pageInner div').is('.tileLayoutC')){
			$('.tileLayoutC').each(function(i,e){
				var h=0;
				$(e).find('.block span').each(function(j,e2){
					h = Math.max($(e2).height(), h);
				});
				$(e).find('.block span').height(h);
			});
		}
		//====================================================================================
		
		//ol.numbered1 =============================================================================
		$('ol.numbered1').each(function(i,e){
			$(e).children('li').each(function(i2,e2){
				$(e2).addClass('n'+(i2+1));
			});
		});
		//====================================================================================
		
		//define =============================================================================
		$('dl.define').each(function(i,e){
			
			if(ua._isIE7){
				$(e).find('>dt>p:first-child').append('<strong>:</strong>');
			}
			
			var dtMaxW = $(e).children('dt:first').width();
			var lineH = 0;
			
			$(e).children('dt').each(function(iDT,eDT){
				dtMaxW = Math.max( $(eDT).width(), dtMaxW );
				var lineH = Math.max( $(eDT).height(), $(eDT).next('dd').height() );
				$(eDT).height(lineH).next('dd').height(lineH);
			});
			$(e).children('dt').width(dtMaxW);
			$(e).children('dd').css('marginLeft', $(e).children('dt').outerWidth(true));
			
		});
		//====================================================================================
		
		//IE7 ======================================================================
		if(ua._isIE7){
			//Table ----------------------
			$('table').attr('cellspacing','0');
			
			//annotation ----------------------
			$('ol.annotation').each(function(i,e){
				$(e).find('>li>p:first-child').each(function(iLI, eLI){
					$(eLI).prepend('*'+ (iLI+1) +'. ');
				});
			});
		}
		
		if(ua._isSafari6low){
			$('#header').prepend('<div id="dm"><img src="shared/img/clear.gif" title="" alt="" /></div>');
		}
	}
	