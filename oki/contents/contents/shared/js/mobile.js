// 
// ---------------------

/* global menu, txt, decodeURIComponent, NotHTTP, Searching, get, availableTags, pageInfo, homeFile */

//スワイプ判定の動き量設定（デフォルト：30）
$.event.special.swipe.horizontalDistanceThreshold = 80;

var ev = {
    "swipe": false
};
var tableProp = {};
var bodyScrollPos = 0;
var activePage = {},
    $activePage;
var touchTime = { "time": 0, "start": 0, "end": 0 };

var searchResultProp = {
    "scrollTop": 0,
    "collapse": { "resultCT1": true }
};
var memoryPnanelState = {
    "searchResult": {
        "save": function() {
            try {
                sessionStorage.setItem('searchResultProp', JSON.stringify(searchResultProp));
            } catch (e) {}
        },
        "load": function() {
            try {
                searchResultProp = $.extend({},
                    //searchResultPropDefault,
                    { "scrollTop": 0, "collapse": { "resultCT1": true } },
                    JSON.parse(sessionStorage.getItem("searchResultProp"))
                );
                sessionStorage.removeItem('searchResultProp');
            } catch (e) {}
            return true;
        }
    },
    "toc": {

    }
};


var mobileFunc = {


    "setHightlight": function() {
        var hilightOp = {
            "UpLo": (get["charUpLo"]) ? true : false,
            "ZenHan": (get["ZenHan"]) ? true : false,
            "pMatch": (get["pMatch"]) ? true : false
        };
        $('h1, #pageInner').keyHighlight(get["k"], hilightOp);
    }


    ,
    "setPageSearchResult": function() {
        if (get["k"]) {
            mobileFunc.setHightlight();
            Searching.searching(get['k'], "onload");
            memoryPnanelState.searchResult.load(); //console.log(searchResultProp);
            mobileFunc.setSearchResultContents();
            $('.searchResultBtn, #searchResult').addClass('enable');
            $(".ui-page #searchKey").val(get["k"]);

        } else {
            $('.searchResultBtn, #searchResult').removeClass('enable');
            $(".ui-page #searchKey").val('');
        }
    }


    ,
    "setSearchResultContents": function() {
        var ctNum = 0;
        $('#searchResult .result > *').each(function(i, e) {
            if ($(e).is('h2')) {
                ctNum++;

                var expandClass = (function() { //console.log('resultCT'+ctNum+": "+ searchResultProp.collapse["resultCT" + ctNum]);
                    if (searchResultProp.collapse["resultCT" + ctNum]) {
                        return [' aria-expanded="true"', " in"];
                    } else {
                        return ["", ""];
                    }
                })();

                $(e).wrap('<div class="category"/>')
                    .wrapInner('<a href="#resultCT' + ctNum + '"' + expandClass[0] + ' data-toggle="collapse"/>')
                    .after('<div id="resultCT' + ctNum + '" class="items panel-collapse collapse' + expandClass[1] + '">');

            } else if ($(e).is('.item')) {
                $(e).appendTo($(e).prev('.category').children('.items'));
            }
        });
    }


    ,
    "addSearchEvent": function() {
        $(document).on('submit', 'form#search', function(e) {
            //console.log('search start');
            var pMatch = $('#pMatch').is(':checked');
            if (!$('#searchResult').is('.enable')) {
                $('#option .optionBtn').removeClass('open');
                $('#option .optionItem').hide();
                $('#option .optionItem label').removeClass('disabled').find('input').removeAttr('disabled');
                $('#orSearch, #ZenHan, #charUpLo, #pMatch').removeAttr('checked');
                $('#andSearch').attr('checked', 'checked').click();
            }
            var str = $('.ui-page-active').find("#searchKey").val();
            str = (pMatch) ? str : $.trim(str.replace(/[\s　]+/g, " "));
            if (str == "") {
                return false;
            } else {
                $('#searchKey').blur();
                Searching.searching(str);
                searchResultProp = $.extend({}, { "scrollTop": 0, "collapse": { "resultCT1": true } });
                mobileFunc.setSearchResultContents();
                $('#searchResult .resultArea').scrollTop(0);
                $('.searchResultBtn, #searchResult').addClass('enable');
                $('#container').addClass('searchResultEnabled');
                mobileFunc.searchResultShowHide("show");
                return false;
            }
        });

        $(document).on('tap', '#relatedWords a, .termWord a', function(e) {
            //console.log(e.currentTarget.search);
            var data = e.currentTarget.search.replace(/^\?/, '');
            if (data) {
                data = data.split('&');
                var temp, getData = [];
                for (var i = 0, len = data.length; i < len; i++) {
                    temp = decodeURIComponent(data[i]).split('=');
                    ak = temp[0];
                    temp.shift();
                    getData[ak] = temp.join('=');
                }
                $('.ui-page-active').find("#searchKey").val(getData['k']);
                if (getData['pMatch'] * 1 && getData['rel'] * 1) {
                    $('#searchResult #option #pMatch').click();
                    $('#option .optionBtn').addClass('open');
                    $('#option .optionItem').show();
                }
                $('.ui-page-active').find("#search").submit();
            }
            return false;
        });
        $(document).on('active', ".ui-page-active #searchKey", function() {
            mobileFunc.displayZoom(false);
        });
    }


    ,
    "_isSetAutoComp": function() {
        var r = false;
        var min = Math.min($(window).width(), $(window).height());
        if (600 <= min) {
            r = true;
        }
        return r;
    }


    ,
    "setAutoComplete": function($searchKey) {
        try {
            $("#searchKey").autocomplete("destroy");
        } catch (e) {}
        //
        $searchKey.autocomplete({
            delay: 500,
            appendTo: '.ui-page-active header',
            position: { my: "center top", at: "center bottom", of: ".ui-page-active header" },
            create: function(event, ui) {
                $searchKey.closest('header').find('.ui-autocomplete').on('tap', 'li', function(e) {
                    $(e.currentTarget).click();
                });
            },
            source: function(request, response) {
                var key = mobileFunc.katakanaToZen(request.term);
                var re = new RegExp('^' + $.ui.autocomplete.escapeRegex(key), "i");
                if (/[\!-\~！-～￥]/.test(key)) {
                    key = key.split('');
                    for (var i = 0, len = key.length; i < len; i++) {
                        key[i] = key[i].replace(/[\!-\~！-～￥]/, function(s) {
                            if (/[！-～]/.test(s)) {
                                return '(' + s + '|' + $.ui.autocomplete.escapeRegex(String.fromCharCode(s.charCodeAt(0) - 0xFEE0)) + ')';
                            } else if (/[￥]/.test(s)) {
                                return '(' + s + '|' + $.ui.autocomplete.escapeRegex(String.fromCharCode(0x005C)) + ')';
                            } else if (/[\\]/.test(s)) {
                                return '(' + $.ui.autocomplete.escapeRegex(s) + '|' + String.fromCharCode(0xFFE5) + ')';
                            } else {
                                return '(' + $.ui.autocomplete.escapeRegex(s) + '|' + String.fromCharCode(s.charCodeAt(0) + 0xFEE0) + ')';
                            }
                        });
                    }
                    key = key.join('');
                    re = new RegExp('^' + key, "i");
                }
                //console.log(re);
                var list = [];
                $.each(availableTags, function(i, values) {
                    if ($.isArray(values)) {
                        for (var j in values) {
                            if (re.test(values[j])) {
                                list.push(values[0]);
                                break;
                            }
                        }
                    } else {
                        if (re.test(values)) {
                            list.push(values);
                        }
                    }
                });
                list = mobileFunc.uniqueArray(list);
                if (list.length > 10) {
                    list = list.slice(0, 10);
                    $('.ui-page-active header > .ui-autocomplete').addClass('moreThan');
                } else {
                    $('.ui-page-active header > .ui-autocomplete').removeClass('moreThan');
                }
                response(list);
            }
        });
    }


    ,
    "katakanaToZen": function(str) {
        var han = "ｶﾞ ｷﾞ ｸﾞ ｹﾞ ｺﾞ ｻﾞ ｼﾞ ｽﾞ ｾﾞ ｿﾞ ﾀﾞ ﾁﾞ ﾂﾞ ﾃﾞ ﾄﾞ ﾊﾞ ﾊﾟ ﾋﾞ ﾋﾟ ﾌﾞ ﾌﾟ ﾍﾞ ﾍﾟ ﾎﾞ ﾎﾟ ｳﾞ ｧ ｱ ｨ ｲ ｩ ｳ ｪ ｴ ｫ ｵ ｶ ｷ ｸ ｹ ｺ ｻ ｼ ｽ ｾ ｿ ﾀ ﾁ ｯ ﾂ ﾃ ﾄ ﾅ ﾆ ﾇ ﾈ ﾉ ﾊ ﾋ ﾌ ﾍ ﾎ ﾏ ﾐ ﾑ ﾒ ﾓ ｬ ﾔ ｭ ﾕ ｮ ﾖ ﾗ ﾘ ﾙ ﾚ ﾛ ﾜ ｦ ﾝ ｡ ｢ ｣ ､ ･ ｰ ﾞ ﾟ".split(' ');
        var zen = "ガギグゲゴザジズゼゾダヂヅデドバパビピブプベペボポヴァアィイゥウェエォオカキクケコサシスセソタチッツテトナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲン。「」、・ー゛゜".split('');
        for (var i = 0, len = han.length; i < len; i++) {
            var re = new RegExp(han[i], "g");
            str = str.replace(re, zen[i]);
        }
        return str;
    }


    ,
    "uniqueArray": function(array) {
        var storage = {};
        var uniqueArray = [];
        var i, value;
        for (i = 0; i < array.length; i++) {
            value = array[i];
            if (!(value in storage)) {
                storage[value] = true;
                uniqueArray.push(value);
            }
        }
        return uniqueArray;
    }



    ,
    "searchResultShowHide": function(sh) {
        //開き動作
        var show = function() {
            mobileFunc.displayZoom(false);
            bodyScrollPos = $('body').scrollTop();
            $('.ui-page-active').css('marginTop', '-' + bodyScrollPos + 'px').addClass('panelOpen');
            if (!$('.ui-page-active').find('#wrapper').parent().is('.SRFixed')) {
                $('.ui-page-active').find('#wrapper,footer').wrapAll('<div class="SRFixed"/>');
            }
            $('.searchResultBtn') //.addClass('opened')
                .find('.glyphicon').removeClass('glyphicon-triangle-left').addClass('glyphicon-triangle-right');
            $('.panelBackDrop').remove();
            $('#searchResult').prepend('<div class="panelBackDrop searchResultBackDrop"/>')
                .removeClass('closed').addClass('opened');
            $('body').addClass('searchResultOpen');
            $('#searchResult .searchResultBackDrop').on('tap', function(e) { //console.log(e);
                mobileFunc.searchResultShowHide("hide");
                return false;
            });
            if (searchResultProp.scrollTop) {
                $('#searchResult .resultArea').scrollTop(searchResultProp.scrollTop);
                searchResultProp.scrollTop = 0;
            }
        };
        //閉じ動作
        var hide = function() {
            $('.ui-page-active').removeAttr('style').removeClass('panelOpen');
            $('.ui-page-active .SRFixed').replaceWith($('.ui-page-active .SRFixed').html());
            $('.searchResultBtn').removeAttr('style') //.removeClass('opened')
                .find('.glyphicon').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-left');
            $('#searchResult').removeClass('opened').addClass('closed');
            $('body').removeClass('searchResultOpen').scrollTop(bodyScrollPos);
            $('.panelBackDrop').remove();
            mobileFunc.displayZoom(true);
        };

        if (sh) {
            if ('show' == sh) show();
            if ('hide' == sh) hide();
        } else {
            if ($('#searchResult').is('.closed')) {
                show();
            } else {
                hide();
            }
        }
        return false;
    }


    ,
    "tocShowHide": function(sh) {
        var btnImgRep = function(file) {
            $('#toToc .tocBtn img').attr('src', function() {
                var src = $(this).attr('src');
                if (src) {
                    src = src.replace(/\/[^\/]+.png$/, '/' + file + '.png');
                }
                return src;
            });
        };

        //開き動作
        var show = function() {
            mobileFunc.displayZoom(false);
            bodyScrollPos = $('body').scrollTop();
            var file = $activePage.attr('data-url').split('/').slice(-1).join('');
            file = file.split('?').slice(0, 1).join('');
            var id = file.split("_", 1).join('');
            if (!$('.tocBtn').is(".loaded")) {
                $('#localToc .toc').html(function() {
                    var html = $('#menu-html').html();
                    $('#menu-html').remove();
                    return html;
                });
                $('#localToc ul ul').addClass('childMenu');
                $('#localToc ul ul').prev('span').append('<span class="doMenu"/>');
                $('.tocBtn').addClass("loaded");
            }
            $('#localToc .selected').removeClass('selected');
            $('#localToc a[href="' + file + '"]').addClass('selected');
            $('#localToc a.selected').parents('ul').addClass('open');
            $('#localToc ul.open').prev('span').children('.doMenu').addClass('open');
            $('#localToc .doMenu.open').parent('span').addClass('childOpen');
            $activePage.css('marginTop', '-' + bodyScrollPos + 'px').addClass('panelOpen');
            $('#localToc').removeClass('closed').addClass('opened');
            //btnImgRep('remove');
            $('.panelBackDrop').remove();
            $('body').addClass('tocOpen').append('<div class="panelBackDrop"/>');
            $('.panelBackDrop').on('tap', function() {
                mobileFunc.tocShowHide("hide");
                return false;
            });
        };
        //閉じ動作
        var hide = function() {
            $activePage.removeAttr('style').removeClass('panelOpen');
            $('#localToc').removeClass('opened').addClass('closed');
            //btnImgRep('menu');
            $('body').removeClass('tocOpen').scrollTop(bodyScrollPos);
            $('.panelBackDrop').remove();
            mobileFunc.displayZoom(true);
        };


        if (sh) {
            if ('show' == sh) show();
            if ('hide' == sh) hide();
        } else {
            if ($('#localToc').is('.closed')) {
                show();
            } else {
                hide();
            }
        }
    }


    ,
    "menuModalShoHide": function(type) {
        if ("close" == type) {
            $('#menuModal').addClass('close');
        } else {
            $('#menuModal').toggleClass('close');
        }
        var menuBtnIcon = $('#showMenu > img');
        var iconSrc = menuBtnIcon.attr('src');
        if ($('#menuModal').is('.close')) {
            //閉じる
            menuBtnIcon.attr('src', iconSrc.replace("remove.png", "menu.png"));
            $('.menuOverlay').remove();
            $('body').removeClass('showMenuModal');
            mobileFunc.displayZoom(true);
        } else {
            //開く
            menuBtnIcon.attr('src', iconSrc.replace("menu.png", "remove.png"));
            $('.menuOverlay').remove();
            $('body').append('<div class="menuOverlay"/>');
            $('.menuOverlay').height($(document).outerHeight(true) - $('.menuOverlay').position().top);
            $('body').addClass('showMenuModal');
            mobileFunc.displayZoom(false);
        }
        return true;
    }


    ,
    "setTableScroll": function() {
        var portraitWidth = Math.min($(window).width(), $(window).height());
        var contentsMargin = $(window).width() - $('.ui-page-active #page').width();
        var portraitContainerWidth = portraitWidth - contentsMargin;
        $('.ui-page-active table').each(function(i, e) {
            if (!$(e).parents().is('.safety, .layoutTable')) {
                $(e).width(portraitContainerWidth * 2).parent().css({
                    "overflowX": "auto",
                    "width": "100%",
                    "maxWidth": "100%",
                    "marginBottom": "20px"
                });
            }
        });
    }


    ,
    "movPage": function(e, t) {
        var nextPrevElement = $(e.currentTarget).find('#nextPrev');
        var nextURL = nextPrevElement.find('.next a').attr('href');
        var prevURL = nextPrevElement.find('.prev a').attr('href');
        var chengeUrl = '';
        var op = { transition: 'slide' };
        //console.log(nextPrevElement);return;

        if (e.type == 'swipeleft') {
            chengeUrl = nextURL;
            if (t == 'tableLeft') chengeUrl = '';
        } else {
            chengeUrl = prevURL;
            if (t == 'tableRight') chengeUrl = '';
            op.reverse = true;
        }

        if (chengeUrl) {
            $('body').pagecontainer('change', chengeUrl, op);
            get["k"] = '';
            //console.log(chengeUrl);
            return false;
        }
    }


    ,
    "setPageSwipeEvent": function() {
        $(document).on({
            "swipeleft swiperight": function(e) {
                //console.log(e.type + ': .ui-page-active');
                $.extend(ev, {
                    "swipe": true,
                    "swipeTarget": e
                });
            },
            "touchstart": function(e) {
                //console.log('touchstart: .ui-page-active');
                touchTime.start = e.timeStamp;
                $.extend(ev, {
                    mousedown: true,
                    mouseup: false
                });
            },
            "gesturechange": function() {
                $.extend(ev, {
                    gesturechange: true
                });
            },
            "touchend": function(e) {
                //console.log('touchend: .ui-page-active');
                touchTime.end = e.timeStamp;
                if (touchTime.end - touchTime.start > 1000) {
                    $.extend(ev, {
                        swipe: false,
                        swipeTarget: undefined
                    });
                }
                $.extend(ev, {
                    mousedown: false,
                    mouseup: true
                });
                //スワイプ動作
                //console.log(ev);
                if (ev.swipe && !ev.gesturechange) {
                    var t = ev.swipeTarget;
                    var _isTable = $(t.target).parents().is('table');
                    if (_isTable && tableProp.width > tableProp.outerWidth) {
                        if (tableProp.scroll == "left") {
                            mobileFunc.movPage(t, "tableLeft");
                        } else if (tableProp.scroll == "right") {
                            mobileFunc.movPage(t, "tableRight");
                        }
                    } else {
                        mobileFunc.movPage(t, true);
                    }
                }
                ev = {};
                //alert(document.body.clientWidth / window.innerWidth);
            }
        }, '.ui-page-active:not(.panelOpen)');
    }


    ,
    "displayZoom": function(showHide) {
        if (!showHide) {
            $('head meta[name="viewport"]').attr('content', "width=device-width,initial-scale=1.0,user-scalable=no,maximum-scale=1");
        } else if (showHide) {
            $('head meta[name="viewport"]').attr('content', "width=device-width,initial-scale=1.0,user-scalable=yes");
        }
        //alert(showHide);
    }
};


//var zoomrate = function(){
//	return document.body.clientWidth / window.innerWidth;
//};

//$(window).on("orientationchange gestureend", function(){
//	$('#localToc').css({
//		"maxHeight": "200%",
//		"minHeight": "200%"
//	});
//});





/*
 * pageshowイベント
 */
$(document).on('pageshow', function(page) {
    activePage = page.target;
    $activePage = $(activePage);
    //console.log(activePage);

    // テーブルスクロールセット
    mobileFunc.setTableScroll();


    //検索結果から遷移してきたときの処理
    mobileFunc.setPageSearchResult();

    if (mobileFunc._isSetAutoComp()) {
        mobileFunc.setAutoComplete($activePage.find('#searchKey'));
    }

    $('.ui-autocomplete').on('tap', '.ui-menu-item', function() {
        return false;
    });


    // to top icon fade in fade out
    if (!$("body").children().is('#toTop')) {
        $("body").append('<div id="toTop"><span/></div>');
    }

});
//end pageshowイベント


$(document).on("scroll", function() {
    // トップから250px以内はボタンを非表示にする
    if ($(this).scrollTop() < 250) {
        $("#toTop").hide();
    } else if ($(this).scrollTop() > 350) {
        // トップから350px以上スクロールしたらボタンを表示する
        $("#toTop").fadeIn();
    } else if (250 < $(this).scrollTop() && $(this).scrollTop() < 350) {
        // それ以外はフェードアウトする
        $("#toTop").fadeOut();
    }
});
$(document).on("click", '#toTop', function() {
    $('html,body').animate({ scrollTop: 0 }, 500);
});



//トップページのメニュー
//$(document).on('tap','#showMenu, #menuModal a', function(e){
$(document).find('#showMenu, .menuOverlay').unbind('tap');
$(document).find('#showMenu, .menuOverlay').bind('tap', function(e) {
    mobileFunc.menuModalShoHide();
});

//ページスワイプ遷移イベント
mobileFunc.setPageSwipeEvent();

// ページナビ内アンカークリック
$(document).find('div.pagenavi a[href^="#"]').unbind('tap');
$(document).find('div.pagenavi a[href^="#"]').bind('tap', function(e) {
    //console.log('tap: .pagenavi');
    e.preventDefault();
    var y = $activePage.find($(e.currentTarget).attr('href')).offset().top;
    $('html,body').animate({ scrollTop: y }, 500, "swing");
    return false;
});

// アコーディオン
$(document).find('.supplement:not(.non_child) h5').unbind('tap');
$(document).find('.supplement:not(.non_child) h5').bind('tap', function(e) {
    var clickHeader = $(e.currentTarget);
    var openContents = clickHeader.parent('.supplement').children('.supplementContents');
    clickHeader.toggleClass('open');
    if (clickHeader.is('.open')) {
        openContents.slideDown("fast");
    } else {
        openContents.slideUp("fast");
    }
});

//もくじ表示
$(document).find('.tocBtn').unbind('tap');
$(document).find('.tocBtn').bind('tap', function(e) {
    //console.log('.tocBtn click');
    if (!$('body').is('.searchResultOpen')) {
        mobileFunc.tocShowHide();
    }
    //return false;
    e.preventDefault();
});
$(document).find('#localToc').unbind('swipeleft');
$(document).find('#localToc').bind('swipeleft', function(e) {
    //console.log('#localToc swipeleft');
    mobileFunc.tocShowHide();
    //return false;
});

//もくじ内リンク、矢印クリック
$(document).find('#localToc .toc a[href], #localToc .toc .doMenu').unbind('tap');
$(document).find('#localToc .toc a[href], #localToc .toc .doMenu').bind('tap', function(e) {
    //console.log('tap: a[href], .doMenu');
    if ($(e.currentTarget).is('a') && $(e.currentTarget).next().is('.doMenu') || $(e.currentTarget).is('.doMenu') && !$(e.currentTarget).is('.open')) {
        var doMenu = $(e.currentTarget).parent().children('.doMenu');
        var targetLi = doMenu.closest('li');
        var targetSpan = targetLi.children('span');
        var targetUl = targetLi.children('ul');
        doMenu.toggleClass('open');
        if (doMenu.is('.open')) {
            targetSpan.addClass('childOpen');
            targetUl.slideDown() /*.addClass('open')*/ ;
        } else {
            targetSpan.removeClass('childOpen');
            targetUl.slideUp() /*.removeClass('open')*/ ;
        }
        return false;
    } else {
        var uri = $(e.currentTarget).parent().children('a').attr('href');
        if (uri) {
            mobileFunc.tocShowHide('hide');
            window.location.href = uri;
        }
    }
});





//検索のイベント追加
mobileFunc.addSearchEvent();

//検索結果表示
$(document).find('#searchResult .searchResultBtn').unbind('touchend click');
$(document).find('#searchResult .searchResultBtn').bind('touchend click', function(e) {
    //console.log('.searchResultBtn touchend');
    if ($('body').hasClass('searchResultOpen')) {
        mobileFunc.searchResultShowHide('hide');
    } else {
        mobileFunc.searchResultShowHide('show');
    }
    return false;
});
$(document).find('#searchResult').unbind('swiperight');
$(document).find('#searchResult').bind('swiperight', function(e) {
    //console.log('#searchResult swiperight');
    mobileFunc.searchResultShowHide("hide");
    e.preventDefault();
});
$(document).find('#searchResultClose').unbind('tap');
$(document).find('#searchResultClose').bind('tap', function(e) {
    mobileFunc.searchResultShowHide("hide");
    e.preventDefault();
});


//検索オプション開閉
$(document).find('#option .optionBtn').unbind('click');
$(document).find('#option .optionBtn').bind('click', function(e) {
    //console.log('option open & close');
    $(e.currentTarget).toggleClass('open');
    if ($(e.currentTarget).is('.open')) {
        $('#option .optionItem').slideDown(200);
    } else {
        $('#option .optionItem').slideUp(200);
    }
    $(e.currentTarget).blur();
});

//検索結果カテゴリ開閉状況記憶
$(document).on({
    'show.bs.collapse': function(e) {
        searchResultProp.collapse[this.id] = true;
    },
    'hide.bs.collapse': function(e) {
        searchResultProp.collapse[this.id] = false;
    }
}, "#searchResult .items");

//検索結果リンク遷移時の動作
$(document).find('#searchResult .item h3 a').unbind('click');
$(document).find('#searchResult .item h3 a').bind('click', function(e) {
    $.extend(searchResultProp, {
        "scrollTop": $('#searchResult .resultArea').scrollTop()
    });
    memoryPnanelState.searchResult.save();
    return true;
});




//テーブルプロパティ取得
$(document).on("touchstart", "table", function(e) {
    tableProp.target = $(e.target).closest('table');
    tableProp.outer = tableProp.target.parent();
    tableProp.width = tableProp.target.width();
    tableProp.outerWidth = tableProp.outer.width();
    tableProp.scroll = false;
    tableProp.scrollNum = tableProp.outer.scrollLeft();
    tableProp.scrollMax = tableProp.width +
        (tableProp.target.css('borderLeftWidth').replace('px', '') * 1) +
        (tableProp.target.css('borderRightWidth').replace('px', '') * 1) -
        tableProp.outerWidth;
    if (tableProp.scrollNum <= 0) {
        tableProp.scroll = "left";
    } else if (tableProp.scrollNum >= tableProp.scrollMax) {
        tableProp.scroll = "right";
    } else {
        tableProp.scroll = false;
    }
});

// おすすめ機能の画像へリンク付与
$(document).on('click tap', '.tileLayoutA .block .link', function(e) {
    window.location.href = $(e.currentTarget).find('h4 a').attr('href');
});

//PC, Mobile 切り替え時のCookie保存
$(document).on('click tap', '#mobileLink a[href][rel]', function(e) {
    document.cookie = 'okiManualDevice=' + $(e.currentTarget).attr('rel') + cookiePath;
});


//jamp index
$(document).on('click tap', 'a[href$="index.html"]', function(e) {
    if (/^\.\.\/index\.html$/.test($(e.currentTarget).attr('href'))) {
        var href = $(e.currentTarget).attr('href').replace(/(index)(\.html?)$/, homeFile);
        $(e.currentTarget).attr('href', href);
    }
});

$(document).on('vmousecancel', function() {
    ev = {};
});

if (typeof $activePage == 'undefined') {
    $('[data-role="page"]').trigger('pageshow');
}