// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide
		Top JavaScript
 Modified : 2016-08-18
/////////////////////////////////////////////////////////////////////////////////////////////////*/

/* global ua */

var topFunc = {
	
	"recommendTab": function(){
		$("#recommend").tabs({
			"activate": function(event, ui){
				$(ui.oldTab).find('a img').attr('src', function(){
					return $(this).attr('src').replace(/(^.+)_ac(\.[^\.]+$)/, "$1$2");
				});
				$(ui.newTab).find('a img').attr('src', function(){
					if(!/(^.+)_ac(\.[^\.]+$)/.test($(this).attr('src'))){
						return $(this).attr('src').replace(/(^.+)(\.[^\.]+$)/, "$1_ac$2");
					}
				});
			}
		});
		$('#recommend > ul li.ui-tabs-active a img').attr('src', function(){
			return $(this).attr('src').replace(/(^.+)(\.[^\.]+$)/, "$1_ac$2");
		});
		$('#recommend > ul.ui-tabs-nav li a img').hover(
			function(){
				var src = $(this).attr('src');
				if(!/_ac\.[^\.]+$/.test(src) && !$(this).closest('.ui-tabs-active').length)
					$(this).attr('src', $(this).attr('src').replace(/(^.+)(\.[^\.]+$)/, "$1_ac$2"));
			},function(){
				var src = $(this).attr('src');
				if(/_ac\.[^\.]+$/.test(src) && !$(this).closest('.ui-tabs-active').length)
					$(this).attr('src', $(this).attr('src').replace(/(^.+)_ac(\.[^\.]+$)/, "$1$2"));
			}
		);
		$('#recommend > div a img').hover(
			function(){
				var src = $(this).attr('src');
				if(!/_on\.[^\.]+$/.test(src))
					$(this).attr('src', $(this).attr('src').replace(/(^.+)(\.[^\.]+$)/, "$1_on$2"));
			},function(){
				var src = $(this).attr('src');
				if(/_on\.[^\.]+$/.test(src))
					$(this).attr('src', $(this).attr('src').replace(/(^.+)_on(\.[^\.]+$)/, "$1$2"));
			}
		);
		$('#moreRec a').hover(function(){
			$('#moreRec').css('backgroundPosition', '-1000px 0');
		},function(){
			$('#moreRec').css('backgroundPosition', '0 0');
		});
	},
	
	"rightNaviPullDw": function(){
		$(document).on('click', function(e){
			if($(e.target).closest('a').is('#mainNavi .navRight>ul>li>a')){
				var parentLI = $(e.target).closest('li');
				if(parentLI.is('.open')){
					parentLI.removeClass('open');
				}else{
					$('#mainNavi .navRight .open').removeClass('open');
					parentLI.addClass('open');
				}
				return false;
			}else{
				$('#mainNavi .navRight li.open').removeClass('open');
			}
			
		});
    }

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
$(function(){
		
	//検索
	searchFormFunc();
	
	
	topFunc.recommendTab();
	topFunc.rightNaviPullDw();
	
	//外部リンク[rel="external"]
	$(document).on('click', 'a[href][rel="external"]', function(){
		window.open($(this).attr('href'));
		return false;
	});
	
	//サイジング
	sizeing();
	if(ua._isIE8){
		document.body.onresize = function(){
			sizeing();
		};
	}else{
		$(window).resize(function(){
			sizeing();
		});
	}
	
	
	
	//モバイル切り替え
	$('#mobileLink').on('click', 'a[href][rel]', function(e){
		document.cookie = 'okiManualDevice=' + $(e.currentTarget).attr('rel') + cookiePath;
	});
	
	
	
//	if(!$("#mainNavi").children().is('#toTop')){
//		$("#mainNavi").append('<div id="toTop"><span/></div>');
//	}
//	$(window).on("scroll", function() {
//		// トップから250px以内はボタンを非表示にする
//		if($(this).scrollTop() < 250){
//			$("#toTop").hide();
//		} else if ($(this).scrollTop() > 350) {
//		// トップから350px以上スクロールしたらボタンを表示する
//			$("#toTop").fadeIn();
//		} else if (250 < $(this).scrollTop() < 350) {
//		// それ以外はフェードアウトする
//			$("#toTop").fadeOut();
//		}
//	});
//	$(document).on("click", '#toTop', function(){
//		$('html,body').animate({scrollTop:0} , 500);
//	});
	
});//($function()の終了
///////////////////////////////////////////////////////////////////////////////////////////////////////


