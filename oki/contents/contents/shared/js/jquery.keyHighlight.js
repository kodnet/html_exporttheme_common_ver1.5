// 
// ---------------------
/**@license
 * Keyword Highlight
 * 
 * Create by k.Hiramatsu
 * Modefiy 2016-02-03
 * 
 * 
 * ex)
 *  $(targetNome).keyHighlight(keyword, {Element:"tagName", Class:"className"});
 * 
 *  Sample:
 *     $('div#hoge').keyHighlight("hogehoge");
 *     $('div#hoge').keyHighlight("hogehoge", {
 *        "Element": "strong",        //Def: mark
 *        "Class":   "light",         //Def: ""
 *        "addID":   true,            //Def: true ID付与
 *        "scroll":  [false, "body", "html,body"],  //Def: [false, "body"] ヒットした位置にスクロール、スクロール量計算基準要素、スクロール要素
 *        "UpLo":    false,           //Def: false 大文字小文字区別
 *        "ZenHan":  false            //Def: false 全角半角区別
 *        "pMatch":  false            //Def: false 完全一致
 *     });
 * 
 * Default Element:"mark", Class:""
 * 
 */
(function($){
	
	$.fn.keyHighlight = function(keyword, options){
		
		//セッティング
		var defaults = {
			"Element": "mark",
			"Class": "",
			"addID": true,
			"scroll": [false, "body", "html,body"],
			"UpLo": false,
			"ZenHan": false,
			"pMatch": false
		};
		var settings = $.extend({}, defaults, options);
		if(settings.pMatch){
			settings.UpLo = true;
			settings.ZenHan = true;
		}
		
		//キーワードが空だったら終了
		if(!settings.pMatch) keyword = $.trim( keyword.replace(/[\s　]+/g," ") );
		if(!keyword || keyword == '' || !this.length) return;
		
		//console.log(keyword);
		
		
		//分割文字候補
		var splitArr = '㌀㍇㍢㍣㍤㍥㍦㍧㍨㍩㍪㍫㍬㍭㍮㍯㍰'.split('');
		
		//変換エンティティ
		var entity = {
			";": ["&#59;"]
			,"&": ["&amp;","&#38;"]
			,"<": ["&lt;"]
			,">": ["&gt;"]
			,"'": ["&#39;","&apos;"]
			,'"': ["&quot;","&#34;"]
		};
		
		//正規表現エスケープ
		var entity2Arr = ') ( [ ] { } / $ # & ` " _ ? ^ + * | . , - \\'.split(' ');
		for(var i in entity2Arr){
			entity2Arr[i] = '\\' + entity2Arr[i];
		}
		var entity2 = new RegExp('(['+entity2Arr.join('')+'])', "g");
		
		//タグ
		var TagNameArr = [
			"a","abbr","acronym","address","applet","area","article","aside","audio","b","base","basefont"
			,"bdi","bdo","bgsound","big","blink","blockquote","br","button","canvas","caption","center","cite"
			,"code","col","colgroup","command","comment","datalist","dd","del","details","dfn","dir","div"
			,"dl","dt","em","embed","fieldset","figcaption","figure","font","footer","form","frame","frameset"
			,"h1","h2","h3","h4","h5","h6","header","hgroup","hr","i","iframe","ilayer","img","input","ins","isindex"
			,"kbd","keygen","label","layer","legend","li","link","listing","map","mark","marquee","menu","meta"
			,"meter","multicol","nav","nextid","nobr","noembed","noframes","nolayer","noscript","object"
			,"ol","optgroup","option","output","p","param","plaintext","pre","progress","q","rb","rbc","rp","rt"
			,"rtc","ruby","s","samp","script","section","select","server","small","source","spacer","span","strike"
			,"strong","style","sub","summary","sup","table","tbody","td","textarea","tfoot","th","thead","time","tr"
			,"track","tt","u","ul","var","video","wbr","xmp"
		];
		
		
		//エスケープ置換
		var escapeRegex = function(s){
			return s.replace(entity2, "\\$1");
		};
		
		
		//文字を実態文字参照へ変換
		var strToEntity = function(str){
			var r = str;
			var strRe = new RegExp("[\&\<\>\"\']", "g");
			r = r.replace(strRe, function(chr){
				return entity[chr][0];
			});
			return r;
		};
		
		
		//タグと文字に分離
		var strTagSep = function(htmlSrc){
			var htmlStr=[], htmlTag=[];
			//ソースをタグと文字列に分離し１つの配列に
			var tag = "((<\/?("+TagNameArr.join('|')+")( [^>]*?)?>)+)";
			var reg = new RegExp("("+tag+"|([^<]+))", 'ig');
			//htmlSrc = htmlSrc.replace(/(<!--.*?-->)/g, '').replace(/(\s+|&nbsp;)/g, ' ');
			htmlSrc = htmlSrc.replace(/(\s+|&nbsp;)/g, ' ');
			
			var temp = $('<div>'+htmlSrc+'</div>');
			temp.find('*').contents().each(function(i,e){//console.log(i);
				if (this.nodeType == 8) {//console.log(this);
					$(this).remove();
				}
			});
			htmlSrc = temp.html();
			
			htmlSrc = htmlSrc.match(reg);
			//分離した配列を個々の変数へ（検索対象の文字列のみと最後に統合するタグのみ）
			for(var i=0,len=htmlSrc.length; i<len; i++){
				if(/^</.test(htmlSrc[i])){
					htmlStr.push(splitStr[0]);
					htmlTag.push(htmlSrc[i]);
				}else{
					htmlStr.push(htmlSrc[i]);
					htmlTag.push("");
				}
			}
			return [htmlStr, htmlTag];
		};
		
		
		//タグと文字を統合
		var strTagJoin = function(){
			var htmlSrc=[];
			var reg = new RegExp(splitStr[0]);
			var n = Math.max(htmlStr.length, htmlTag.length);
			for(var i=0; i<n; i++){
				if(reg.test(htmlStr[i])){
					htmlSrc.push(htmlTag[i]||'');
				}else{
					htmlSrc.push(htmlStr[i]||'');
				}
			}
			return htmlSrc;
		};
		
		
		//重複配列削除
		var uniqueArr = function(array){
			if(!array) return array;
			var storage = {};
			var uniqueArray = [];
			var i,value;
			for( i=0; i<array.length; i++){
				value = array[i];
				if(!(value in storage)){
					storage[value] = true;
					uniqueArray.push(value);
				}
			}
			return uniqueArray;
		};
		
		
		//空配列削除
		var delNullArr = function(array){
			if(!array) return array;
			var r=[];
			for( i=0; i<array.length; i++){
				if(array[i]){
					r.push(array[i]);
				}
			}
			return r;
		};
		
		
		//
		var UpLo = (settings.UpLo) ? "g":"ig";
		
		
		//処理ループ
		var selectorLen = this.length;
		for(var ini=0; ini<selectorLen; ini++){
			
			var thisSelector = this[ini];
			
			//タグ区切りに使う文字がセレクタ中に使われているか判断。
			var splitStr = splitArr;
			var tempReg = new RegExp(splitStr.join('|'), "g");
			var temp = $(thisSelector).html().match(tempReg);
			if(temp){
				temp = new RegExp(temp.join('|'), "g");
				splitStr = splitStr.join('').replace(temp, '');
				splitStr = splitStr.split('');
			}
			//２つ残ればOK。２こ以下の場合は処理停止し次のセレクタへ
			if(splitStr.length < 2) continue;
			
			
			var htmlSrc = strTagSep($(thisSelector).html());
			var htmlStr = htmlSrc[0];//文字列配列
			var htmlTag = htmlSrc[1];//タグ配列
			//console.log(htmlSrc[0]);console.log(htmlSrc[1]);
			//検索対象文字列作成（配列を区切り文字で連結）
			var htmlStr = htmlStr.join(splitStr[1]);
			
			//検索対象文字列の文字参照を文字に変換
			for(var key in entity){
				var regs = new RegExp('('+ entity[key].join('|') +')', "g");
				htmlStr = htmlStr.replace(regs, key);
			}
			
			//キーワードで検索パターン作成
			var reg;
			if(settings.pMatch){
				reg = new RegExp(escapeRegex(keyword), "g");
			}else{
				var keyRegArr = [];
				var keyArr = keyword.split(' ');
				for(var i in keyArr){
					var keyReg = [];
					var keyStr = keyArr[i].split('');//一文字ずつ配列に格納
					
					if(!settings.ZenHan){
						for(var j in keyStr){
							keyStr[j] = keyStr[j].replace(/[\!-\~！-～￥]/, function(s){
								if(/[！-～]/.test(s)){
									return '(' + s + '|' + escapeRegex(String.fromCharCode(s.charCodeAt(0) - 0xFEE0)) + ')';
								}else if(/[￥]/.test(s)){
									return '(' + s + '|' + escapeRegex(String.fromCharCode(0x005C)) + ')';
								}else if(/[\\]/.test(s)){
									return '(' + escapeRegex(s) + '|' + String.fromCharCode(0xFFE5) + ')';
								}else{
									return '(' + escapeRegex(s) + '|' + String.fromCharCode(s.charCodeAt(0) + 0xFEE0) + ')';
								}
							});
							keyReg.push(keyStr[j]);//一文字ずつ「()」でラップ
						}
					}else{
						for(var j in keyStr){
							keyStr[j] = escapeRegex(keyStr[j]);//正規表現に関わる文字をエンティティ
							keyReg.push("("+keyStr[j]+")");//一文字ずつ「()」でラップ
						}
					}
					keyRegArr.push( keyReg.join("(("+splitStr[1] + splitStr[0] + splitStr[1]+")*)") );
				}
				keyRegArr = "(" + keyRegArr.join('|') +")";
				reg = new RegExp(keyRegArr, UpLo);
				//console.log(reg);
			}
			
			//検索し重複削除。ヒット文字列パターン用文字列作成
			var hit = false;
			hit = htmlStr.match(reg);
			hit = delNullArr(uniqueArr(hit));
			//console.log(hit);
			
			//ヒット文字列パターン毎にハイライトタグ付け
			if(hit){
				for(var i in hit){
					hit[i] = escapeRegex(hit[i]);
				}
				
				var reg = new RegExp('('+ hit.join('|') +')', UpLo);
				
				var repTag = [];
				var markClass = (settings.Class) ? ' class="'+ settings.Class +'"' : "";
				var wrapTag = ['<'+settings.Element + markClass +'>', '</'+settings.Element+'>'];
				
				htmlStr = htmlStr.replace(reg, function(s){
					var reg2 = new RegExp('('+ splitStr[0] +'|'+ splitStr[1] +')');
					temp='';
					if(reg2.test(s)){//セパレータ文字があるなら
						//セパレータ文字がない部分に
						var reg3 = new RegExp("[^" + splitStr[0] + splitStr[1] + "]+", 'ig');
						match = s.match(reg3);
						temp = s;
						for(var i in match){//console.log(i+':'+match[i]);
							var temp2 = escapeRegex(match[i]);
							var rem = new RegExp(temp2);
							temp = temp.replace(rem, splitStr[2]);
							repTag.push(wrapTag[0] + strToEntity(match[i]) + wrapTag[1]);
						}
					}else{
						temp = splitStr[2];
						repTag.push(wrapTag[0] + strToEntity(s) + wrapTag[1]);
					}
					
					//console.log(temp);
					return temp;
				});
				
				//console.log('元: '+ htmlStr);
				//検索対象文字列の文字を文字参照に戻す
				htmlStr = strToEntity(htmlStr);
				//console.log('結: '+ htmlStr);
				
				var re = new RegExp(splitStr[2]);
				for(var i in repTag){
					htmlStr = htmlStr.replace(re, repTag[i]);
				}
				
			}
			//タグ付け後の文字列を配列に再分離
			htmlStr = htmlStr.split(splitStr[1]);
			
			
			//再分離した配列とタグ配列を統合
			htmlSrc = strTagJoin().join('');
			
			
			//書き換え
			//console.log('完: '+ htmlSrc);
			$(thisSelector).html(htmlSrc);
			
			
			delete htmlStr;
			delete  htmlTag;
			delete hit;
			delete TagNameArr;
			keyStr = tag = reg = tempReg = temp = keyReg = keyRegArr = undefined;
			
			//console.log("end" + ini);
		}
		
		//ヒットタグにID付与（hitMark + 連番）
		if(settings.addID){
			var selClass = (settings.Class) ? '.' + settings.Class : '';
			$(this).find(settings.Element + selClass).each(function(i,e){
				$(e).attr("id", "hitMark" + i);
			});
		}
		
		if(settings.scroll[0]){
			var scrollPos = Math.floor( 
					$("#hitMark0").offset().top
					- $(settings.scroll[1]).offset().top
					- 20
			);
			scrollPos = (scrollPos) ? scrollPos : 0;
			$(settings.scroll[2]).animate({"scrollTop": scrollPos}, 700);
		}
		
		
		return;
		
	};
	
})(jQuery);
