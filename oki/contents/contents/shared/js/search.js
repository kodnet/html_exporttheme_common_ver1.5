// 
// ---------------------
/*/////////////////////////////////////////////////////////////////////////////////////////////////
	User Guide
		Search JavaScript
 Modified : 2018-02-09
/////////////////////////////////////////////////////////////////////////////////////////////////*/

/* global data, keyword_data, term, srCountType, thisFile, get,  txt, decodeURIComponent, pageInfo, ua, loadPath, notFoundJampPage */


// ---------------------------------------------------------

var Searching = (function() {

    var settings = {
        "maxItemOfPage": (pageInfo.viewMobile) ? 100000 : 10 // 1ページあたりの結果数
            ,
        "pageNaviMax": 5 // ページインクリメントの最大
            ,
        "snippetStart": 10 // ヒットキーワードの前何文字表示
            ,
        "snippetLength": 25 // 説明の表示文字数
            ,
        "jampPath": (loadPath.contents) ? loadPath.contents + '/' : '',
        "reSort": [
            "co", "sc", "pr", "fa", "kt", "qa", "mo", "os", "su", "ne", "ab", "me", "sw", "am", "ha", "tel", "cc", "ps", "ap", "pd", "vp", "uf", "mn", "mp", "ts", "cf", "manual"
            /*"co"//"コピー"
            ,"sc"//"スキャン"
            ,"pr"//"プリント"
            ,"fa"//"ファクス"
            ,"kt"//"基本的な使い方"
            ,"qa"//"困ったときには"
            ,"mo"//"目的から探す"
            ,"os"//"まだまだあるおすすめ機能"
            ,"su"//"セットアップ"
            ,"ne"//"ネットワーク"
            ,"ab"//"アドレス帳の登録／編集"
            ,"me"//"メンテナンス"
            ,"sw"//"便利なソフトウェア"
            ,"am"//"本機の管理／仕様一覧"
            ,"ha"//"はじめにお読みください"
            ,"syo"//"商標"
            ,"lic"//"ライセンス
            */
        ],
        "notFoundJampPage": "ha_000001.html",
        "index": data //別ファイルのインデックスの変数名
            ,
        "keywordData": keyword_data,
        "termDic": term,
        "srCountType": srCountType,
        "st": "#searchResult .count",
        "re": "#searchResult .result",
        "nv": "#searchResult .navi",
        "startTime": new Date().getTime(),
        "datalist": [],
        "datalistBody": [],
        "datalistTitle": [],
        "bolderlist": [],
        "releatekey": [],
        "hitTag": "mark",
        "hitClass": "hitkey",
        "KeyPass": true //遷移先のキーを渡す
            ,
        "KeyPassKey": "k",
        "KeyPassPage": "p",
        "page": 1,
        "option": {
            "andOr": "and",
            "charUpLo": false //大文字小文字区別[ する：true | しない：false ]
                ,
            "ZenHan": false,
            "pMatch": false
        },
        "opZenHanShow": ["ja", "zh"],
        "lastQuery": "",
        "entity": [
            ["&", ["&amp;", "&#38;"]] //二重変換抑制のため'&'を先頭に配置
            ,
            [" ", ["&nbsp;", "&#160;"]],
            ["<", ["&lt;", "&#60;"]],
            [">", ["&gt;", "&#62;"]],
            ['"', ["&quot;", "&#34;"]],
            ["'", ["&apos;", "&#39;"]],
            ["±", ["&plusmn;", "&#177;"]]
        ],
        "omi": '<span class="conm">&#8230;</span>'
    };

    var sResultData = [],
        searchTime = {},
        serchRe, temp = '';

    var __self = {

        "_init": function() {

            //ページネーション値取得
            settings.page = get['p'] || 1;

            //オプション取得設定
            for (var k in settings.option) {
                if (get[k]) {
                    settings.option[k] = (/^\d+$/.test(get[k])) ? get[k] * 1 : get[k];
                } else {
                    if (k == "andOr") {
                        settings.option[k] = 'and';
                    } else {
                        settings.option[k] = 0;
                    }
                }
            }

            __self._setIndex();

        }

        /* ***********************************
         * 
         */
        ,
        "_redy": {

            //言語による全角半角の表示設定
            "zenHanShowHide": function() {
                var lang = window.settings.language;
                var showLang = settings.opZenHanShow.join(',');
                var re = new RegExp("(,|^)" + lang + "(,|$)");
                if (!re.test(showLang)) {
                    $('#ZenHan').attr('disabled', 'disabled').hide().parent().attr('disabled', 'disabled').hide();
                }
            }

            //検索オプションの引継ぎ
            ,
            "optionDisplay": function() {
                    for (var k in settings.option) {
                        var t = $('#option').find('[name="' + k + '"]');
                        if (k == "andOr") {
                            t.filter('[value="' + get[k] + '"]').click();
                        } else {
                            t.removeAttr('checked');
                            if (t.is(':checkbox')) {
                                t.filter('[value="' + get[k] + '"]').click();
                            } else {
                                t.filter('[value="' + get[k] + '"]').attr('checked', 'checked');
                            }
                            __self._pMatchOption();
                        }
                    }
                }
                //イベント追加
                ,
            "addEvent": function() {

                $('#searchOption #option').css({
                    "width": $('#searchOption').width()
                });
                $('#searchOption > .label a').on("click", function() {
                    $('#option').slideDown("fast");
                    return false;
                });
                $('#searchOption span.close').on("click", function() {
                    $('#option').slideUp("fast");
                    return false;
                });

                $('#pMatch').on('change', function(e) {
                    __self._pMatchOption();
                });

                $('#searchResult .navigation .navi').on('click', 'span:not(.disable,.selected)', function() {
                    __self._pageMove($(this));
                });

            }
        }


        /* ***********************************
         * 完全一致のオプション表示
         */
        ,
        "_pMatchOption": function() {
            var enable = [
                "#pMatch", "#charUpLo"
            ].join(',');
            var t = $('#option').find('input:not(' + enable + ')');
            if ($('#pMatch').is(':checked')) {
                t.attr('disabled', 'disabled');
                t.parent().addClass('disabled');
            } else {
                t.removeAttr('disabled');
                t.parent().removeClass('disabled');
            }
        }

        /* ***********************************
         * 検索実行
         */
        ,
        "_searching": function(key, onload, btn) {
            searchTime.start = new Date().getTime();
            searchTime.onload = (onload == "onload") ? true : false;

            var getKey = '';
            if (settings.option.pMatch) {
                getKey = key || get['k'] || false;
            } else {
                getKey = key || $.trim(get['k'].replace(/[\s　]+/g, " ")) || false;
            }

            for (var k in settings.option) {
                var val = $('#option [name="' + k + '"]:checked').val();
                settings.option[k] = (/^\d+$/.test(val)) ? val * 1 : val || 0;
            }

            if (pageInfo.viewMobile && onload == "onload") {
                for (var k in get) {
                    settings.option[k] = (/^\d+$/.test(get[k])) ? get[k] * 1 : get[k];
                }
            }

            getKey = __self._convertKatakana(getKey);
            //if(getKey && getKey != settings.lastQuery){
            if (getKey) {
                settings.page = (onload == "onload") ? settings.page : 1;
                $("#searchKey").val(getKey).addClass("keyIn");
                settings.lastQuery = getKey;
                var re = __self._find(getKey);

                serchRe = re;
                __self._pagenavi(re);
                __self._view(re, settings.page);

                $("#Navi").tabs({ "active": 1 });
            }
            if (get['rel'] && !btn) {
                $('#option').show();
            } else {
                $('#searchOption span.close').click();
            }
        }



        /* ***********************************
         * 検索実行 TOP
         */
        ,
        "_searchingTop": function(key) {
            for (var k in settings.option) {
                var target = $('#option [name="' + k + '"]');
                var isEnabled = true;
                var val = '';
                if (target.is(':disabled') || target.parent().is('.disabled')) {
                    isEnabled = false;
                }
                if (isEnabled) val = $('#option [name="' + k + '"]:checked').val();
                settings.option[k] = (/^\d+$/.test(val)) ? val * 1 : val || 0;
            }

            var getKey = key;
            getKey = __self._convertKatakana(getKey);
            if (getKey) {
                var re = __self._find(getKey);
                if (re.length) {
                    var jampPage = __self._getUriParam(getKey, 1, settings.jampPath + settings.index[re[0]].File);
                    window.location = jampPage;
                } else {
                    //alert(txt.searchNotFound +'\n'+ txt.searchMoveOther);
                    var jampFile = settings.notFoundJampPage;
                    if (typeof notFoundJampPage != 'undefined') jampFile = notFoundJampPage;
                    var jampPage = __self._getUriParam(getKey, 1, settings.jampPath + jampFile);
                    window.location = jampPage;
                }

            }
        }


        /* ***********************************
         * インデックス取得
         */
        ,
        "_setIndex": function() {
            searchTime.loadIndexStart = new Date().getTime();
            var body, title, len = settings.index.length;
            for (var i = 0; i < len; i++) {
                body = title = '';
                body = __self._entityReplace(__self._ufrm(settings.index[i].Body));
                title = __self._entityReplace(__self._ufrm(settings.index[i].Title));
                settings.datalist.push(title + "　" + body);
                //settings.datalistBody.push( body );
                //settings.datalistTitle.push( title );
            }
            searchTime.loadIndexEnd = new Date().getTime();
            searchTime.loadIndexTime = Math.round(searchTime.loadIndexEnd - searchTime.loadIndexStart) / 1000;
        }


        /* ***********************************
         * エンティティ置換
         */
        ,
        "_entityReplace": function(str, type) {
            if (!str) return str;

            var r = str,
                eArr = settings.entity;

            var StringToEntity = function() {
                EntityToString();
                for (var i in eArr) {
                    var re = new RegExp(eArr[i][0], 'ig');
                    r = r.replace(re, eArr[i][1][0]);
                }
            };
            var EntityToString = function() {
                for (var i in eArr) {
                    var re = new RegExp(eArr[i][1].join('|'), 'ig');
                    r = r.replace(re, eArr[i][0]);
                }
                r = r.replace(/[ ]+/g, ' ');
            };

            if ('StringToEntity' == type) {
                StringToEntity();
            } else {
                EntityToString();
            }
            return r;
        }


        /* ***********************************
         * ユーザーフォント削除
         */
        ,
        "_ufrm": function(data) {
            var r = data;
            if (r) r = r.replace(/(@@@)((\d@)+)(@@)/g, '');
            return r;
        }


        /* ***********************************
         * 配列の重複削除
         */
        ,
        "_uniqueArray": function(array) {
            var storage = {};
            var uniqueArray = [];
            var i, value;
            for (i = 0; i < array.length; i++) {
                value = array[i];
                if (!(value in storage)) {
                    storage[value] = true;
                    uniqueArray.push(value);
                }
            }
            return uniqueArray;
        }


        /* ***********************************
         * 半角カタカナ　<=>　全角カタカナ
         */
        ,
        "_convertKatakana": function(str, toType) {
            var han = "ｶﾞ ｷﾞ ｸﾞ ｹﾞ ｺﾞ ｻﾞ ｼﾞ ｽﾞ ｾﾞ ｿﾞ ﾀﾞ ﾁﾞ ﾂﾞ ﾃﾞ ﾄﾞ ﾊﾞ ﾊﾟ ﾋﾞ ﾋﾟ ﾌﾞ ﾌﾟ ﾍﾞ ﾍﾟ ﾎﾞ ﾎﾟ ｳﾞ ｧ ｱ ｨ ｲ ｩ ｳ ｪ ｴ ｫ ｵ ｶ ｷ ｸ ｹ ｺ ｻ ｼ ｽ ｾ ｿ ﾀ ﾁ ｯ ﾂ ﾃ ﾄ ﾅ ﾆ ﾇ ﾈ ﾉ ﾊ ﾋ ﾌ ﾍ ﾎ ﾏ ﾐ ﾑ ﾒ ﾓ ｬ ﾔ ｭ ﾕ ｮ ﾖ ﾗ ﾘ ﾙ ﾚ ﾛ ﾜ ｦ ﾝ ｡ ｢ ｣ ､ ･ ｰ ﾞ ﾟ".split(' ');
            var zen = "ガギグゲゴザジズゼゾダヂヅデドバパビピブプベペボポヴァアィイゥウェエォオカキクケコサシスセソタチッツテトナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲン。「」、・ー゛゜".split('');
            if (han.length == zen.length) {
                //基本は半角=>全角
                var from = han;
                var to = zen;
                if (toType == "toHan") {
                    from = zen;
                    to = han;
                }
                for (var i = 0, len = from.length; i < len; i++) {
                    var re = new RegExp(from[i], "g");
                    str = str.replace(re, to[i]);
                }
            }
            return str;
        }


        /* ***********************************
         * エスケープ処理
         */
        ,
        "_escapeRegex": function(str) {
            return str.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&");
        }


        /* ***********************************
         * 全角半角正規表現生成　＆　エスケープ　a => (a|ａ)、ＡＢ => (Ａ|A)(Ｂ|B)、［ => (［|\[)
         */
        ,
        "_zenHanRegExp": function(str) {
            if (/[\!-\~！-～￥]/.test(str)) {
                str = str.split('');
                for (var i = 0, len = str.length; i < len; i++) {
                    str[i] = str[i].replace(/[\!-\~！-～￥]/, function(s) {
                        if (/[！-～]/.test(s)) {
                            return '(' + s + '|' + __self._escapeRegex(String.fromCharCode(s.charCodeAt(0) - 0xFEE0)) + ')';
                        } else if (/[￥]/.test(s)) {
                            return '(' + s + '|' + __self._escapeRegex(String.fromCharCode(0x005C)) + ')';
                        } else if (/[\\]/.test(s)) {
                            return '(' + __self._escapeRegex(s) + '|' + String.fromCharCode(0xFFE5) + ')';
                        } else {
                            return '(' + __self._escapeRegex(s) + '|' + String.fromCharCode(s.charCodeAt(0) + 0xFEE0) + ')';
                        }
                    });
                }
                str = str.join('');
            } else {
                str = __self._escapeRegex(str);
            }
            return str;
        }

        /* ***********************************
         * 検索キー作成
         */
        ,
        "_createFindReg": function(v) {
            var findregs = [];
            var rkey = v;
            var key = [],
                findregs = [];

            if (settings.option.pMatch) {
                v = __self._escapeRegex(v); //エスケープ処理
                key = [v];
                findregs.push(new RegExp("(" + v + ")", settings.charUpLo));
            } else {
                v = v.replace(/[\s　]+/g, " ");
                if (!settings.option.ZenHan) {
                    v = __self._zenHanRegExp(v); //全角半角＆エスケープ処理
                } else {
                    v = __self._escapeRegex(v); //エスケープ処理
                }
                key = v.split(" ");
                for (var i = 0, len = key.length; i < len; i++) {
                    if (key[i] == "") key.splice(i, 1);
                }
                for (var i = 0, len = key.length; i < len; i++) {
                    try {
                        findregs.push(new RegExp("(" + key[i] + ")", settings.charUpLo));
                    } catch (e) {}
                }
            }


            //関連キーワード作成
            __self._releateQuery(rkey);

            return findregs;
        }


        /* ***********************************
         * ソート
         */
        ,
        "_regSort": function(a, b) {
            return (a.source.length < b.source.length) ? 1 : -1;
        }


        /* ***********************************
         * 関連キーワード作成
         */
        ,
        "_releateQuery": function(keys) {
            settings.releatekey = [];
            keys = $.trim(keys.replace(/[\s　]+/g, " "));

            var rebf = ["(^| )", "( |$)"];
            if (settings.option.pMatch) {
                rebf = ["^", "$"];
            }
            $.each(settings.keywordData, function(baseWord, releatewords) {
                var re = new RegExp(rebf[0] + __self._zenHanRegExp(baseWord) + rebf[1], "i");
                if (re.test(keys)) {
                    settings.releatekey = settings.releatekey.concat(releatewords);
                }
            });

            for (var i in settings.releatekey) {
                settings.releatekey[i] = settings.releatekey[i].replace(/[\s　]+/g, " ");
            }
            settings.releatekey = __self._uniqueArray(settings.releatekey);
        }


        /* ***********************************
         * 検索処理
         */
        ,
        "_find": function(query) {
            //console.log(settings.option);
            if (!query) return [];

            settings.charUpLo = (settings.option.charUpLo) ? "" : "i";

            //検索キー作成 [/(aaa)/i, /(bbb)/i]
            var findRegs = __self._createFindReg(query);
            settings.bolderlist = findRegs;

            var result = [];
            //console.log(findRegs);

            //キーワード分のループ
            for (var i in findRegs) {
                var findReg = findRegs[i];
                var Hit, HitTitle, HitBody;
                result[i] = [];
                //インデックスデータのループ
                for (var j in settings.datalist) {
                    Hit = findReg.test(settings.datalist[j]);
                    //HitTitle = findReg.test(settings.datalistTitle[j]);
                    //HitBody = findReg.test(settings.datalistBody[j]);
                    if (Hit) {
                        //result[i].push([j, HitTitle, HitBody]);
                        result[i].push([j]);
                    }
                }
            }

            //キーワードが複数の場合、and or の処理
            if (findRegs.length > 1) {
                if (settings.option.andOr == 'or') {
                    //orは重複を統一
                    var temp = [];
                    for (var i in result) {
                        temp = temp.concat(result[i]);
                    }
                    result = temp;

                    var storage = {};
                    var uniqueArray = [];
                    var val;
                    for (var i in result) {
                        val = result[i][0];
                        if (val in storage) {
                            //uniqueArray[i][1] = uniqueArray[i][1] || result[storage[val]][1];
                            //uniqueArray[i][2] = uniqueArray[i][2] || result[storage[val]][2];
                        } else {
                            storage[val] = val;
                            uniqueArray.push(result[i]);
                        }
                    }
                    result = uniqueArray;

                } else {
                    //andは重複を抽出
                    var overlap = result[0];
                    //console.log("1-Result: "+overlap.toString());
                    var val;
                    var len = result.length;
                    for (var i = 1; i < len; i++) { //キーワードループ（2個目から）
                        //console.log((i+1)+"-Result: "+result[i].toString());
                        var temp = [];
                        for (var j in overlap) {
                            var lapA = overlap[j][0];
                            for (var k in result[i]) { //結果数ループ
                                val = result[i][k][0]; //結果2個目以降のindex
                                if (val == lapA) { //基本オブジェクトと比較
                                    //console.log('重複: '+val);
                                    //result[i][k][1] = result[i][k][1] || overlap[j][1];
                                    //result[i][k][2] = result[i][k][2] || overlap[j][2];
                                    temp.push(result[i][k]);
                                }
                            }
                        }
                        overlap = temp;
                    }
                    result = overlap;
                }
            }
            //キーワードがひとつの場合、配列レベルを上げる
            else {
                result = result[0];
            }

            result = __self._sortResult(result);

            //console.log(result);
            return result;

        }


        /* ***********************************
         * 検索結果からソートと表示用データ作成
         */
        ,
        "_sortResult": function(result) {
            if (result && result.length) {
                var re = result;
                var sortData = { "other": [] };
                while (re.length) {
                    var thisData = re.shift();
                    var thisIndex = settings.index[thisData[0]];
                    if (!sortData[thisIndex.ChapKey]) {
                        sortData[thisIndex.ChapKey] = [];
                    }
                    sortData[thisIndex.ChapKey].push([thisIndex, thisData]);
                }
                sortData.other = re;

                //console.log(sortData);
                re = [];
                sResultData = [];
                for (var i in settings.reSort) {
                    var d = sortData[settings.reSort[i]];
                    if (d) {
                        for (var j in d) {
                            sResultData.push(d[j][0]);
                            re.push(d[j][1]);
                        }
                    }
                }
                //console.log(sResultData);
                //console.log(re);
                result = re;
            }
            return result;
        }


        /* ***********************************
         * 遷移用ＵＲＬ作成
         */
        ,
        "_getUriParam": function(keyword, page, url) {
            var key = [];
            var uri = url || '';
            key.push(settings.KeyPassKey + "=" + encodeURIComponent(keyword));
            for (var k in settings.option) {
                if (settings.option[k]) {
                    if (k == "andOr") {
                        if (settings.option[k] != "and")
                            key.push(k + "=" + settings.option[k]);
                    } else {
                        key.push(k + "=" + settings.option[k]);
                    }
                }
            }
            key.push(settings.KeyPassPage + "=" + page);
            return uri + '?' + key.join('&');
        }



        /* ***********************************
         * 検索結果を表示
         */
        ,
        "_view": function(result, page) {
            var offset = page || 1;
            var totalHit = result.length;
            var buf = [],
                count = 0;

            if (!totalHit) {
                var findReg = [__self._zenHanRegExp(settings.lastQuery)];
                //var inputKey = __self._uniqueArray(settings.lastQuery.split(' '));
                //for(var i in inputKey){
                //	if(inputKey[i])
                //		findReg.push(__self._zenHanRegExp(inputKey[i]));
                //}
                findReg = new RegExp("^(" + findReg.join('|') + ")$", "i");
                var baseTerm = [];
                for (var k in settings.termDic) {
                    var termItem = settings.termDic[k];
                    for (var i in termItem) {
                        if (findReg.test(termItem[i])) {
                            baseTerm.push(k);
                        }
                    }
                }
                baseTerm = __self._uniqueArray(baseTerm);
                if (baseTerm.length) {
                    buf.push('<p class="info">' + txt.searchNotFound2 + '<br/>' + txt.searchOtherLink2 + '</p>');
                    buf.push('<div class="termWord">');
                    //buf.push('<h4>'+ txt.searchSynonymHeader +'</h4>');
                    for (var i in baseTerm) {
                        buf.push('<p>' +
                            '<a href="' + __self._getUriParam(baseTerm[i], 1, pageInfo.fileName) + '">' +
                            baseTerm[i] +
                            '</a>' +
                            '</p>'
                        );
                    }
                    buf.push('</div');
                    $(settings.re).html(buf.join(""));
                } else {
                    $(settings.re).html('<p class="info">' + txt.searchNotFound + '<br/>' + txt.searchReTry + '</p>');
                }
                $(settings.st).html('');
            } else {
                //遷移先のキーを渡す
                var key;
                if (settings.KeyPass) {
                    key = __self._getUriParam(settings.lastQuery, offset);
                }

                var prevChapName = '';
                for (var i = (offset - 1) * settings.maxItemOfPage; i < totalHit; i++) {
                    count++;
                    if (count > settings.maxItemOfPage) break;
                    var da = settings.index[result[i][0]];
                    var title = __self._entityReplace(da.Title);
                    var body = __self._entityReplace(da.Body);
                    var chapName = __self._entityReplace(da.ChapName);

                    //トリミング
                    var snippetRe = __self._snippet(body, settings.bolderlist);
                    body = snippetRe.body;

                    //ハイライト
                    body = __self._boldKeyword(body, settings.bolderlist);
                    title = __self._boldKeyword(title, settings.bolderlist);

                    //チャプタータイトル表示
                    if (chapName != prevChapName) {
                        buf.push('<h2>' + chapName + '</h2>');
                        prevChapName = chapName;
                    }

                    buf.push(
                        '<div class="item">', '<h3><a href="', settings.jampPath + da.File + key, '" data-ajax="false">', title || txt.searchUnknoun, '</a></h3>', '<p>', snippetRe.start + body + snippetRe.end, '</p>'
                        //,'<div class="path">', da.Path ,'</div>
                        , '</div>'
                    );
                }

                //関連リンク
                $('#relatedWords').remove();
                if (settings.releatekey.length) {
                    temp = '<div id="relatedWords"><h4>' + txt.searchRelatedHeader + '</h4>';
                    var releatedFil = [];
                    for (var k in settings.option) {
                        if (settings.option[k]) releatedFil.push(k + "=" + settings.option[k]);
                    }
                    for (var i = 0, len = settings.releatekey.length; i < len; i++) {
                        var releatedFil_add = [];
                        var releatedAddClass = [];
                        if (/\s/.test(settings.releatekey[i]) && !settings.option.pMatch) {
                            releatedFil_add = ["pMatch=1", "rel=1"];
                            releatedAddClass.push('pMatch');
                        }
                        fil = [settings.KeyPassKey + "=" + encodeURIComponent(settings.releatekey[i])].concat(releatedFil).concat(releatedFil_add);
                        relatedUrl = pageInfo.fileName + "?" + fil.join('&amp;');
                        releatedAddClass = (releatedAddClass.length) ? ' ' + releatedAddClass.join(' ') : '';
                        temp += '<span class="relatedKey' + releatedAddClass + '"><a href="' + relatedUrl + '" data-ajax="false">' + settings.releatekey[i] + '</a></span>';
                    }
                    temp += '</div>';
                    buf.unshift(temp);
                }

                var srCount = '';
                if (settings.srCountType == 1) {
                    srCount = '<span>' + totalHit + '</span><span>' + txt.searchCounts1 + '</span>';
                } else {
                    srCount = '<span>' + totalHit + '</span><span>' + txt.searchCounts1 + '</span>';
                }

                $(settings.st).html(function() {
                    return (totalHit == 1) ? srCount.replace('items', 'item') : srCount;
                });

                $(settings.re).html(function() {
                    return buf.join("") || '<p class="info">' + txt.searchNotFound + '<br/>' + txt.searchReTry + '</p>';
                });

            }
            $('html,body').scrollTop(0);

            //時間計測
            searchTime.end = new Date().getTime();
            searchTime.time = Math.round(searchTime.end - searchTime.start) / 1000;
            if (searchTime.onload) {
                searchTime.time += searchTime.loadIndexTime;
                searchTime.onload = false;
            }
            //console.log("Search time: " + searchTime.time + " sec");
        }


        /* ***********************************
         * 説明トリミング
         */
        ,
        "_snippet": function(body, boldreg) {
            var sniPoint = 0,
                isMatch = false;
            var igm = /*(settings.option.pMatch)? '' :*/ settings.charUpLo;
            for (i = 0, len = boldreg.length; i < len; i++) {
                var re = new RegExp(boldreg[i].source, igm);
                isMatch = body.match(re);
                if (isMatch) break;
            }
            if (isMatch) sniPoint = Math.max(sniPoint, isMatch.index);

            var sb = sniPoint - settings.snippetStart;
            var eb = sniPoint + settings.snippetStart + settings.snippetLength;
            var rb = body.substring(sb, eb);

            var start = (sb > 0) ? settings.omi : "";
            var end = (body.length > eb) ? settings.omi : "";

            return { "body": rb, "start": start, "end": end };
        }


        /* ***********************************
         * キーワードの強調処理
         */
        ,
        "_boldKeyword": function(body, boldregs, boolFalse) {
            var result = __self._entityReplace(body, 'EntityToString');
            var ele = settings.hitTag || "mark";
            var cls = settings.hitClass || '';

            if (boolFalse !== false) {
                var strreg = [],
                    tmpreg = '';
                for (i = 0, len = boldregs.length; i < len; i++) {
                    tmpreg = boldregs[i].source;
                    tmpreg = tmpreg.substring(1, tmpreg.length - 1);
                    strreg.push(__self._entityReplace(tmpreg));
                }
                strreg = "(" + strreg.join('|') + ")";

                var igm = /*(settings.option.pMatch)? 'gm' :*/ settings.charUpLo + 'gm';
                var boldreg = new RegExp(strreg, igm);
                if (!/^(\d|\d(@\d)+)$/.test(result)) {
                    result = result.replace(boldreg, '<' + ele + ' class="' + cls + '">$1</' + ele + '>');
                }
            }
            return result;
        }


        /* ***********************************
         * ページナビ作成
         */
        ,
        "_pagenavi": function(result, p) {
            var buf = [];
            var bufF = $('<div><span class="marg first" title="' + txt.searchResultFirst + '">&nbsp;</span>' +
                '<span class="marg prev" title="' + txt.searchResultPrev + '">&nbsp;</span></div>');
            var bufR = $('<div><span class="marg next" title="' + txt.searchResultNext + '">&nbsp;</span>' +
                '<span class="marg last" title="' + txt.searchResultLast + '">&nbsp;</span></div>');
            var pageCount = Math.ceil(result.length / settings.maxItemOfPage);
            settings.page = (p === 'last') ? pageCount : p || settings.page;
            //console.log(settings.page);
            var i = 1;
            if (settings.pageNaviMax < pageCount) {
                if (settings.page == 1) { //先頭
                    bufF.find('span').addClass('disable');
                    //bufR = bufR;
                } else if (settings.page == pageCount) { //最後
                    i = pageCount - settings.pageNaviMax + 1;
                    //bufF = bufF;
                    bufR.find('span').addClass('disable');
                } else {
                    i = settings.page - (Math.floor(settings.pageNaviMax / 2));
                    if (pageCount - i < settings.pageNaviMax - 1) {
                        i = i - ((settings.pageNaviMax - 1) - (pageCount - i));
                    }
                    //bufF = bufF;
                    //bufR = bufR;
                }
            } else {
                bufF.find('span').addClass('hide');
                bufR = $('');
            }
            bufF = bufF.html() || "";
            bufR = bufR.html() || "";

            i = (i < 1) ? 1 : i;
            var loop = Math.min(pageCount, i + settings.pageNaviMax - 1);
            while (i <= loop) {
                if (i == settings.page) {
                    buf.push('<span class="selected">' + i + '</span>');
                } else {
                    buf.push('<span>' + i + '</span>');
                }
                i++;
            }
            $(settings.nv).html(bufF + buf.join("") + bufR);

            if (p === 'last') return pageCount;
        }

        ,
        "_pageMove": function(n) {
            searchTime.start = new Date().getTime();
            var movPage = function(p) {
                if (/^\d+$/.test(p)) {
                    __self._pagenavi(serchRe, p);
                    __self._view(serchRe, p);
                } else {
                    return false;
                }
            };

            if (n.is('.prev')) {
                movPage(n.nextAll('.selected').text() - 1);
            } else if (n.is('.next')) {
                movPage(n.prevAll('.selected').text() - 0 + 1);
            } else if (n.is('.first')) {
                movPage(1);
            } else if (n.is('.last')) {
                __self._view(serchRe, __self._pagenavi(serchRe, "last"));
            } else {
                movPage(n.text());
            }

        }

    };

    __self._init();

    return {
        "redy": __self._redy,
        "searching": __self._searching,
        "searchingTop": __self._searchingTop
    };
}());


$(function() {
    Searching.redy.zenHanShowHide();
    Searching.redy.optionDisplay();
    Searching.redy.addEvent();
});